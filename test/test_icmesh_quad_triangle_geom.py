import numpy as np
from icmesh import (
    mesh_cuboid,
    mesh_tetra,
)


def test_quad_triangle_geomtraits():
    # a 2D quadrangle mesh created by giving number of nodes, faces/edges and cells
    quad_mesh_2D = mesh_cuboid(4, 4, 1)

    # set connectivities
    quad_mesh_2D.set_edges_nodes([[0, 1], [1, 2], [2, 3], [0, 3]])
    quad_mesh_2D.set_cells_edges([[0, 1, 2, 3]])

    # set vertices
    quad_mesh_2D.vertices[0] = np.asarray([0, 0])
    quad_mesh_2D.vertices[1] = np.asarray([2, 0])
    quad_mesh_2D.vertices[2] = np.asarray([2, 2])
    quad_mesh_2D.vertices[3] = np.asarray([1, 2])

    assert (
        quad_mesh_2D.cells_faces.targets_size()
        == quad_mesh_2D.cells_edges.targets_size()
    )
    assert quad_mesh_2D.cells_faces.targets_size() == 4

    assert not quad_mesh_2D.ordered_faces_nodes

    # a 2D triangle mesh created by giving number of nodes, faces/edges and cells
    tri_mesh_2D = mesh_tetra(3, 3, 1)

    # set connectivities
    tri_mesh_2D.set_edges_nodes([[0, 1], [1, 2], [2, 0]])
    tri_mesh_2D.set_cells_edges([[0, 1, 2]])

    # set vertices
    tri_mesh_2D.vertices[0] = np.asarray([0, 0])
    tri_mesh_2D.vertices[1] = np.asarray([0.5, 2])
    tri_mesh_2D.vertices[2] = np.asarray([3, 0.3])

    assert (
        tri_mesh_2D.cells_faces.targets_size() == tri_mesh_2D.cells_edges.targets_size()
    )
    assert tri_mesh_2D.cells_faces.targets_size() == 3

    assert not tri_mesh_2D.ordered_faces_nodes
