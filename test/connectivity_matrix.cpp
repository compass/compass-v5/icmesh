#include <catch2/catch_test_macros.hpp>
#include <icmesh/Connectivity_matrix.h>
#include <iostream>

template <std::size_t i, std::size_t j, typename CMat>
  requires(icmesh::is_connectivity_matrix_v<CMat>)
void identify_element(const CMat &mat) {
  std::cout << "connectivity matrix element (" << i << "," << j << "): ";
  auto &elt = mat.template connectivity<i, j>();
  if constexpr (std::is_same_v<std::decay_t<decltype(elt)>,
                               icmesh::Undefined>) {
    std::cout << "undefined" << std::endl;
  } else {
    std::cout << elt.targets_size_v << std::endl;
  }
}

TEST_CASE("test connectivity matrix") {

  using namespace icmesh;

  connectivity_matrix_row_t<-1, 0, 2> tc;

  static_assert(is_CM_row_v<connectivity_matrix_row<-1, 0>>);

  using cmr = connectivity_matrix_row<-1, 1>;
  static_assert(cmr::get<0>() == -1);
  static_assert(cmr::get<1>() == 1);

  // row nb 1 (i.e. second row) in dimension 3
  using dcmr = default_CM_row<1, 3>::type;

  static_assert(dcmr::get<0>() == 2);
  static_assert(dcmr::get<1>() == 0);
  static_assert(dcmr::get<2>() == -1);
  static_assert(dcmr::get<3>() == -1);

  dcmr row;

  static_assert(get<0>(row) == 2);
  static_assert(get<1>(row) == 0);
  static_assert(get<2>(row) == -1);
  static_assert(get<3>(row) == -1);

  Connectivity_matrix<connectivity_matrix_row<-1, 1>,
                      connectivity_matrix_row<-1, -1>>
      M;

  static_assert(is_connectivity_matrix_v<decltype(M)>);

  // default_connectivity_matrix<2> has connectivity types:
  //  ( 0   -1   -1 )
  //  ( -1   0   -1 )
  //  ( -1  -1    0 )

  using DCM = default_connectivity_matrix<2>;
  static_assert(is_connectivity_matrix_v<DCM>);

  DCM dcm;
  // all connectivities
  // dcm.connectivity<i, i>() raises error (at the compilation) because no
  // connectivities on the diagonal (no connectivity node - node for example)
  auto &conn_ExV = dcm.connectivity<1, 0>();
  auto &conn_VxE = dcm.connectivity<0, 1>();
  auto &conn_FxV = dcm.connectivity<2, 0>();
  auto &conn_VxF = dcm.connectivity<0, 2>();
  auto &conn_ExF = dcm.connectivity<1, 2>();
  auto &conn_FxE = dcm.connectivity<2, 1>();
  static_assert(
      std::is_same_v<Mesh_connectivity<2>, std::decay_t<decltype(conn_ExV)>>);
  static_assert(
      std::is_same_v<Mesh_connectivity<-1>, std::decay_t<decltype(conn_VxE)>>);
  static_assert(
      std::is_same_v<Mesh_connectivity<-1>, std::decay_t<decltype(conn_FxV)>>);
  static_assert(
      std::is_same_v<Mesh_connectivity<-1>, std::decay_t<decltype(conn_VxF)>>);
  static_assert(
      std::is_same_v<Mesh_connectivity<-1>, std::decay_t<decltype(conn_ExF)>>);
  static_assert(
      std::is_same_v<Mesh_connectivity<-1>, std::decay_t<decltype(conn_FxE)>>);

  // test over structured connectivity matrix, print the size of all
  // Connectivity
  //  (-1 if unstructured connectivity)
  {
    structured_CM_row<0, 2> srow0;
    std::cout << get<0>(srow0) << " " << get<1>(srow0) << " " << get<2>(srow0)
              << " " << std::endl;
    structured_CM_row<1, 2> srow1;
    std::cout << get<0>(srow1) << " " << get<1>(srow1) << " " << get<2>(srow1)
              << " " << std::endl;
    structured_CM_row<2, 2> srow2;
    std::cout << get<0>(srow2) << " " << get<1>(srow2) << " " << get<2>(srow2)
              << " " << std::endl;
  }
  {
    structured_CM_row<0, 3> srow0;
    std::cout << get<0>(srow0) << " " << get<1>(srow0) << " " << get<2>(srow0)
              << " " << get<3>(srow0) << std::endl;
    structured_CM_row<1, 3> srow1;
    std::cout << get<0>(srow1) << " " << get<1>(srow1) << " " << get<2>(srow1)
              << " " << get<3>(srow1) << std::endl;
    structured_CM_row<2, 3> srow2;
    std::cout << get<0>(srow2) << " " << get<1>(srow2) << " " << get<2>(srow2)
              << " " << get<3>(srow2) << std::endl;
    structured_CM_row<3, 3> srow3;
    std::cout << get<0>(srow3) << " " << get<1>(srow3) << " " << get<2>(srow3)
              << " " << get<3>(srow3) << std::endl;
  }

  structured_connectivity_matrix<2> scm;

  auto &ExF = scm.connectivity<1, 2>();
  CHECK(!ExF); // connectivity has not been filled yet
  auto &FxE = scm.connectivity<2, 1>();
  CHECK(!FxE); // connectivity has not been filled yet
  static_assert(Structured_connectivity<2>::connectivity_size<2, 1>() == 4);
  static_assert(
      std::is_same_v<Mesh_connectivity<4>, std::decay_t<decltype(FxE)>>);

  CHECK(scm.is_consistent_row<0>());
  CHECK(scm.is_consistent_row<1>());
  CHECK(scm.is_consistent_row<2>());
  CHECK(scm.is_consistent_column<0>());
  CHECK(scm.is_consistent_column<1>());
  CHECK(scm.is_consistent_column<2>());
  CHECK(scm.is_consistent());

  structured_connectivity_matrix<3> scm3;
  CHECK(scm3.is_consistent());
  identify_element<0, 1>(scm3);
  identify_element<0, 2>(scm3);
  identify_element<0, 3>(scm3);
  identify_element<1, 0>(scm3);
  identify_element<1, 2>(scm3);
  identify_element<1, 3>(scm3);
  identify_element<2, 0>(scm3);
  identify_element<2, 1>(scm3);
  identify_element<2, 3>(scm3);
  identify_element<3, 0>(scm3);
  identify_element<3, 1>(scm3);
  identify_element<3, 2>(scm3);

  auto sc3 = structured_connectivity(2, 2, 2);
  auto cm3 = connectivity_matrix(sc3);
  CHECK(cm3.is_consistent_row<0>());
  CHECK(cm3.is_consistent_row<1>());
  CHECK(cm3.is_consistent_row<2>());
  CHECK(cm3.is_consistent_row<3>());
  CHECK(cm3.is_consistent_column<0>());
  CHECK(cm3.is_consistent_column<1>());
  CHECK(cm3.is_consistent_column<2>());
  CHECK(cm3.is_consistent_column<3>());
  CHECK(cm3.is_consistent());
}
