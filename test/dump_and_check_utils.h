#pragma once
#include <catch2/catch_test_macros.hpp>
#include <compass_cxx_utils/pretty_print.h>
#include <icmesh/Mesh_elements.h>

template <std::size_t dim>
void dump_and_check(const icmesh::Mesh_elements<dim> &mesh) {
  compass::utils::Pretty_printer print;
  print("== mesh of dimension", dim);
  print(mesh.nodes.size(), "nodes");
  print(mesh.edges.size(), "edges");
  print(mesh.faces.size(), "faces");
  print(mesh.cells.size(), "cells");
  if constexpr (dim == 1) {
    CHECK(mesh.edges == mesh.cells);
    CHECK(mesh.nodes == mesh.faces);
    CHECK(mesh.nodes != mesh.cells);
  }
  if constexpr (dim == 2) {
    CHECK(mesh.edges == mesh.faces);
    CHECK(mesh.nodes != mesh.faces);
    CHECK(mesh.faces != mesh.cells);
    CHECK(mesh.nodes != mesh.cells);
  }
  if constexpr (dim == 3) {
    CHECK(mesh.nodes != mesh.edges);
    CHECK(mesh.nodes != mesh.faces);
    CHECK(mesh.nodes != mesh.cells);
    CHECK(mesh.edges != mesh.faces);
    CHECK(mesh.edges != mesh.cells);
    CHECK(mesh.faces != mesh.cells);
  }
}
