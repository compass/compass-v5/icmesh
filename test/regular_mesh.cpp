#include <catch2/catch_test_macros.hpp>
#include <icmesh/Geomtraits.h>

TEST_CASE("test regular mesh") {
  // required accuracy
  auto eps = 1.e-7;

  SECTION("in 1D") {
    // creates a 1D grid with N segments, N+1 nodes
    const std::size_t N = 5;
    auto step = 1.0 / N;
    auto unit_grid = icmesh::Constant_axis_offset{0., N, step};
    // creates a regular mesh with geometry utils methodes
    auto mesh1D = icmesh::mesh(icmesh::lattice(unit_grid));

    CHECK_FALSE(mesh1D.ordered_faces_nodes);

    for (auto &&[n, P] : mesh1D.vertices.enumerate())
      CHECK(fabs(P[0] - n * step) < eps);

    auto edges_nodes = mesh1D.get_connectivity<mesh1D.edge, mesh1D.node>();
    for (auto &&[e, e_nodes] : edges_nodes.targets_by_source()) {
      CHECK(e_nodes.size() == 2);
      CHECK(((e_nodes[0] == e) && (e_nodes[1] == e + 1)));
    }

    auto &edge_isobarycenter = mesh1D.get_edge_isobarycenters(); // Vertex_field
    for (auto &&[e, P] : edge_isobarycenter.enumerate())
      CHECK(fabs(P[0] - (e + 0.5) * step) < eps);

    auto mesh_isobarycenters =
        mesh1D.isobarycenters(mesh1D.joined_elements); // Vertex_field
    auto mesh_centers_of_mass =
        mesh1D.centers_of_mass(mesh1D.joined_elements); // Vertex_field
    for (auto &&el : mesh1D.joined_elements) {
      // In a regular mesh, the isobarycenter and center-of-mass of all elements
      // are identical
      CHECK(icmesh::distance(mesh_isobarycenters(el),
                             mesh_centers_of_mass(el)) < eps);
    }

    auto &edge_measure = mesh1D.get_edge_measures(); // Scalar_field
    for (auto &&m : edge_measure)
      CHECK(fabs(m - 1. / N) < eps);
  }

  SECTION("in 2D") {
    // creates a 2D grid with (N segments, N+1 nodes) * (N segments, N+1 nodes)
    const std::size_t N = 3;
    auto step = 1.0 / N;
    auto unit_grid = icmesh::Constant_axis_offset{0., N, step};
    // creates a regular mesh with geometry utils methodes
    auto mesh2D = icmesh::mesh(icmesh::lattice(unit_grid, unit_grid));

    CHECK_FALSE(mesh2D.ordered_faces_nodes);

    for (auto &&[n, P] : mesh2D.vertices.enumerate()) {
      CHECK(fabs(P[0] - ((int)n / (N + 1)) * step) < eps);
      CHECK(fabs(P[1] - n % (N + 1) * step) < eps);
    }

    auto cells_nodes = mesh2D.get_connectivity<mesh2D.cell, mesh2D.node>();
    for (auto &&[K, K_nodes] : cells_nodes.targets_by_source()) {
      CHECK(K_nodes.size() == 4);
      auto base = ((int)K / N) * (N + 1) + K % N;
      CHECK(K_nodes[0] == base);
      CHECK(K_nodes[1] == base + 1);
      CHECK(K_nodes[2] == base + N + 1);
      CHECK(K_nodes[3] == base + N + 2);
    }

    auto &edge_isobarycenter = mesh2D.get_edge_isobarycenters(); // Vertex_field
    for (auto &&[e, P] : edge_isobarycenter.enumerate()) {
      if (e < N * (N + 1)) {
        CHECK(fabs(P[0] - ((int)e / N) * step) < eps);
        CHECK(fabs(P[1] - (e % N + 0.5) * step) < eps);
      } else {
        auto e2 = e - N * (N + 1);
        CHECK(fabs(P[0] - (e2 / (N + 1) + 0.5) * step) < eps);
        CHECK(fabs(P[1] - e2 % (N + 1) * step) < eps);
      }
    }

    auto &cell_isobarycenter = mesh2D.get_cell_isobarycenters(); // Vertex_field
    for (auto &&[K, P] : cell_isobarycenter.enumerate()) {
      CHECK(fabs(P[0] - (K / N + 0.5) * step) < eps);
      CHECK(fabs(P[1] - (K % N + 0.5) * step) < eps);
    }

    auto mesh_isobarycenters =
        mesh2D.isobarycenters(mesh2D.joined_elements); // Vertex_field
    auto mesh_centers_of_mass =
        mesh2D.centers_of_mass(mesh2D.joined_elements); // Vertex_field
    for (auto &&el : mesh2D.joined_elements) {
      // In a regular mesh, the isobarycenter and center-of-mass of all elements
      // are identical
      CHECK(icmesh::distance(mesh_isobarycenters(el),
                             mesh_centers_of_mass(el)) < eps);
    }

    auto &edge_measure = mesh2D.get_edge_measures(); // Scalar_field
    for (auto &&m : edge_measure)
      CHECK(fabs(m - step) < eps);

    auto &cell_measure = mesh2D.get_cell_measures(); // Scalar_field
    auto cell_vol = std::pow(step, 2);
    for (auto &&m : cell_measure)
      CHECK(fabs(m - cell_vol) < eps);

    for (auto &&K : mesh2D.cells)
      CHECK(fabs(mesh2D.diameter<mesh2D.cell>(K) - sqrt(2) * step) < eps);

    // normal_vect_out<mesh.edge> takes indices of an edge and
    // a 2D element, it gets the coordinates from geom
    using Mesh_t = decltype(mesh2D);
    using Vertex = Mesh_t::Vertex;
    Vertex vy{0.0, 1.0};
    Vertex vy_inv{0.0, -1.0};
    auto K0 = 0;
    auto K1 = 1;
    auto e = 13; // edge common to both cells K0 and K1

    auto normal_outward_K0 =
        mesh2D.template normal_vect_out<mesh2D.edge>(e, K0);
    CHECK(icmesh::distance(normal_outward_K0, vy) < eps);

    auto normal_outward_K1 =
        mesh2D.template normal_vect_out<mesh2D.edge>(e, K1);
    CHECK(icmesh::distance(normal_outward_K1, vy_inv) < eps);
  }

  SECTION("in 3D") {
    // creates a 3D grid with (N segments, N+1 nodes) ** 3
    const std::size_t N = 2;
    auto step = 1.0 / N;
    auto unit_grid = icmesh::Constant_axis_offset{0., N, step};
    // creates a regular mesh with geometry utils methodes
    auto mesh3D =
        icmesh::mesh(icmesh::lattice(unit_grid, unit_grid, unit_grid));

    CHECK_FALSE(mesh3D.ordered_faces_nodes);

    for (auto &&[n, P] : mesh3D.vertices.enumerate()) {
      CHECK(fabs(P[0] - ((int)n / ((N + 1) * (N + 1))) * step) < eps);
      CHECK(fabs(P[1] - ((int)n / (N + 1)) % (N + 1) * step) < eps);
      CHECK(fabs(P[2] - (n % (N + 1)) * step) < eps);
    }

    auto cells_nodes = mesh3D.get_connectivity<mesh3D.cell, mesh3D.node>();
    for (auto &&[K, K_nodes] : cells_nodes.targets_by_source()) {
      CHECK(K_nodes.size() == 8);
      auto base = ((int)K / (N * N)) * (N + 1) * (N + 1) +
                  ((int)K / N) % N * (N + 1) + K % N;
      CHECK(K_nodes[0] == base);
      CHECK(K_nodes[1] == base + 1);
      CHECK(K_nodes[2] == base + N + 1);
      CHECK(K_nodes[3] == base + N + 2);
      CHECK(K_nodes[4] == base + (N + 1) * (N + 1));
      CHECK(K_nodes[5] == base + (N + 1) * (N + 1) + 1);
      CHECK(K_nodes[6] == base + (N + 1) * (N + 1) + N + 1);
      CHECK(K_nodes[7] == base + (N + 1) * (N + 1) + N + 2);
    }

    auto &edge_isobarycenter = mesh3D.get_edge_isobarycenters(); // Vertex_field
    for (auto &&[e, P] : edge_isobarycenter.enumerate()) {
      if (e < N * (N + 1) * (N + 1)) {
        CHECK(fabs(P[0] - e / (N * (N + 1)) * step) < eps);
        CHECK(fabs(P[1] - (e / N) % (N + 1) * step) < eps);
        CHECK(fabs(P[2] - (e % N + 0.5) * step) < eps);
      } else if (e < 2 * N * (N + 1) * (N + 1)) {
        auto e2 = e - N * (N + 1) * (N + 1);
        CHECK(fabs(P[0] - e2 / (N * (N + 1)) * step) < eps);
        CHECK(fabs(P[1] - ((e2 / (N + 1)) % N + 0.5) * step) < eps);
        CHECK(fabs(P[2] - e2 % (N + 1) * step) < eps);
      } else {
        auto e3 = e - 2 * N * (N + 1) * (N + 1);
        CHECK(fabs(P[0] - (e3 / ((N + 1) * (N + 1)) + 0.5) * step) < eps);
        CHECK(fabs(P[1] - (e3 / (N + 1)) % (N + 1) * step) < eps);
        CHECK(fabs(P[2] - e3 % (N + 1) * step) < eps);
      }
    }

    auto &face_isobarycenter = mesh3D.get_face_isobarycenters(); // Vertex_field
    for (auto &&[f, P] : face_isobarycenter.enumerate()) {
      if (f < N * N * (N + 1)) {
        CHECK(fabs(P[0] - f / (N * N) * step) < eps);
        CHECK(fabs(P[1] - ((f / N) % N + 0.5) * step) < eps);
        CHECK(fabs(P[2] - (f % N + 0.5) * step) < eps);
      } else if (f < 2 * N * N * (N + 1)) {
        auto f2 = f - N * N * (N + 1);
        CHECK(fabs(P[0] - (f2 / (N * (N + 1)) + 0.5) * step) < eps);
        CHECK(fabs(P[1] - (f2 / N) % (N + 1) * step) < eps);
        CHECK(fabs(P[2] - (f2 % N + 0.5) * step) < eps);
      } else {
        auto f3 = f - 2 * N * N * (N + 1);
        CHECK(fabs(P[0] - (f3 / (N * (N + 1)) + 0.5) * step) < eps);
        CHECK(fabs(P[1] - ((f3 / (N + 1)) % N + 0.5) * step) < eps);
        CHECK(fabs(P[2] - f3 % (N + 1) * step) < eps);
      }
    }

    auto &cell_isobarycenter = mesh3D.get_cell_isobarycenters(); // Vertex_field
    for (auto &&[K, P] : cell_isobarycenter.enumerate()) {
      CHECK(fabs(P[0] - (K / (N * N) + 0.5) * step) < eps);
      CHECK(fabs(P[1] - ((K / N) % N + 0.5) * step) < eps);
      CHECK(fabs(P[2] - (K % N + 0.5) * step) < eps);
    }

    auto mesh_isobarycenters =
        mesh3D.isobarycenters(mesh3D.joined_elements); // Vertex_field
    auto mesh_centers_of_mass =
        mesh3D.centers_of_mass(mesh3D.joined_elements); // Vertex_field
    for (auto &&el : mesh3D.joined_elements) {
      // In a regular mesh, the isobarycenter and center-of-mass of all elements
      // are identical
      CHECK(icmesh::distance(mesh_isobarycenters(el),
                             mesh_centers_of_mass(el)) < eps);
    }

    auto &edge_measure = mesh3D.get_edge_measures(); // Scalar_field
    for (auto &&m : edge_measure)
      CHECK(fabs(m - step) < eps);

    auto &face_measure = mesh3D.get_face_measures(); // Scalar_field
    auto face_area = std::pow(step, 2);
    for (auto &&m : face_measure)
      CHECK(fabs(m - face_area) < eps);

    auto &cell_measure = mesh3D.get_cell_measures(); // Scalar_field
    auto cell_vol = std::pow(step, 3);
    for (auto &&m : cell_measure)
      CHECK(fabs(m - cell_vol) < eps);

    for (auto &&f : mesh3D.faces)
      CHECK(fabs(mesh3D.diameter<mesh3D.face>(f) - sqrt(2) * step) < eps);

    for (auto &&K : mesh3D.cells)
      CHECK(fabs(mesh3D.diameter<mesh3D.cell>(K) - sqrt(3) * step) < eps);

    // normal_vect_out<mesh.edge> takes indices of an edge and
    // a 2D element, it gets the coordinates from geom
    using Mesh_t = decltype(mesh3D);
    using Vertex = Mesh_t::Vertex;
    Vertex vz{0.0, 0.0, 1.0};
    Vertex vz_inv{0.0, 0.0, -1.0};
    auto f0 = 0;
    auto f1 = 1;
    auto e = 19; // edge common to both faces f0 and f1

    auto normal_outward_f0 =
        mesh3D.template normal_vect_out<mesh3D.edge>(e, f0);
    CHECK(icmesh::distance(normal_outward_f0, vz) < eps);

    auto normal_outward_f1 =
        mesh3D.template normal_vect_out<mesh3D.edge>(e, f1);
    CHECK(icmesh::distance(normal_outward_f1, vz_inv) < eps);

    // normal_vect_out<mesh.face> takes indices of a face and
    // a 3D element, it gets the coordinates from geom
    auto K0 = 0;
    auto K1 = 1;
    auto f = 25; // face common to both cells K0 and K1

    auto normal_outward_K0 =
        mesh3D.template normal_vect_out<mesh3D.face>(f, K0);
    CHECK(icmesh::distance(normal_outward_K0, vz) < eps);

    auto normal_outward_K1 =
        mesh3D.template normal_vect_out<mesh3D.face>(f, K1);
    CHECK(icmesh::distance(normal_outward_K1, vz_inv) < eps);
  }
}
