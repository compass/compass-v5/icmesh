import numpy as np
from icmesh import regular_lattice, scottish_lattice


def test_regular_lattice():
    def from_tuple(t):
        l = regular_lattice(t)
        print(type(l))
        assert l.elements_shape == t
        assert l.points_shape == tuple(i + 1 for i in t)
        return l

    l = from_tuple((2,))
    assert l.origin == (0.0,)
    assert l.extent == (2.0,)
    l = from_tuple((3, 2))
    assert l.origin == (0.0, 0.0)
    assert l.extent == (3.0, 2.0)
    l = from_tuple((1, 2, 3))
    assert l.origin == (0.0, 0.0, 0.0)
    assert l.extent == (1.0, 2.0, 3.0)


def test_scottish_lattice():
    def from_arrays(*args, **kwargs):
        l = scottish_lattice(*args, **kwargs)
        print(type(l))
        return l

    l = from_arrays(np.ones(10, dtype=np.double))
    assert l.origin == (0.0,)
    assert l.extent == (10.0,)
    l = from_arrays(
        np.ones(10, dtype=np.double),
        np.arange(1, 6, dtype=np.double),
        origin=(1.0, 2.0),
    )
    assert l.origin == (1.0, 2.0)
    assert l.extent == (10.0, 15.0)
