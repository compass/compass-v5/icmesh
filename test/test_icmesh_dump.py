import numpy as np
import xml.dom.minidom as minidom
from icmesh import regular_mesh, scottish_mesh, read_fvca6
from icmesh.dump import write_mesh, dump_mesh, dump_vtu_3d
from pathlib import Path


def is_coherent(the_mesh, vtufile, npzfile):
    # get npz info
    mesh = np.load(npzfile)
    npz_vertices = mesh["vertices"]
    coc_to_list = lambda mesh, s: np.split(
        mesh[f"{s}_values"], mesh[f"{s}_offsets"][:-1]
    )
    cfv = mesh["cells_faces_values"]
    cells_faces = coc_to_list(mesh, "cells_faces")
    faces_nodes = coc_to_list(mesh, "faces_nodes")
    assert np.all(cfv == np.hstack([faces for faces in the_mesh.cells_faces.targets()]))
    # get vtu info (binary file, read only the header)
    mesh_doc = minidom.parse(vtufile)
    file_arrays = mesh_doc.getElementsByTagName("DataArray")
    for data in file_arrays:
        if data.getAttribute("Name") == "Points":
            # 3 coordinates
            assert int(data.getAttribute("NumberOfComponents")) == 3

    mesh_dom = mesh_doc.getElementsByTagName("Piece")[0]
    assert len(cells_faces) == int(mesh_dom.getAttribute("NumberOfCells"))
    assert len(npz_vertices) == int(mesh_dom.getAttribute("NumberOfPoints"))


def test_io_mesh():
    # dump and write regular mesh
    mesh = regular_mesh((2, 3, 4), origin=(1, 1, 0))
    write_mesh("regular.vtu", mesh)
    dump_mesh("regular.npz", mesh)
    is_coherent(mesh, "regular.vtu", "regular.npz")

    # dump and write scottish mesh
    mesh = scottish_mesh(
        np.arange(1, 7, dtype=np.double) / 12.0,
        np.arange(1, 5, dtype=np.double) / 4.0,
        np.arange(1, 4, dtype=np.double) / 9.0,
    )
    write_mesh("scottish.vtu", mesh)
    dump_mesh("scottish.npz", mesh)
    is_coherent(mesh, "scottish.vtu", "scottish.npz")

    # dump and write voronoi mesh (no structured mesh)
    current_path = Path(__file__).parent.absolute()
    voro_file = (current_path / Path("mesh_data")).as_posix() + "/vmesh_1.msh"
    mesh = read_fvca6(voro_file)
    write_mesh("voronoi.vtu", mesh)
    dump_mesh("voronoi.npz", mesh)
    is_coherent(mesh, "voronoi.vtu", "voronoi.npz")


def test_dump_vtu_3d():
    mesh = scottish_mesh(
        np.arange(1, 6, dtype=np.double) / 15.0,
        np.arange(1, 3, dtype=np.double) / 3.0,
        np.arange(1, 5, dtype=np.double) / 10.0,
    )
    dump_vtu_3d(mesh, "scottish_3d.vtu")
    mesh = regular_mesh((2, 3, 4), origin=(1, 1, 0))
    dump_vtu_3d(mesh, "regular_3d.vtu")
    current_path = Path(__file__).parent.absolute()
    voro_file = (current_path / Path("mesh_data")).as_posix() + "/vmesh_1.msh"
    mesh = read_fvca6(voro_file)
    dump_vtu_3d(mesh, "voronoi_3d.vtu")
