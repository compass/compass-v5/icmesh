
#include <catch2/catch_test_macros.hpp>

#include <icmesh/Connectivity_matrix.h>

TEST_CASE("Default connectivity matrices") {
  using namespace icus;
  using namespace icmesh;

  default_connectivity_matrix<2> cm2;
  CHECK(is_connectivity_matrix_v<default_connectivity_matrix<2>>);
  CHECK(is_regular_connectivity<decltype(cm2.connectivity<1, 0>())>);
  CHECK(cm2.connectivity<1, 0>().targets_size_v == 2);
}
