from icmesh import (
    regular_mesh,
    grid_center,
    bottom_boundary_nodes,
    bottom_boundary_edges,
    bottom_boundary_faces,
    xmin_boundary_faces,
    all_boundaries_faces,
    bottom_boundary,
)


def test_boundaries():
    mesh = regular_mesh((5, 2, 3), extent=(1.5, 5.12, 3.2))
    center = grid_center(mesh)
    assert center == (0.75, 2.56, 1.6)
    vertices = mesh.vertices.as_array()
    face_isobarycenters = mesh.face_isobarycenters.as_array()
    bottom_bv = bottom_boundary_nodes(mesh)
    left_bf = xmin_boundary_faces(mesh)
    all_bf = all_boundaries_faces(mesh)
    assert (vertices[bottom_bv.iset.mapping_as_array()][:, -1] <= mesh.origin[-1]).all
    assert (
        face_isobarycenters[left_bf.iset.mapping_as_array()][:, 0] <= mesh.origin[0]
    ).all
    assert len(all_bf) == 62
    bottom_elemts = bottom_boundary(mesh)
    bottom_be = bottom_boundary_edges(mesh)
    bottom_bf = bottom_boundary_faces(mesh)
    assert bottom_bv.issubset(bottom_elemts)
    assert bottom_be.issubset(bottom_elemts)
    assert bottom_bf.issubset(bottom_elemts)
    assert (bottom_bv | bottom_bf | bottom_be).issubset(bottom_elemts)
    assert (bottom_elemts).issubset(bottom_bv | bottom_bf | bottom_be)
    assert (bottom_bv | bottom_bf | bottom_be).issuperset(bottom_elemts)
    assert (bottom_elemts).issuperset(bottom_bv | bottom_bf | bottom_be)
