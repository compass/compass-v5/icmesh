#include <catch2/catch_test_macros.hpp>
#include <icmesh/Geom_utils.h>

TEST_CASE("test geometry") {

  SECTION("in 1D") {
    constexpr int dim = 1;
    using Vertex = std::array<icmesh::Scalar, dim>;
    const auto eps = 1.e-7; // accuracy

    // define some basic vectors
    Vertex v0{0.0};
    Vertex vx{1.0};
    Vertex vx_inv{-1.0};

    // usual Euclidian norm
    CHECK(fabs(icmesh::norm(vx) - 1.0) < eps);
    CHECK(((icmesh::norm(v0) >= 0) && (icmesh::norm(v0) < eps)));

    // distance(a,b) = norm(a-b)
    CHECK(
        ((icmesh::distance(vx, vx) >= 0) && (icmesh::distance(vx, vx) < eps)));
    CHECK(fabs(icmesh::distance(vx, v0) - 1) < eps);

    // area of a triangle : no meaning in 1D
    auto area = icmesh::triangle_area<dim>(v0, vx, vx_inv);
    CHECK(((area >= 0) && (area < eps)));
  }

  SECTION("in 2D") {
    constexpr int dim = 2;
    using Vertex = std::array<icmesh::Scalar, dim>;
    const auto eps = 1.e-7; // accuracy

    // define some basic vectors
    Vertex v0{0.0, 0.0};
    Vertex vx{1.0, 0.0};
    Vertex vy{0.0, 1.0};

    // usual Euclidian norm
    CHECK(fabs(icmesh::norm(vx) - 1.0) < eps);
    CHECK(((icmesh::norm(v0) >= 0) && (icmesh::norm(v0) < eps)));

    // distance(a,b) = norm(a-b)
    CHECK(
        ((icmesh::distance(vx, vx) >= 0) && (icmesh::distance(vx, vx) < eps)));
    CHECK(fabs(icmesh::distance(vx, v0) - 1) < eps);
    CHECK(fabs(icmesh::distance(vx, vy) - sqrt(2)) < eps);

    // vector normal to a segment
    auto n_vect = icmesh::normal_vect<dim>(vx, v0);
    CHECK(icmesh::distance(n_vect, vy) < eps);

    auto Ibase = icus::ISet(3);
    auto coords = icus::make_field<Vertex>(Ibase);
    coords(0) = v0;
    coords(1) = vx;
    coords(2) = vy;
    std::array<int, 2> points = {1, 0};
    n_vect = icmesh::normal_vect<dim>(coords, points);
    CHECK(icmesh::distance(n_vect, vy) < eps);

    // area of a triangle
    auto area = icmesh::triangle_area<dim>(v0, vx, vy);
    CHECK(fabs(area - 0.5) < eps);
    area = icmesh::triangle_area<dim>(v0, v0, vy);
    CHECK(((area >= 0) && (area < eps)));

    // area and vector normal to a triangle
    std::tie(n_vect, area) = icmesh::triangle_VecNormalT(v0, vx, vy, vx);
    CHECK(fabs(area - 0.5) < eps);
    // the vector normal to a triangle has no meaning in 2D and is set to null
    CHECK(icmesh::norm(n_vect) < eps);
  }

  SECTION("in 3D") {
    constexpr int dim = 3;
    using Vertex = std::array<icmesh::Scalar, dim>;
    const auto eps = 1.e-7; // accuracy

    // define some basic vectors
    Vertex v0{0.0, 0.0, 0.0};
    Vertex vx{1.0, 0.0, 0.0};
    Vertex vy{0.0, 1.0, 0.0};
    Vertex vz{0.0, 0.0, 1.0};
    Vertex vz_inv{0.0, 0.0, -1.0};

    {
      // we allow (and check) operations between vertices (i.e. array)
      using namespace compass::utils::array_operators;
      CHECK(v0 - vx == Vertex{-1., 0, 0});
      CHECK(v0 - vy == Vertex{0, -1., 0});
      CHECK(v0 - vz == Vertex{0, 0, -1.});
    }

    // usual Euclidian norm
    CHECK(fabs(icmesh::norm(vx) - 1.0) < eps);
    CHECK(((icmesh::norm(v0) >= 0) && (icmesh::norm(v0) < eps)));

    // distance(a,b) = norm(a-b)
    CHECK(
        ((icmesh::distance(vx, vx) >= 0) && (icmesh::distance(vx, vx) < eps)));
    CHECK(fabs(icmesh::distance(vx, v0) - 1) < eps);
    CHECK(fabs(icmesh::distance(vx, vy) - sqrt(2)) < eps);

    // vector product
    auto vprod = icmesh::vector_product<dim>(vx, vy);
    CHECK(icmesh::distance(vprod, vz) < eps);
    vprod = icmesh::vector_product<dim>(vx, v0);
    CHECK(icmesh::norm(vprod) < eps);
    vprod = icmesh::vector_product<dim>(v0, v0);
    CHECK(icmesh::norm(vprod) < eps);

    // vector normal to a triangle
    auto n_vect = icmesh::normal_vect<dim>(v0, vx, vy);
    CHECK(icmesh::distance(n_vect, vz) < eps);

    auto Ibase = icus::ISet(4);
    auto coords = icus::make_field<Vertex>(Ibase);
    coords(0) = v0;
    coords(1) = vz;
    coords(2) = vx;
    coords(3) = vy;
    std::array<int, 3> points = {0, 2, 3};
    n_vect = icmesh::normal_vect<dim>(coords, points);
    CHECK(icmesh::distance(n_vect, vz) < eps);

    // area of a triangle
    auto area = icmesh::triangle_area<dim>(v0, vx, vy);
    CHECK(fabs(area - 0.5) < eps);
    area = icmesh::triangle_area<dim>(v0, v0, vy);
    CHECK(((area >= 0) && (area < eps)));

    // volume of a tetrahedron
    auto volume = icmesh::tetra_volume(v0, vx, vy, vz);
    CHECK(fabs(volume - 1 / 6.) < eps);

    // area and vector normal to a triangle, outward with respect to a point
    std::tie(n_vect, area) = icmesh::triangle_VecNormalT(v0, vx, vy, vz);
    CHECK(icmesh::distance(n_vect, vz_inv) < eps);
    CHECK(fabs(area - 0.5) < eps);
  }
}
