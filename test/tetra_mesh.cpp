#include <catch2/catch_test_macros.hpp>
#include <compass_cxx_utils/pretty_print.h>
#include <icmesh/Geomtraits.h>

TEST_CASE("test triangle tetra mesh") {
  using namespace icus;
  using namespace icmesh;

  compass::utils::Pretty_printer print;
  using stencils = std::vector<std::vector<icus::index_type>>;
  using stencils_nodes = std::vector<icus::index_type>;

  auto eps = 1.e-7;
  SECTION("in triangle mesh") {
    // triangle mesh (2D) data with 2 cells
    auto data_edges_nodes_tri =
        stencils{{0, 1}, {0, 3}, {1, 2}, {1, 3}, {2, 3}};
    auto data_cells_edges_tri = stencils{{0, 1, 3}, {2, 3, 4}};

    auto mesh_triangle = icmesh::mesh_tetra(4, 5, 2);

    CHECK_FALSE(mesh_triangle.ordered_faces_nodes);

    // set vertices
    mesh_triangle.vertices(0) = {0, 0};
    mesh_triangle.vertices(1) = {1, 0};
    mesh_triangle.vertices(2) = {1, 1};
    mesh_triangle.vertices(3) = {0, 1};

    // set connectivity
    mesh_triangle.set_connectivity<mesh_triangle.edge, mesh_triangle.node>(
        data_edges_nodes_tri);
    mesh_triangle.set_connectivity<mesh_triangle.cell, mesh_triangle.face>(
        data_cells_edges_tri);

    auto edges_nodes =
        mesh_triangle
            .get_connectivity<mesh_triangle.edge, mesh_triangle.node>();
    for (auto &&[e, e_nodes] : edges_nodes.targets_by_source())
      CHECK(e_nodes.size() == 2);

    auto cells_nodes =
        mesh_triangle
            .get_connectivity<mesh_triangle.cell, mesh_triangle.node>();
    for (auto &&[cn, c_nodes] : cells_nodes.targets_by_source())
      CHECK(c_nodes.size() == 3);

    auto cells_faces =
        mesh_triangle
            .get_connectivity<mesh_triangle.cell, mesh_triangle.face>();
    for (auto &&[cf, c_faces] : cells_faces.targets_by_source())
      CHECK(c_faces.size() == 3);

    auto cells_edges =
        mesh_triangle
            .get_connectivity<mesh_triangle.cell, mesh_triangle.edge>();
    for (auto &&[ce, c_edges] : cells_edges.targets_by_source())
      CHECK(c_edges.size() == 3);

    auto &cell_measure = mesh_triangle.get_cell_measures();
    for (auto &&cm : cell_measure)
      CHECK(abs(cm - 0.5) < eps);
  }
  // tetra mesh (3D) data with 2 cells
  SECTION("in tetrahedral mesh") {
    auto data_nodes = stencils_nodes{0, 1, 2, 3, 4};
    auto data_edges_nodes = stencils{{0, 1}, {0, 3}, {0, 2}, {1, 2}, {1, 3},
                                     {2, 3}, {0, 4}, {2, 4}, {1, 4}};
    auto data_faces_nodes = stencils{{0, 1, 2}, {0, 2, 3}, {1, 2, 3}, {0, 1, 3},
                                     {0, 4, 2}, {2, 4, 1}, {0, 4, 1}};
    auto data_faces_edges = stencils{{0, 2, 3}, {1, 2, 5}, {3, 4, 5}, {0, 1, 4},
                                     {2, 6, 7}, {3, 7, 8}, {1, 6, 8}};
    auto data_cells_nodes = stencils{{0, 1, 2, 3}, {0, 2, 1, 4}};
    auto data_cells_edges = stencils{{0, 1, 2, 3, 4, 5}, {0, 2, 3, 6, 7, 8}};
    auto data_cells_faces = stencils{{0, 1, 2, 3}, {0, 4, 5, 6}};

    std::size_t nodes_num = data_nodes.size();
    std::size_t edges_num = data_edges_nodes.size();
    std::size_t faces_num = data_faces_nodes.size();
    std::size_t cells_num = data_cells_nodes.size();

    auto mesh_tetrah =
        icmesh::mesh_tetra(nodes_num, edges_num, faces_num, cells_num);

    CHECK_FALSE(mesh_tetrah.ordered_faces_nodes);

    mesh_tetrah.vertices(0) = {0, 0, 0};
    mesh_tetrah.vertices(1) = {1, 1, 0};
    mesh_tetrah.vertices(2) = {1, 0, 1};
    mesh_tetrah.vertices(3) = {0, 1, 0};
    mesh_tetrah.vertices(4) = {1, 0, 0};

    // set connectivity
    mesh_tetrah.set_connectivity<mesh_tetrah.edge, mesh_tetrah.node>(
        data_edges_nodes);
    mesh_tetrah.set_connectivity<mesh_tetrah.face, mesh_tetrah.edge>(
        data_faces_edges);
    mesh_tetrah.set_connectivity<mesh_tetrah.cell, mesh_tetrah.edge>(
        data_cells_edges);
    mesh_tetrah.set_connectivity<mesh_tetrah.cell, mesh_tetrah.face>(
        data_cells_faces);
    mesh_tetrah.set_connectivity<mesh_tetrah.face, mesh_tetrah.node>(
        data_faces_nodes);

    auto edges_nodes =
        mesh_tetrah.get_connectivity<mesh_tetrah.edge, mesh_tetrah.node>();
    for (auto &&[e, e_nodes] : edges_nodes.targets_by_source())
      CHECK(e_nodes.size() == 2);

    auto faces_nodes =
        mesh_tetrah.get_connectivity<mesh_tetrah.face, mesh_tetrah.node>();
    for (auto &&[cn, c_nodes] : faces_nodes.targets_by_source())
      CHECK(c_nodes.size() == 3);

    auto faces_edges =
        mesh_tetrah.get_connectivity<mesh_tetrah.face, mesh_tetrah.node>();
    for (auto &&[fe, f_edges] : faces_edges.targets_by_source())
      CHECK(f_edges.size() == 3);

    auto cells_nodes =
        mesh_tetrah.get_connectivity<mesh_tetrah.cell, mesh_tetrah.node>();
    for (auto &&[cn, c_nodes] : cells_nodes.targets_by_source())
      CHECK(c_nodes.size() == 4);

    auto cells_faces =
        mesh_tetrah.get_connectivity<mesh_tetrah.cell, mesh_tetrah.face>();
    for (auto &&[cf, c_faces] : cells_faces.targets_by_source())
      CHECK(c_faces.size() == 4);

    auto cells_edges =
        mesh_tetrah.get_connectivity<mesh_tetrah.cell, mesh_tetrah.edge>();
    for (auto &&[ce, c_edges] : cells_edges.targets_by_source())
      CHECK(c_edges.size() == 6);
  }
}
