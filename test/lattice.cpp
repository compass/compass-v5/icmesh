#include <catch2/catch_test_macros.hpp>
#include <icmesh/Lattice.h>
#include <icmesh/Structured_connectivity.h>
#include <iostream>

TEST_CASE("test lattice") {
  using namespace icmesh;
  compass::utils::Pretty_printer print;

  auto cao = Constant_axis_offset{1., 12, 2.};

  std::cout << "Constant offsets:";
  for (std::size_t i = 0; i < cao.size(); ++i) {
    std::cout << " " << cao(i);
    CHECK(cao(i) == 1.0 + i * 2.0);
  }
  std::cout << std::endl;

  std::vector<double> steps{{1, 2, 3, 4, 5}};
  auto sao = Specific_axis_offset{0., steps};

  std::cout << "Specific offsets:";
  std::cout << " " << sao(0);
  for (std::size_t i = 1; i < sao.size(); ++i) {
    std::cout << " " << sao(i);
    CHECK(sao(i) == sao(i - 1) + steps[i - 1]);
  }
  std::cout << std::endl;

  auto l2d = lattice(cao, sao);

  std::cout << "Lattice shape:";
  for (auto &&n : l2d.elements_shape())
    std::cout << " " << n;
  std::cout << std::endl;
  CHECK(l2d.elements_shape().size() == 2);
  CHECK(l2d.elements_shape()[0] == cao.size() - 1);
  CHECK(l2d.elements_shape()[1] == sao.size() - 1);

  int i = 3, j = 4;
  auto [x1, y1] = l2d(i, j);
  auto [x2, y2] = l2d(std::array<int, 2>{i, j});
  CHECK((x1 == cao(i) && x2 == cao(i)));
  CHECK((y1 == sao(j) && y2 == sao(j)));

  auto lcon = structured_connectivity(l2d.elements_shape());
}
