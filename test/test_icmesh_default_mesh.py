from pathlib import Path
from icmesh import (
    Mesh1D,
    Mesh2D,
    Mesh3D,
    mesh,
    read_fvca6,
)
from icmesh.check import check_3D_mesh


def test_default_mesh():
    # creates empty mesh with the elements ISet (nodes, edges, faces, cells)
    # there is no values for the vertices nor the connectivities
    # 3D cube with 1 cell
    cube = mesh(8, 12, 6, 1)
    # 2D triangle with 1 cell
    triangle = mesh(3, 3, 1)


def test_read_fvca6():
    # get current path to create mesh path and output path (mkdir if necessary)
    current_path = Path(__file__).parent.absolute()
    mesh_file = (current_path / Path("mesh_data")).as_posix() + "/vmesh_1.msh"
    the_mesh = read_fvca6(mesh_file)
    check_3D_mesh(the_mesh)
