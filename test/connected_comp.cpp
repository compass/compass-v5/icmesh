
#include <catch2/catch_test_macros.hpp>
#include <compass_cxx_utils/pretty_print.h>
#include <icmesh/Connected_components.h>

static inline void print_meshes_info(auto &connected_meshes) {
  auto previous_m = connected_meshes.original_mesh;
  auto mesh = connected_meshes.mesh;
  auto gamma = connected_meshes.gamma;
  auto same_loc = connected_meshes.same_location;

  compass::utils::Pretty_printer print;
  for (auto &&[i, pos] : mesh.vertices.enumerate())
    print("node", i, "position is", pos);

  auto prev_fn =
      previous_m.template get_connectivity<previous_m.face, previous_m.node>();
  for (auto &&[f, nodes] : prev_fn.targets_by_source())
    print("previous face", f, "connects to previous nodes", nodes);
  auto fn = mesh.template get_connectivity<mesh.face, mesh.node>();
  auto fe = mesh.template get_connectivity<mesh.face, mesh.edge>();
  auto en = mesh.template get_connectivity<mesh.edge, mesh.node>();
  for (auto &&[f, nodes] : fn.targets_by_source()) {
    print("face", f, "connects to nodes", nodes);
    for (auto &&e : fe.targets_by_source(f)) {
      auto &&[n0, n1] = en.targets_by_source(e);
      print("face connects to edge", e, "containing nodes", n0, n1);
    }
  }
  auto face_centers = mesh.get_face_isobarycenters();
  auto gamma_centers = mesh.isobarycenters(gamma);
  auto same_face = same_loc.restrict_target(mesh.faces);
  for (auto &&[i, pos] : std::views::zip(gamma.mapping(), gamma_centers)) {
    print("gamma face ", i, "with center :", pos);
    // find the other faces at the same location, check with isobarycenter
    auto je_i = mesh.faces.mapping()[i]; // index in joined_elements
    for (auto &&o_face : same_face.targets_by_source(je_i)) {
      print("    same face as", o_face, "with center", face_centers(o_face));
    }
  }

  auto same_edge = same_loc.restrict_target(mesh.edges);
  auto gedges = fe.extract(gamma, mesh.edges).image();

  auto same_node = same_loc.restrict_target(mesh.nodes);
  auto gnodes = fn.extract(gamma, mesh.nodes).image();
  for (auto i : gnodes.mapping()) { // i in mesh.nodes
    print("gamma node ", i, "at pos :", mesh.vertices(i));
    // find the other nodes at the same location, check with isobarycenter
    auto je_i = mesh.nodes.mapping()[i]; // index in joined_elements
    for (auto &&o_node : same_node.targets_by_source(je_i)) {
      print("    same node as", o_node, "at pos", mesh.vertices(o_node));
    }
  }

  auto prev_cf =
      previous_m.template get_connectivity<previous_m.cell, previous_m.face>();
  for (auto &&[K, faces] : prev_cf.targets_by_source())
    print("previous cell", K, "connects to previous faces", faces);
  auto cf = mesh.template get_connectivity<mesh.cell, mesh.face>();
  for (auto &&[K, faces] : cf.targets_by_source())
    print("cell", K, "connects to faces", faces);
}

TEST_CASE("connected components") {

  SECTION("3D mesh") {
    // creates a 3D grid with N segments, N+1 nodes
    const std::size_t N = 2;
    auto step = 1.0 / N;
    auto unit_grid = icmesh::Constant_axis_offset{0., N, step};
    // creates a regular mesh with geometry utils methodes
    auto mesh = icmesh::mesh(icmesh::lattice(unit_grid, unit_grid, unit_grid));
    auto gamma = mesh.faces.extract({1});
    // some coherency tests are done at the creation of Connected_components in
    // debug
    auto conn_meshes = icmesh::Connected_components(mesh, gamma);

    // print previous and new mesh info
    print_meshes_info(conn_meshes);
    CHECK(conn_meshes.mesh.nodes.size() == mesh.nodes.size() + 4);
    CHECK(conn_meshes.mesh.edges.size() == mesh.edges.size() + 4);
    CHECK(conn_meshes.mesh.faces.size() == mesh.faces.size() + 1);
    CHECK(conn_meshes.mesh.cells.size() == mesh.cells.size());
    // other gamma set of faces
    auto gamma1 = mesh.faces.extract({4, 5});
    auto conn_meshes1 = icmesh::Connected_components(mesh, gamma1);
    CHECK(conn_meshes1.mesh.nodes.size() == mesh.nodes.size() + 9);
    CHECK(conn_meshes1.mesh.edges.size() == mesh.edges.size() + 12);
    CHECK(conn_meshes1.mesh.faces.size() == mesh.faces.size() + 4);
    CHECK(conn_meshes1.mesh.cells.size() == mesh.cells.size());
    // other gamma set of faces
    auto gamma2 = mesh.faces.extract({4, 5, 6, 7});
    auto conn_meshes2 = icmesh::Connected_components(mesh, gamma2);
    CHECK(conn_meshes2.mesh.nodes.size() == mesh.nodes.size() + 18);
    CHECK(conn_meshes2.mesh.edges.size() == mesh.edges.size() + 24);
    CHECK(conn_meshes2.mesh.faces.size() == mesh.faces.size() + 8);
    CHECK(conn_meshes2.mesh.cells.size() == mesh.cells.size());
    // gamma is the whole set of faces
    auto gamma3 = mesh.faces;
    auto conn_meshes3 = icmesh::Connected_components(mesh, gamma3);
    CHECK(conn_meshes3.mesh.nodes.size() ==
          mesh.nodes.size() + mesh.cells.size() * 8);
    CHECK(conn_meshes3.mesh.edges.size() ==
          mesh.edges.size() + mesh.cells.size() * 12);
    CHECK(conn_meshes3.mesh.faces.size() ==
          mesh.faces.size() + mesh.cells.size() * 6);
    CHECK(conn_meshes3.mesh.cells.size() == mesh.cells.size());
  }
}
