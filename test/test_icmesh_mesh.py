import numpy as np
import icus
from icmesh import (
    regular_mesh,
    scottish_mesh,
    Mesh1D,
    QuadMesh,
    CuboidMesh,
    Mesh2D,
    Mesh3D,
    Vertices_3,
    Vertex_1,
    Vertex_2,
    Vertex_3,
)


def test_vertex():
    v = Vertex_3(np.array([0, 1, 5]))
    assert v.as_array().shape == (3,)
    v = Vertex_2(np.array([0, 5]))
    assert v.as_array().shape == (2,)
    v = Vertex_1(np.array([4]))
    assert v.as_array().shape == (1,)


def test_regular_mesh():
    def from_tuple(t):
        m = regular_mesh(t)
        print(type(m))
        return m

    m = from_tuple((2,))
    assert m.dimension == 1
    assert m.elements_shape == (2,)
    assert m.extent == (2.0,)
    assert m.to_vertex(np.array([12])).as_array().shape == (1,)
    assert m.vertices.as_array().shape == (3, 1)
    assert not m.ordered_faces_nodes
    m = from_tuple((3, 2))
    assert m.dimension == 2
    assert m.elements_shape == (3, 2)
    assert m.extent == (3.0, 2.0)
    assert m.to_vertex(np.array([13, 56])).as_array().shape == (2,)
    assert m.vertices.as_array().shape[-1] == 2
    m = from_tuple((1, 2, 3))
    assert m.dimension == 3
    assert m.elements_shape == (1, 2, 3)
    assert m.extent == (1.0, 2.0, 3.0)
    assert m.to_vertex(np.array([3, 6, 7])).as_array().shape == (3,)
    assert m.vertices.as_array().shape[-1] == 3


def test_scottish_mesh():
    def from_arrays(*args, **kwargs):
        l = scottish_mesh(*args, **kwargs)
        print(type(l))

    from_arrays(np.ones(10, dtype=np.double))
    from_arrays(np.ones(3, dtype=np.double), np.arange(1, 5, dtype=np.double))


def test_2D_matplotlib_figure():
    import matplotlib.pyplot as plt

    mesh = regular_mesh((2, 3), origin=(1, 2))
    # a field property
    cell_index = icus.labels(mesh.cells)
    assert isinstance(cell_index, icus.Field)
    for k in mesh.cells:
        cell_index[k] = k
    # image corners
    llc, trc = mesh.bbox
    plt.imshow(
        cell_index.as_array().reshape(mesh.elements_shape).T[::-1],
        extent=[llc[0], trc[0], llc[1], trc[1]],
    )
    plt.colorbar()
    plt.title("cell index")
    plt.savefig("cell_index.png")


def test_3D_vti_export():
    import vtkwriters as vtkw

    mesh = regular_mesh((2, 3, 4), origin=(1, 1, 0))
    # a field property
    x = icus.field(mesh.nodes)
    assert isinstance(mesh.vertices, icus.Field)
    assert isinstance(Vertices_3(icus.ISet(10)), icus.Field)
    assert mesh.dimension == 3
    assert not mesh.ordered_faces_nodes
    # we would like to be able to do something like: x[:] = mesh.vertices[:, 0]
    x.as_array()[:] = mesh.vertices.as_array()[:, 0]
    # a raw numpy array
    z = np.reshape(mesh.vertices.as_array()[:, 2], mesh.points_shape)
    vtkw.write_vti(
        vtkw.vti_doc(
            shape=mesh.elements_shape,
            extent=[(xmin, xmax) for xmin, xmax in zip(*mesh.bbox)],
            pointdata={
                "x": x.as_array().reshape(mesh.points_shape).ravel(order="F"),
                "z": z.ravel(order="F"),
            },
        ),
        "mesh",
    )


def test_StructMesh():

    # build regular mesh, give the number of edges in each direction
    eps = 1e-10
    # QuadMesh is optional, already the case when calling regular_mesh
    m = QuadMesh(regular_mesh((3, 1), origin=(1.0, 0.0)))
    assert np.all(m.cell_measures.as_array() - 1.0 < eps)
    assert np.all(m.face_measures.as_array() - 1.0 < eps)
    assert np.all(m.face_measures.as_array() == m.edge_measures.as_array())
    assert [1.5, 0.5] in m.cell_isobarycenters.as_array().tolist()
    assert [1.0, 0.5] in m.face_isobarycenters.as_array().tolist()
    assert np.all(m.face_isobarycenters.as_array() == m.edge_isobarycenters.as_array())
    assert np.all(m.cell_centers_of_mass.as_array() == m.cell_isobarycenters.as_array())
    assert np.all(m.face_centers_of_mass.as_array() == m.face_isobarycenters.as_array())
    assert np.all(m.edge_centers_of_mass.as_array() == m.edge_isobarycenters.as_array())
    assert [1.0, 0.0] in m.vertices.as_array().tolist()
    # build scottish mesh, give the distance between 2 nodes in each direction, origin=0
    # QuadMesh is optional, already the case when calling scottish_mesh
    m = QuadMesh(
        scottish_mesh(np.ones(3, dtype=np.double), np.arange(1, 5, dtype=np.double))
    )
    for v in np.arange(1, 5, dtype=np.double):
        assert np.count_nonzero(m.cell_measures == v) == 3
    assert np.all(m.face_measures.as_array() == m.edge_measures.as_array())
    assert np.all(m.face_isobarycenters.as_array() == m.edge_isobarycenters.as_array())
    assert np.all(
        m.face_centers_of_mass.as_array() == m.edge_centers_of_mass.as_array()
    )
    # identical meshes
    # Mesh1D is optional, already the case when calling scottish_mesh and regular_mesh
    ms = Mesh1D(scottish_mesh(np.ones(5, dtype=np.double)))
    mr = Mesh1D(regular_mesh((5,)))
    assert np.all(ms.vertices.as_array() == mr.vertices.as_array())
    assert np.all(
        ms.cell_isobarycenters.as_array() == mr.cell_isobarycenters.as_array()
    )
    assert np.all(ms.face_measures.as_array() == mr.face_measures.as_array())

    m = CuboidMesh(regular_mesh((4, 2, 3)))
    assert np.all(m.cell_measures.as_array() - 1.0 < eps)
    assert np.all(m.face_measures.as_array() - 1.0 < eps)
    assert np.all(m.edge_measures.as_array() - 1.0 < eps)
