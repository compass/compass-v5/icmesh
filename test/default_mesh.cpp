
#include <catch2/catch_test_macros.hpp>
#include <icmesh/Geomtraits.h>
#include <icmesh/Raw_mesh.h>

TEST_CASE("1D default mesh") {
  using namespace icmesh;
  using stencils = std::vector<std::vector<icus::index_type>>;
  auto data = stencils{{0, 1}, {2, 1}};

  // dimension 1 mesh with 3 vertices and 2 edges
  auto mesh = raw_mesh(3, 2);
  CHECK(mesh.dimension == 1);
  CHECK_FALSE(mesh.has_connectivity<mesh.edge, mesh.node>());
  CHECK_FALSE(mesh.ordered_faces_nodes);
  mesh.set_connectivity<mesh.edge, mesh.node>(data);
  CHECK(mesh.has_connectivity<mesh.edge, mesh.node>());
  auto ExN = mesh.get_connectivity<mesh.edge, mesh.node>();
  for (auto p = begin(data); auto &&edge_nodes : ExN.targets()) {
    // target order is preserved when using Raw_mesh::set_connectivity
    CHECK(std::ranges::equal(*p, edge_nodes));
    ++p;
  }
  CHECK_FALSE(mesh.has_connectivity<mesh.node, mesh.edge>());
  // will be build upon request by transposition
  auto NxE = mesh.get_connectivity<mesh.node, mesh.edge>();
  CHECK(mesh.has_connectivity<mesh.node, mesh.edge>());
}

TEST_CASE("3D cart2default mesh") {
  // creates a 3D cartesian grid with (N segments, N+1 nodes) ** 3
  std::size_t N = 8;
  auto unit_step = icmesh::Constant_axis_offset{0., N, 1. / N};
  // creates a regular mesh with geometry utils methodes
  auto cm = icmesh::mesh(icmesh::lattice(unit_step, unit_step, unit_step));
  CHECK(cm.has_connectivity<cm.edge, cm.node>());

  // creates the 3D default type corresponding mesh
  // creates new mesh elements iset to build another ISet tree
  auto dm = icmesh::mesh(cm.nodes.size(), cm.edges.size(), cm.faces.size(),
                         cm.cells.size());
  // init the connectivities: conn<N+1, N>
  dm.set_connectivity<dm.edge, dm.node>(
      cm.get_connectivity<cm.edge, cm.node>().targets());
  // à comprendre !!!
  // CHECK_FALSE(dm.get_connectivity<dm.edge, dm.node>().has_sorted_targets());
  dm.set_connectivity<dm.face, dm.edge>(
      cm.get_connectivity<cm.face, cm.edge>().targets());
  dm.set_connectivity<dm.cell, dm.face>(
      cm.get_connectivity<cm.cell, cm.face>().targets());
  // it is possible to get other connectivities
  CHECK_FALSE(dm.has_connectivity<dm.node, dm.edge>());
  dm.get_connectivity<dm.node, dm.edge>();
  dm.get_connectivity<dm.face, dm.node>();
  CHECK(dm.has_connectivity<dm.node, dm.edge>());

  // init the vertices coordinates (copy values from cart mesh)
  std::copy(std::begin(cm.vertices), std::end(cm.vertices),
            dm.vertices.values.get());
  // compass::utils::Pretty_printer print;
  // for (auto v : dm.vertices) print(v);

  auto eps = 1e-10;
  // check some geometries informations
  CHECK(abs(dm.get_measures<dm.cell>()(0) - std::pow(1. / float(N), 3)) < eps);
  CHECK(abs(dm.get_measures<dm.edge>()(3) - 1. / float(N)) < eps);
}
