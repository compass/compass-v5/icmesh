import numpy as np
import vtkwriters as vtkw
from icmesh import regular_mesh, Connected_components  # connected meshes
from icmesh.dump import write_mesh, faces_consecutive_nodes


# write the set of faces
def write_gamma_mesh(filename, mesh, faces, ofmt="binary"):

    # write vtu mesh file with properties
    vertices = mesh.vertices.as_array()
    # build 3D tabular with, for each cell the list of faces nodes in consecutive order
    # cells = build_Kfaces_nodes(mesh)
    faces_nodes = faces_consecutive_nodes(mesh)
    assert faces.parent() == mesh.faces
    cells = [[faces_nodes[f]] for f in faces.mapping()]

    vtkw.write_vtu(
        vtkw.polyhedra_vtu_doc(
            vertices,
            cells,
            ofmt=ofmt,
        ),
        str(filename),
    )


def connected_test(original_mesh, gamma):
    connected_meshes = Connected_components(original_mesh, gamma)
    mesh = connected_meshes.mesh
    gamma = connected_meshes.gamma
    color_cells = np.zeros(mesh.cells.size())
    for f in mesh.faces:
        cells = mesh.faces_cells.targets_by_source(f)
        if cells.size == 1:
            color_cells[cells] = 1

    write_mesh("main_mesh.vtu", mesh, celldata={"boundary cells": color_cells})
    write_gamma_mesh("gamma.vtu", mesh, gamma)
    return color_cells


def test_connected_comp0():
    mesh = regular_mesh((2, 2, 2))
    # cannot extract with empty gamma faces
    try:
        gamma = mesh.faces.extract([])
        color_cells = connected_test(mesh, gamma)
    except:
        pass
    gamma = mesh.faces.extract([1])
    color_cells = connected_test(mesh, gamma)
    # only boundary cells because the mesh is too small
    assert (color_cells == 0).sum() == 0


def test_connected_comp1():
    mesh = regular_mesh((7, 4, 3))
    gamma = mesh.faces.extract([0, 39, 40])
    color_cells = connected_test(mesh, gamma)
    assert (color_cells == 0).sum() == 8


def test_connected_comp2():
    mesh = regular_mesh((7, 4, 3))
    gamma = mesh.faces.extract([42, 43, 67])
    color_cells = connected_test(mesh, gamma)
    assert (color_cells == 0).sum() == 6


def test_connected_comp3():
    mesh = regular_mesh((7, 4, 3))
    gamma = mesh.faces.extract([39, 40, 67, 109])
    color_cells = connected_test(mesh, gamma)
    assert (color_cells == 0).sum() == 6


def test_connected_comp4():
    mesh = regular_mesh((7, 4, 3))
    # gamma is a whole x-plane
    gamma = mesh.faces.extract(np.arange(36, 48))
    color_cells = connected_test(mesh, gamma)
    assert (color_cells == 0).sum() == 6


def test_connected_comp5():
    mesh = regular_mesh((7, 4, 5))
    gamma = mesh.faces.extract([85, 86, 87, 270, 271, 272])
    color_cells = connected_test(mesh, gamma)
    assert (color_cells == 0).sum() == 24


def test_connected_comp6():
    mesh = regular_mesh((6, 6, 6))
    gamma = mesh.faces.extract(np.arange(108, 144))
    color_cells = connected_test(mesh, gamma)
    assert (color_cells == 0).sum() == 32


def test_connected_comp7():
    mesh = regular_mesh((3, 2, 2))
    gamma = mesh.faces
    color_cells = connected_test(mesh, gamma)
    # only boundary cells
    assert (color_cells == 0).sum() == 0
