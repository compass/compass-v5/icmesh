#include <catch2/catch_test_macros.hpp>
// #include <concepts>
#include <icmesh/Mesh_elements.h>
#include <icmesh/Structured_connectivity.h>

#include "dump_and_check_utils.h"

template <class... N>
  requires(std::conjunction_v<std::is_integral<N>...>)
void build_and_check(const N &...n) {
  using namespace icmesh;
  auto con = structured_connectivity(n...);
  auto mesh = mesh_elements(con.elements);
  dump_and_check(mesh);
}

TEST_CASE("test mesh elements") {
  // a 1D mesh with the answer to the univers
  build_and_check(42);
  // a chessboard
  build_and_check(8, 8);
  // a cube
  build_and_check(1, 1, 1);
}
