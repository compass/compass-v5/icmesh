#include <catch2/catch_test_macros.hpp>
#include <concepts>
#include <iostream>
#include <ranges>
#include <vector>

#include <icmesh/Axis_utils.h>

void dump(std::ranges::range auto &&R) {
  for (auto &&x : R)
    std::cout << x << " ";
  std::cout << std::endl;
}

TEST_CASE("test axis utils") {

  using namespace icmesh;

  std::cout << "combinations 1,1" << std::endl;
  for (auto &&a : compass::utils::combinations<1, 1>{}) {
    dump(a);
    CHECK(a[0] == 0);
  }

  int res = 0;
  std::cout << "combinations 1,2" << std::endl;
  for (auto &&a : compass::utils::combinations<1, 2>{}) {
    dump(a);
    CHECK(a[0] == res);
    res += 1;
  }

  {
    using combination = compass::utils::combinations<2, 3>;
    using iterator = combination::iterator;
    std::vector<iterator> combits;
    for (auto p = combination{}.begin(); p != combination{}.end(); ++p) {
      combits.emplace_back(p);
    }
    std::vector<std::vector<int>> res2 = {{0, 1}, {0, 2}, {1, 2}};
    int i = 0;
    std::cout << "iterators" << std::endl;
    for (auto &&it : combits) {
      dump(*it);
      CHECK(((*it)[0] == res2[i][0] && (*it)[1] == res2[i][1]));
      i += 1;
    }
  }

  std::cout << "combinations 2,2" << std::endl;
  for (auto &&a : compass::utils::combinations<2, 2>{}) {
    dump(a);
    CHECK((a[0] == 0 && a[1] == 1));
  }
  std::cout << "blocked axis 1,2" << std::endl;
  res = 1;
  for (auto &&info : blocked_axis<1, 2>) {
    std::cout << info << std::endl;
    CHECK(info == (std::size_t)res);
    res += 1;
  }
  std::cout << "blocked axis 2,2" << std::endl;
  for (auto p = blocked_axis<2, 2>.begin(); p != blocked_axis<2, 2>.end();
       ++p) {
    std::cout << *p << " with underlying combination: ";
    dump(*(p.combination));
    CHECK(*p == 3);
    CHECK(((*(p.combination))[0] == 0 && (*(p.combination))[1] == 1));
  }
  std::cout << "blocked axis 2,2" << std::endl;
  for (auto &&info : blocked_axis<2, 2>) {
    std::cout << info << std::endl;
  }
}
