import numpy as np
from icmesh import (
    regular_mesh,
    scottish_mesh,
)


def step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def check_cell_isobarycenters(mesh):
    centers = mesh.cell_isobarycenters
    other_centers = mesh.isobarycenters(mesh.cells)
    assert (centers.as_array() == other_centers.as_array()).all()


def check_face_isobarycenters(mesh):
    centers = mesh.face_isobarycenters
    other_centers = mesh.isobarycenters(mesh.faces)
    assert (centers.as_array() == other_centers.as_array()).all()


def check_cell_centers_of_mass(mesh):
    centers = mesh.cell_centers_of_mass
    other_centers = mesh.centers_of_mass(mesh.cells)
    assert (centers.as_array() == other_centers.as_array()).all()


def check_face_centers_of_mass(mesh):
    centers = mesh.face_centers_of_mass
    other_centers = mesh.centers_of_mass(mesh.faces)
    assert (centers.as_array() == other_centers.as_array()).all()


def check_cell_measures(mesh):
    measures = mesh.cell_measures
    other_measures = mesh.measures(mesh.cells)
    assert (measures.as_array() == other_measures.as_array()).all()


def check_face_measures(mesh):
    measures = mesh.face_measures
    other_measures = mesh.measures(mesh.faces)
    assert (measures.as_array() == other_measures.as_array()).all()


def check_all(mesh):
    check_cell_isobarycenters(mesh)
    check_face_isobarycenters(mesh)
    check_cell_centers_of_mass(mesh)
    check_face_centers_of_mass(mesh)
    check_cell_measures(mesh)
    check_face_measures(mesh)


def test_1D_struct_geomtraits():

    Nz = 6
    Lz = 59.3

    print("--------------------------------------")
    print("1D regular grid (Mesh1D)")
    mesh = regular_mesh((Nz,), extent=(Lz,))
    check_all(mesh)

    print("--------------------------------------")
    print("1D scottish grid (Mesh1D)")
    mesh = scottish_mesh(step(Nz, Lz))
    check_all(mesh)


def test_2D_struct_geomtraits():

    Nx = 5
    Nz = 6
    Lx = 23.15
    Lz = 59.3

    print("--------------------------------------")
    print("2D regular grid")
    mesh = regular_mesh((Nx, Nz), extent=(Lx, Lz))
    check_all(mesh)

    print("--------------------------------------")
    print("2D scottish grid")
    mesh = scottish_mesh(step(Nx, Lx), step(Nz, Lz))
    check_all(mesh)


def test_3D_struct_geomtraits():

    Nx = 5
    Ny = 7
    Nz = 6
    Lx = 23.15
    Ly = 5.79
    Lz = 59.3

    print("--------------------------------------")
    print("3D regular grid")
    mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    check_all(mesh)

    print("--------------------------------------")
    print("3D scottish grid")
    mesh = scottish_mesh(step(Nx, Lx), step(Ny, Ly), step(Nz, Lz))
    check_all(mesh)
