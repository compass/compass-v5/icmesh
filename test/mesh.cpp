#include <catch2/catch_test_macros.hpp>
// #include <concepts>
#include <compass_cxx_utils/array_utils.h>
#include <compass_cxx_utils/pretty_print.h>
#include <icmesh/Geomtraits.h>

#include "dump_and_check_utils.h"

template <class... N>
  requires(std::conjunction_v<std::is_integral<N>...>)
void build_and_check(const N &...n) {
  auto con = icmesh::structured_connectivity(n...);
  auto mesh = icmesh::mesh(con);
  dump_and_check(mesh);
}

TEST_CASE("test mesh") {
  compass::utils::Pretty_printer print;

  SECTION("various tests") {
    // a 1D mesh with the answer to the univers
    build_and_check(42);
    // a chessboard
    build_and_check(8, 8);
    // a cube
    build_and_check(1, 1, 1);
  }

  SECTION("test unit cube") {
    auto unit_grid = icmesh::Constant_axis_offset{0., 1, 1.};
    auto mesh = icmesh::mesh(icmesh::lattice(unit_grid, unit_grid, unit_grid));

    print("== a unit cube");
    print("the mesh has", mesh.dimension, "dimensions");
    print(mesh.nodes.size(), "nodes");
    print(mesh.edges.size(), "edges");
    print(mesh.faces.size(), "faces");
    print(mesh.cells.size(), "cells");
    print("nodes iset is :", mesh.nodes);
    print("mesh iset is :", mesh.joined_elements);

    CHECK(mesh.dimension == 3);
    CHECK(mesh.nodes.size() == 8);
    CHECK(mesh.edges.size() == 12);
    CHECK(mesh.faces.size() == 6);
    CHECK(mesh.cells.size() == 1);
    CHECK(mesh.joined_elements.size() == 27);

    for (auto &&[n, P] : mesh.vertices.enumerate()) {
      print("Point(", P, ")");
      CHECK(P[0] == (int)n / 4);
      CHECK(P[1] == ((int)n / 2) % 2);
      CHECK(P[2] == n % 2);
    }

    auto edges_nodes = mesh.get_connectivity<mesh.edge, mesh.node>();
    for (auto &&[e, nodes] : edges_nodes.targets_by_source()) {
      CHECK(nodes.size() == 2);
      if (e < 4) {
        CHECK((nodes[0] == 2 * e && nodes[1] == 2 * e + 1));
      } else if (e < 8) {
        CHECK((nodes[0] == 2 * (e - 4) - e % 2 &&
               nodes[1] == 2 * (e - 3) - e % 2));
      } else {
        CHECK((nodes[0] == e - 8 && nodes[1] == e - 4));
      }
    }

    CHECK_FALSE(mesh.ordered_faces_nodes);

    auto faces_nodes = mesh.get_connectivity<mesh.face, mesh.node>();
    for (auto &&[f, nodes] : faces_nodes.targets_by_source()) {
      CHECK(nodes.size() == 4);
      if (f < 2) {
        CHECK((nodes[0] == 4 * f && nodes[1] == 4 * f + 1 &&
               nodes[2] == 4 * f + 2 && nodes[3] == 4 * f + 3));
      } else if (f < 4) {
        CHECK((nodes[0] == 2 * (f % 2) && nodes[1] == 2 * (f % 2) + 1 &&
               nodes[2] == 2 * (f % 2) + 4 && nodes[3] == 2 * (f % 2) + 5));
      } else {
        CHECK((nodes[0] == f - 4 && nodes[1] == f - 2 && nodes[2] == f &&
               nodes[3] == f + 2));
      }
    }

    auto faces_edges = mesh.get_connectivity<mesh.face, mesh.edge>();
    for (auto &&[f, edges] : faces_edges.targets_by_source()) {
      CHECK(edges.size() == 4);
      if (f < 2) {
        CHECK((edges[0] == 2 * f && edges[1] == 2 * f + 1 &&
               edges[2] == 2 * f + 4 && edges[3] == 2 * f + 5));
      } else if (f < 4) {
        CHECK((edges[0] == f - 2 && edges[1] == f &&
               edges[2] == f + 6 + f % 2 && edges[3] == f + 7 + f % 2));
      } else {
        CHECK((edges[0] == f && edges[1] == f + 2 && edges[2] == f + 4 &&
               edges[3] == f + 6));
      }
    }

    auto cells_nodes = mesh.get_connectivity<mesh.cell, mesh.node>();
    auto nodes_cells = mesh.get_connectivity<mesh.node, mesh.cell>();
    CHECK(cells_nodes.transpose().is_same(nodes_cells));
    for (auto &&cells : nodes_cells.targets())
      CHECK((cells.size() == 1 && cells[0] == 0));
  }

  SECTION("test scottish 2D grid") {
    using Steps = typename icmesh::Specific_axis_offset<>;
    std::vector<double> steps_Ox{0.1, 1., 0.5};
    std::vector<double> steps_Oy{1., 2.};

    auto mesh =
        icmesh::mesh(icmesh::lattice(Steps{steps_Ox}, Steps{Steps{steps_Oy}}));

    print("== a scottish mesh");
    print("the mesh has", mesh.dimension, " dimensions");
    print(mesh.nodes.size(), "nodes");
    print(mesh.edges.size(), "edges");
    print(mesh.faces.size(), "faces");
    print(mesh.cells.size(), "cells");
    print(mesh.joined_elements.size(), "elements in total");

    CHECK(mesh.dimension == 2);
    CHECK(mesh.edges == mesh.faces);
    CHECK(mesh.nodes.size() == 12);
    CHECK(mesh.edges.size() == 17);
    CHECK(mesh.cells.size() == 6);
    CHECK(mesh.joined_elements.size() == 35);

    CHECK_FALSE(mesh.ordered_faces_nodes);

    std::vector<double> point_x{0.0, 0.1, 1.1, 1.6};
    std::vector<double> point_y{0.0, 1.0, 3.0};
    for (auto &&[n, P] : mesh.vertices.enumerate()) {
      print("Point(", P, ")");
      CHECK((P[0] == point_x[(int)n / 3] && P[1] == point_y[n % 3]));
    }

    // we allow operations between vertices (i.e. array)
    using namespace compass::utils::array_operators;

    auto offset = mesh.build_vertex(1, 2);
    for (auto &&P : mesh.vertices)
      P += offset;

    print("After translation...");
    for (auto &&[n, P] : mesh.vertices.enumerate()) {
      print("Point(", P, ")");
      CHECK((P[0] == point_x[(int)n / 3] + offset[0] &&
             P[1] == point_y[n % 3] + offset[1]));
    }

    // origin
    auto O = mesh.build_vertex();
    for (auto &&x : O) {
      CHECK(x == 0);
    }
  }

  SECTION("test connectivity") {
    size_t N = 5;
    // creates a 1D grid with N segments, N+1 nodes
    auto unit_grid = icmesh::Constant_axis_offset{0., N, 1. / N};
    auto mesh_1d = icmesh::mesh(icmesh::lattice(unit_grid));
    auto edges_nodes = mesh_1d.get_connectivity<mesh_1d.edge, mesh_1d.node>();
    for (auto &&[e, nodes] : edges_nodes.targets_by_source()) {
      CHECK((nodes.size() == 2 && nodes[0] == e && nodes[1] == e + 1));
    }
    CHECK(edges_nodes.transpose().is_same(
        mesh_1d.get_connectivity<mesh_1d.node, mesh_1d.edge>()));
    auto cells_nodes = mesh_1d.get_connectivity<mesh_1d.cell, mesh_1d.node>();
    CHECK(cells_nodes.transpose().is_same(
        mesh_1d.get_connectivity<mesh_1d.node, mesh_1d.cell>()));
    CHECK(edges_nodes.is_same(cells_nodes)); // in 1D cells = edges
    CHECK_FALSE(mesh_1d.ordered_faces_nodes);
  }
}
