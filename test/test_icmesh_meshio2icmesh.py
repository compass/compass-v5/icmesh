import numpy as np
from icmesh import (
    scottish_mesh,
    meshio2icmesh,
)
from icmesh.dump import write_mesh
from icmesh.check import check_3D_mesh
from pathlib import Path
import meshio

# get current path to create mesh path and output path (mkdir if necessary)
current_path = Path(__file__).parent.absolute()
mesh_path = (current_path / Path("mesh_data")).as_posix()
output_path = current_path / Path("output_data")
output_path.mkdir(exist_ok=True)
output_path = output_path.as_posix()


def test_reload_cartesian():
    mesh = scottish_mesh(
        np.arange(1, 7, dtype=np.double) / 12.0,
        np.arange(1, 5, dtype=np.double) / 4.0,
        np.arange(1, 4, dtype=np.double) / 9.0,
    )

    # create the vtu file (with a cartesian mesh)...
    write_mesh(output_path + "/cart_scottish_mesh.vtu", mesh)
    # ... then reload it as a default mesh (no regular structure)
    # reload using meshio2icmesh
    mesh_file = output_path + "/cart_scottish_mesh.vtu"
    given_mesh = meshio.read(mesh_file)
    return_mesh, cells_data = meshio2icmesh(mesh_file)
    # for v in return_mesh.vertices:
    #     print(v.as_array())
    # faces_nodes are ordered to respect the edges
    assert return_mesh.ordered_faces_nodes
    check_3D_mesh(return_mesh, mesh_type="cuboid", given_mesh=given_mesh)
    # write default mesh
    write_mesh(output_path + "/default_mesh.vtu", return_mesh)


def test_load_tetra():
    # load a tetrahedral mesh
    mesh_file = mesh_path + "/tetrahedral_mesh.vtk"
    given_mesh = meshio.read(mesh_file)

    return_mesh, cells_data = meshio2icmesh(mesh_file)
    # for v in return_mesh.vertices:
    #     print(v.as_array())

    # faces_nodes are ordered to respect the edges
    assert return_mesh.ordered_faces_nodes
    check_3D_mesh(
        return_mesh, ncells=19422, nnodes=4221, mesh_type="tetra", given_mesh=given_mesh
    )

    # write tetrahedral mesh
    write_mesh(output_path + "/tetrahedral_mesh.vtu", return_mesh)


def test_load_hexa():
    # load a hexahedral mesh
    mesh_file = mesh_path + "/cuboid_mesh.vtk"
    given_mesh = meshio.read(mesh_file)

    return_mesh, cells_data = meshio2icmesh(mesh_path + "/cuboid_mesh.vtk")
    # for v in return_mesh.vertices:
    #     print(v.as_array())
    # faces_nodes are ordered to respect the edges
    assert return_mesh.ordered_faces_nodes
    check_3D_mesh(
        return_mesh,
        ncells=12,
        nfaces=52,
        nedges=75,
        nnodes=36,
        mesh_type="cuboid",
        given_mesh=given_mesh,
    )
    # write hexahedral mesh
    write_mesh(output_path + "/cuboid_mesh.vtu", return_mesh)


def test_load_tetra_pyramid():
    # load a tetra-pyramid mesh
    mesh_file = mesh_path + "/tetrahedral_pyramid_mesh.vtk"
    given_mesh = meshio.read(mesh_file)

    return_mesh, cells_data = meshio2icmesh(mesh_path + "/tetrahedral_pyramid_mesh.vtk")
    # for v in return_mesh.vertices:
    #     print(v.as_array())
    # faces_nodes are ordered to respect the edges
    assert return_mesh.ordered_faces_nodes
    check_3D_mesh(return_mesh, ncells=117, nnodes=43, given_mesh=given_mesh)
    # write tetra-pyramid mesh
    write_mesh(output_path + "/tetrahedral_pyramid_mesh.vtu", return_mesh)


def test_load_tetra_pyramid_hexa():
    # load a tetra-pyramid_hexa mesh
    mesh_file = mesh_path + "/tetra_pyramids_cuboid_mesh.vtk"
    given_mesh = meshio.read(mesh_file)

    return_mesh, cells_data = meshio2icmesh(
        mesh_path + "/tetra_pyramids_cuboid_mesh.vtk"
    )
    # for v in return_mesh.vertices:
    #     print(v.as_array())
    # faces_nodes are ordered to respect the edges
    assert return_mesh.ordered_faces_nodes
    check_3D_mesh(return_mesh, ncells=3695, nnodes=1002, given_mesh=given_mesh)

    # check cell_data
    # upper mesh cell center z > 0.25 with tag 57
    cell_centers = return_mesh.cell_isobarycenters.as_array()
    assert all(cells_data["CellEntityIds"][cell_centers[:, 2] > 0.25] == 57)

    # lower mesh cell center z < 0.25 with tag 56
    assert all(cells_data["CellEntityIds"][cell_centers[:, 2] < 0.25] == 56)

    # check
    # write tetra-pyramid_hexa mesh
    write_mesh(
        output_path + "/tetra_pyramids_cuboid_mesh.vtu",
        return_mesh,
        celldata=cells_data,
    )


def test_load_wadge():
    # load a triangle prisms (wedge) mesh
    mesh_file = mesh_path + "/wedge_mesh.vtk"
    given_mesh = meshio.read(mesh_file)

    return_mesh, cells_data = meshio2icmesh(mesh_path + "/wedge_mesh.vtk")
    # for v in return_mesh.vertices:
    #     print(v.as_array())
    # faces_nodes are ordered to respect the edges
    assert return_mesh.ordered_faces_nodes
    check_3D_mesh(
        return_mesh, ncells=45, nfaces=141, nedges=147, nnodes=52, given_mesh=given_mesh
    )
    # write triangle prisms (wedge) mesh
    write_mesh(output_path + "/wedge_mesh.vtu", return_mesh, celldata=cells_data)
