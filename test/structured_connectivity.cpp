
#include <catch2/catch_test_macros.hpp>
#include <icmesh/Structured_connectivity.h>
#include <iostream>

void dump(auto &&R) {
  for (auto &&x : R)
    std::cout << x << " ";
  std::cout << std::endl;
}

void dump_grid(const auto &g) {
  const auto dim = g.dimension;
  std::cout << "== Grid of dimension " << dim << " ==" << std::endl;
  for (size_t i = 0; i <= dim; ++i)
    std::cout << g.elements[i].size() << " elements of dimension " << i
              << std::endl;
}

template <std::size_t edim> void dump_grid_elements(const auto &g) {
  for (auto &&i : g.elements[edim]) {
    auto e = g.template element<edim>(i);
    std::cout << "element (dim " << edim << ") " << i << " with indices: ";
    dump(e.indices);
    CHECK(g.index(e) == i);
  }
}

template <std::size_t i, std::size_t j, typename StCon>
auto connectivity(const StCon &sc) {
  const char *name[4] = {"nodes", "edges", "faces", "cells"};
  std::cout << "connectivity " << name[i] << " x " << name[j] << std::endl;
  auto con = sc.template connectivity<i, j>();
  for (auto &&neighbors : con.targets())
    dump(neighbors);
  return con;
}

void test_cube() {

  auto g = icmesh::structured_connectivity(1, 1, 1);

  dump_grid(g);

  dump_grid_elements<0>(g);
  dump_grid_elements<1>(g);
  dump_grid_elements<2>(g);
  dump_grid_elements<3>(g);

  auto &&ExN = connectivity<1, 0>(g);
  auto &&FxN = connectivity<2, 0>(g);
  auto &&FxE = connectivity<2, 1>(g);
  auto &&CxN = connectivity<3, 0>(g);
  auto &&CxE = connectivity<3, 1>(g);
  auto &&CxF = connectivity<3, 2>(g);

  CHECK(ExN.source() == g.elements[1]);
  CHECK(FxN.source() == g.elements[2]);
  CHECK(FxE.source() == g.elements[2]);
  CHECK(CxN.source() == g.elements[3]);
  CHECK(CxE.source() == g.elements[3]);
  CHECK(CxF.source() == g.elements[3]);

  CHECK(ExN.target() == g.elements[0]);
  CHECK(FxN.target() == g.elements[0]);
  CHECK(FxE.target() == g.elements[1]);
  CHECK(CxN.target() == g.elements[0]);
  CHECK(CxE.target() == g.elements[1]);
  CHECK(CxF.target() == g.elements[2]);
}

TEST_CASE("test structured connectivity") {

  using namespace icmesh;

  // creates a 1D grid with 10 elements (segments)
  std::cout << std::endl;
  auto g1 = structured_connectivity(10);
  dump_grid(g1);
  std::cout << std::endl;

  // create a 2D grid with 2x2 elements
  auto g2 = structured_connectivity(2, 2);
  dump_grid(g2);
  dump_grid_elements<0>(g2);
  dump_grid_elements<1>(g2);
  dump_grid_elements<2>(g2);
  std::cout << "connectivity d by d-1 (d=dim=2, cells x faces = edges)"
            << std::endl;
  auto CxF = g2.connectivity<2, 1>();
  for (auto &&neighbors : CxF.targets())
    dump(neighbors);
  std::cout << "connectivity d by d-1 (d=1, faces = edges x vertices)"
            << std::endl;
  auto FxV = g2.connectivity<1, 0>();
  for (auto &&neighbors : FxV.targets())
    dump(neighbors);

  test_cube();
}
