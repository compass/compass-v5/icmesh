#pragma once

#include <cassert>
#include <concepts>
#include <numeric>
#include <ranges>
#include <tuple>
#include <utility>
#include <vector>

#include <compass_cxx_utils/array_utils.h>

namespace icmesh {

template <typename scalar = double> class Constant_axis_offset {
  scalar O;
  scalar dx;
  std::size_t n;

public:
  using scalar_type = scalar;
  Constant_axis_offset() = default;
  Constant_axis_offset(const std::size_t nb_steps)
      : O{0}, dx{1.}, n{nb_steps + 1} {}
  Constant_axis_offset(const std::size_t nb_steps, const scalar &step)
      : O{0}, dx{step}, n{nb_steps + 1} {
    assert(dx > 0);
  }
  Constant_axis_offset(const scalar &origin, const std::size_t nb_steps,
                       const scalar &step)
      : O{origin}, dx{step}, n{nb_steps + 1} {
    assert(dx > 0);
  }
  Constant_axis_offset(Constant_axis_offset &) = default;
  Constant_axis_offset(Constant_axis_offset &&) = default;
  Constant_axis_offset &operator=(Constant_axis_offset &) = default;
  Constant_axis_offset &operator=(Constant_axis_offset &&) = default;
  auto operator()(const std::integral auto i) const {
    assert(std::cmp_greater_equal(i, 0) && std::cmp_less(i, size()));
    return O + i * dx;
  }
  auto size() const { return n; }
  auto origin() const { return O; }
  auto extent() const { return (n - 1) * dx; }
};

template <typename scalar = double>
struct Specific_axis_offset : private std::vector<scalar> {
  using base = std::vector<scalar>;

public:
  using scalar_type = scalar;
  Specific_axis_offset() = default;

  template <std::ranges::range Steps>
  void _build_steps(const scalar &origin, const Steps &steps) {
    this->reserve(std::ranges::size(steps) + 1);
    this->emplace_back(origin);
    for (auto &&dx : steps) {
      assert(dx > 0);
      this->emplace_back(this->back() + dx);
    }
    this->shrink_to_fit();
  }
  template <std::ranges::range Steps> Specific_axis_offset(const Steps &steps) {
    _build_steps(0, steps);
  };
  template <std::ranges::range Steps>
  Specific_axis_offset(const scalar &origin, const Steps &steps) {
    _build_steps(origin, steps);
  };
  Specific_axis_offset(Specific_axis_offset &) = default;
  Specific_axis_offset(Specific_axis_offset &&) = default;
  Specific_axis_offset &operator=(Specific_axis_offset &) = default;
  Specific_axis_offset &operator=(Specific_axis_offset &&) = default;
  auto operator()(const std::integral auto i) const {
    assert(std::cmp_greater_equal(i, 0) && std::cmp_less(i, size()));
    return this->base::operator[](i);
  }
  auto size() const { return this->base::size(); }
  auto origin() const { return base::front(); }
  auto extent() const { return base::back() - base::front(); }
};

template <typename... Axis>
// FIXME: requires all scalar type are the same + defintiion of scalar type
class Lattice {

public:
  static constexpr std::size_t dimension = sizeof...(Axis);
  using scalar = double;
  using Vertex = std::array<scalar, dimension>;

private:
  using Axis_tuple = std::tuple<Axis...>;
  Axis_tuple axis;

  template <typename T> struct _get;
  template <std::size_t... i>
    requires(sizeof...(i) == dimension)
  struct _get<std::index_sequence<i...>> {
    template <std::integral... K>
      requires(sizeof...(K) == dimension)
    static auto apply(const Axis_tuple &axis, const K &...k) {
      return std::array<scalar, dimension>{std::get<i>(axis)(k)...};
    }
    template <std::integral T>
    static auto apply(const Axis_tuple &axis,
                      const std::array<T, dimension> &a) {
      return Vertex{std::get<i>(axis)(a[i])...};
    }
    static auto nb_points_along_axis(const Axis_tuple &axis) {
      return std::array<std::size_t, dimension>{std::get<i>(axis).size()...};
    }
    static auto origin(const Axis_tuple &axis) {
      return Vertex{std::get<i>(axis).origin()...};
    }
    static auto extent(const Axis_tuple &axis) {
      return Vertex{std::get<i>(axis).extent()...};
    }
  };

public:
  Lattice() = default;
  Lattice(Lattice &) = default;
  Lattice(Lattice &&) = default;
  Lattice &operator=(Lattice &) = default;
  Lattice &operator=(Lattice &&) = default;
  template <typename... Ts>
  Lattice(const std::tuple<Ts...> &axis_tuple) : axis{axis_tuple} {}
  template <typename... Ts>
  Lattice(std::tuple<Ts...> &&axis_tuple)
      : axis{std::forward<std::tuple<Ts...>>(axis_tuple)} {}

  template <std::integral... I>
    requires(sizeof...(I) == dimension)
  std::array<scalar, dimension> operator()(const I &...i) const {
    return _get<std::make_index_sequence<dimension>>::apply(axis, i...);
  }
  template <std::integral T>
  auto operator()(const std::array<T, dimension> &indices) const {
    return _get<std::make_index_sequence<dimension>>::apply(axis, indices);
  }
  auto points_shape() const {
    return _get<std::make_index_sequence<dimension>>::nb_points_along_axis(
        axis);
  }
  auto elements_shape() const {
    auto shape =
        _get<std::make_index_sequence<dimension>>::nb_points_along_axis(axis);
    for (auto &&n : shape) {
      assert(n > 1);
      --n;
    }
    return shape;
  }
  auto origin() const {
    return _get<std::make_index_sequence<dimension>>::origin(axis);
  }
  auto origin_opposite() const {
    using namespace compass::utils::array_operators;
    return origin() + extent();
  }
  auto extent() const {
    return _get<std::make_index_sequence<dimension>>::extent(axis);
  }
};

// template deduction guide
template <typename... Ts> Lattice(std::tuple<Ts...> &&) -> Lattice<Ts...>;
template <typename... Ts> Lattice(const std::tuple<Ts...> &) -> Lattice<Ts...>;

template <typename... Ts> auto lattice(Ts &&...ts) {
  return Lattice{std::make_tuple(std::forward<Ts>(ts)...)};
}

template <typename T> struct is_lattice {
  static constexpr bool value = false;
};

template <typename... Axis> struct is_lattice<Lattice<Axis...>> {
  static constexpr bool value = true;
};

template <typename T>
inline constexpr bool is_lattice_v = is_lattice<std::decay_t<T>>::value;

} // namespace icmesh
