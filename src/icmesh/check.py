import numpy as np
from compass_utils import messages


# test coherence cells_nodes, cells_edges, and faces_nodes
def check_3D_mesh(
    mesh,
    nnodes=None,
    nedges=None,
    nfaces=None,
    ncells=None,
    mesh_type=None,
    given_mesh=None,
):
    eps = 1e-10
    # check cells_nodes
    c2n = mesh.cells_faces.compose(mesh.faces_nodes)
    assert all(
        [
            all(np.sort(t) == l)
            for t, l in zip(mesh.cells_nodes.targets(), c2n.targets())
        ]
    )

    # check cells_nodes
    c2n = mesh.cells_edges.compose(mesh.edges_nodes)
    assert all(
        [
            all(np.sort(t) == l)
            for t, l in zip(mesh.cells_nodes.targets(), c2n.targets())
        ]
    )

    # check cells_edges
    # if cells_edges are construct using cells_faces and faces_edges, this test should be correct
    c2e = mesh.cells_faces.compose(mesh.faces_edges)
    assert all(
        [
            all(np.sort(t) == l)
            for t, l in zip(mesh.cells_edges.targets(), c2e.targets())
        ]
    )

    # check faces_nodes
    f2n = mesh.faces_edges.compose(mesh.edges_nodes)
    assert all(
        [
            all(np.sort(t) == l)
            for t, l in zip(mesh.faces_nodes.targets(), f2n.targets())
        ]
    )

    # check numbers of nodes
    assert mesh.cells_nodes.image() == mesh.faces_nodes.image()
    assert mesh.cells_nodes.image() == mesh.edges_nodes.image()
    if nnodes is not None:
        assert mesh.cells_nodes.image().size() == nnodes

    # check numbers of edges
    assert mesh.cells_edges.image() == mesh.faces_edges.image()
    assert mesh.cells_edges.image() == mesh.nodes_edges.image()
    if nedges is not None:
        assert mesh.cells_edges.image().size() == nedges

    # edges should have unique centers
    assert len(mesh.edge_isobarycenters.as_array()) == len(
        np.unique(mesh.edge_isobarycenters.as_array(), axis=0)
    )

    # check numbers of faces
    assert mesh.cells_faces.image() == mesh.nodes_faces.image()
    assert mesh.nodes_faces.image() == mesh.edges_faces.image()
    if nfaces is not None:
        assert mesh.cells_faces.image().size() == nfaces

    # faces should have unique centers
    assert len(mesh.face_isobarycenters.as_array()) == len(
        np.unique(mesh.face_isobarycenters.as_array(), axis=0)
    )

    # check numbers of cells
    assert mesh.faces_cells.image() == mesh.nodes_cells.image()
    assert mesh.nodes_cells.image() == mesh.edges_cells.image()
    if ncells is not None:
        assert mesh.faces_cells.image().size() == ncells

    # cells should have unique centers
    assert len(mesh.cell_isobarycenters.as_array()) == len(
        np.unique(mesh.cell_isobarycenters.as_array(), axis=0)
    )

    # check cells_centers
    if given_mesh is not None:
        cells_centers = get_cells_centers(given_mesh)
        assert (abs(mesh.cell_isobarycenters.as_array() - cells_centers) < eps).all()

    # check connectivity matrix for tetra mesh
    if (mesh_type == "tetra") | (mesh_type == "tet"):
        assert mesh.edges_nodes.targets_size() == 2
        assert mesh.faces_nodes.targets_size() == 3
        assert mesh.faces_edges.targets_size() == 3
        assert mesh.cells_nodes.targets_size() == 4
        assert mesh.cells_edges.targets_size() == 6
        assert mesh.cells_faces.targets_size() == 4

    # check connectivity matrix for cuboid mesh
    if mesh_type == "cuboid":
        assert mesh.edges_nodes.targets_size() == 2
        assert mesh.faces_nodes.targets_size() == 4
        assert mesh.faces_edges.targets_size() == 4
        assert mesh.cells_nodes.targets_size() == 8
        assert mesh.cells_edges.targets_size() == 12
        assert mesh.cells_faces.targets_size() == 6


# calculate cells centers of given_mesh (mesh returned by meshio.read(filename))
def get_cells_centers(given_mesh):
    cells_centers = []
    for vtktype, c_nodes in given_mesh.cells_dict.items():
        if (
            (vtktype == "vertex")
            | (vtktype == "line")
            | (vtktype == "triangle")
            | (vtktype == "quad")
        ):
            messages.warning(
                f"in check mesh, cells centers for type {vtktype} are ignored in get_cells_centers, done only for 3D cells"
            )
        else:
            for nodes in c_nodes:
                # if vtktype == "polyhedron8":
                cells_centers.append(
                    np.mean(given_mesh.points[np.unique(nodes)], axis=0)
                )
                # else:
                # cells_centers.append(np.mean(given_mesh.points[nodes], axis=0))
    return np.asarray(cells_centers)
