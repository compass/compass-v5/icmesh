#pragma once

#include <fstream>

namespace icmesh {

int _read_size(auto &&streamf) {
  std::string line;
  std::getline(streamf, line); // remove line
  std::getline(streamf, line);
  return stoi(line);
}

auto _read_connectivity(auto &&streamf, const std::string &name,
                        int source_size, int target_size = -1) {
  std::string line;
  std::stringstream ss;
  std::getline(streamf, line);
  ss = std::stringstream{line}; // decompose line
  // get file name of the connectivity
  std::string conn_name;
  ss >> conn_name;
  if (conn_name != name)
    throw std::runtime_error("Error in cells_faces connectivity, got " +
                             conn_name + " instead of " + name);

  // get size of the connectivity
  std::vector<std::vector<icus::index_type>> neighbourhood;
  neighbourhood.reserve(source_size);

  int size;
  ss >> size;
  if (size != source_size)
    throw std::runtime_error(name + " size not compatible with source size");
  // the target size is known so it is not precised at the beginning of each
  // line
  assert(target_size <= 5);
  if (target_size > 0) {
    for (int s = 0; s < source_size; ++s) {
      std::getline(streamf, line);
      ss = std::stringstream{line}; // decompose line
      // each line contains target_size indices
      std::vector<icus::index_type> neigh(target_size);
      for (int i = 0; i < target_size; ++i) {
        ss >> neigh[i];
        // first index is 0 in icus, 1 in the mesh file
        neigh[i] -= 1;
      }
      neighbourhood.push_back(neigh);
    }
  } else {
    for (int s = 0; s < source_size; ++s) {
      std::getline(streamf, line);
      ss = std::stringstream{line}; // decompose line
      // each line contains 6 indices, the first coef is
      // the nb of targets for this source,
      // the targets can be written in several lines
      ss >> size;
      std::vector<icus::index_type> neigh(size);
      // to know the nb input lines to read for this conn (if too many targets,
      // the conn indices are written over several lines)
      int line_pos = 1;
      for (int i = 0; i < size; ++i) {
        ss >> neigh[i];
        // first index is 0 in icus, 1 in the mesh file
        neigh[i] -= 1;
        // if the connectivity is too long, it is written in several lines
        // each line contains 6 indices
        if ((++line_pos) % 6 == 0 && size >= line_pos) {
          std::getline(streamf, line);
          ss = std::stringstream{line}; // decompose line
        }
      }
      neighbourhood.push_back(neigh);
    }
  }
  return neighbourhood;
}

// template <typename Vertex>
auto read_fvca6(const std::string &filename) {

  std::ifstream f;
  f.open(filename);
  std::stringstream ss;

  // after open, check f and throw std::system_error with the errno
  if (!f)
    throw std::system_error(errno, std::system_category(),
                            "failed to open " + filename);

  std::string line;
  // avoid the first 9 lines
  for (int i = 0; i < 8; ++i)
    std::getline(f, line);

  auto n_nodes = _read_size(f);
  auto n_cells = _read_size(f);
  auto n_faces = _read_size(f);
  auto n_edges = _read_size(f);

  // creates the mesh object (default type, no regular structure)
  auto mesh = icmesh::mesh(n_nodes, n_edges, n_faces, n_cells);

  // read vertices coordinates
  std::getline(f, line); // remove line
  for (auto &&coords : mesh.vertices) {
    std::getline(f, line);
    ss = std::stringstream{line}; // decompose line
    for (int i = 0; i < mesh.dimension; i++) {
      ss >> coords[i];
    }
  }

  // set cells_faces connectivity
  // in this case, preserve_targets_order=true when creating the connectivity
  mesh.set_connectivity<mesh.cell, mesh.face>(
      _read_connectivity(f, "Volumes->faces", n_cells));
  // set cells_nodes connectivity
  mesh.set_connectivity<mesh.cell, mesh.node>(
      _read_connectivity(f, "Volumes->Vertices", n_cells));
  // set faces_edges connectivity
  mesh.set_connectivity<mesh.face, mesh.edge>(
      _read_connectivity(f, "Faces->Edges", n_faces));
  // set faces_nodes connectivity
  mesh.set_connectivity<mesh.face, mesh.node>(
      _read_connectivity(f, "Faces->Vertices", n_faces));
  // when creating the connectivity inside set_connectivity,
  // preserve_targets_order=true
  mesh.ordered_faces_nodes = true;
  // avoid reading faces_cells
  // because not complete data: only interior faces are present
  for (int i = 0; i < n_faces + 1; ++i)
    std::getline(f, line);
  // set edges_nodes connectivity
  // not the same format, size is known and not precised at the beginning of
  // each line
  mesh.set_connectivity<mesh.edge, mesh.node>(
      _read_connectivity(f, "Edges", n_edges, 2));

  return mesh;
}

} // namespace icmesh
