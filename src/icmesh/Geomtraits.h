#pragma once

#include <compass_cxx_utils/array_utils.h>
#include <icmesh/Geom_utils.h>
#include <icmesh/Raw_mesh.h>
#include <icus/Field.h>
#include <icus/ISet.h>

#include <cmath>
#include <iostream> // for deprecated messages
#include <ranges>
#include <vector>

namespace icmesh {
// we allow operations between vertices (i.e. array)
using namespace compass::utils::array_operators;

// contains methods to compute centers, measures...
template <typename raw_mesh_t> struct Geomtraits : raw_mesh_t {
  using Vertex = raw_mesh_t::vertex_type;
  using Scalar = typename Vertex::value_type;
  using Vertex_field = raw_mesh_t::Vertex_field;
  using Scalar_field = icus::Field<Scalar>;
  using raw_mesh_t::cell;
  using raw_mesh_t::dimension;
  using raw_mesh_t::edge;
  using raw_mesh_t::elements;
  using raw_mesh_t::face;
  using raw_mesh_t::node;
  using raw_mesh_t::vertices;
  std::array<Scalar_field, dimension + 1> _measures;
  std::array<Vertex_field, dimension + 1> _centers_of_mass;
  std::array<Vertex_field, dimension + 1> _isobarycenters;
  /* Center-of-mass (CoM) = integral of x over the whole element, divided by
     the element's measure.

     It is usually distinct from the isobarycenter of the vertices, except in
     simple cases such as triangles and rectangles (or rhomboids and cuboids
     in 3D). E.g if one edge of a triangle is divided into multiple segments
     using intermediate nodes, the isobarycenter of the 2D element containing
     these supplementary nodes will be pulled toward the divided edge, while
     its CoM will remain the same as that of the initial triangle.
  */

  // because the construction of the raw_mesh has lots of templates
  // and args generation, the inheritance is constructed with a copy
  // constructor, copy is light
  Geomtraits() = default;
  Geomtraits(raw_mesh_t &&the_mesh) : raw_mesh_t{the_mesh} {}

  // *measures* (=length, area, volume)
  // by convention the measure of dimension 0 object is 1
  template <std::size_t obj_dim> Scalar_field &get_measures() {
    assert(obj_dim >= 0 && obj_dim <= cell);
    if (!_measures[obj_dim].values) {
      auto ok = _fill_measures_and_centers_of_mass<obj_dim>();
      assert(ok);
    }
    return _measures[obj_dim];
  }

  template <std::size_t obj_dim> Vertex_field &get_centers_of_mass() {
    static_assert(obj_dim >= 0 && obj_dim <= cell);
    if (!_centers_of_mass[obj_dim].values) {
      auto ok = _fill_measures_and_centers_of_mass<obj_dim>();
      assert(ok);
    }
    return _centers_of_mass[obj_dim];
  }

  template <std::size_t obj_dim> Vertex_field &get_isobarycenters() {
    static_assert(obj_dim >= 0 && obj_dim <= cell);
    if (!_isobarycenters[obj_dim].values) {
      auto ok = _fill_isobarycenters<obj_dim>();
      assert(ok);
    }
    return _isobarycenters[obj_dim];
  }

  Scalar_field &get_cell_measures() { return get_measures<cell>(); }
  Scalar_field &get_face_measures() { return get_measures<face>(); }
  Scalar_field &get_edge_measures() { return get_measures<edge>(); }
  Scalar_field &get_node_measures() { return get_measures<node>(); }

  Vertex_field &get_cell_centers_of_mass() {
    return get_centers_of_mass<cell>();
  }
  Vertex_field &get_face_centers_of_mass() {
    return get_centers_of_mass<face>();
  }
  Vertex_field &get_edge_centers_of_mass() {
    return get_centers_of_mass<edge>();
  }
  Vertex_field &get_node_centers_of_mass() {
    return get_centers_of_mass<node>();
  }

  Vertex_field &get_cell_isobarycenters() { return get_isobarycenters<cell>(); }
  Vertex_field &get_face_isobarycenters() { return get_isobarycenters<face>(); }
  Vertex_field &get_edge_isobarycenters() { return get_isobarycenters<edge>(); }
  Vertex_field &get_node_isobarycenters() { return get_isobarycenters<node>(); }

  Scalar_field measures(icus::ISet iset) {
    auto iset_measures = Scalar_field{iset};
    _fill_measures<node>(iset, iset_measures);
    _fill_measures<edge>(iset, iset_measures);
    // if dim > 1, cells are other objects (nor nodes nor edges)
    if constexpr (dimension > 1) {
      _fill_measures<cell>(iset, iset_measures);
      // if dim > 2, faces are other objects (nor nodes, nor edges, nor cells)
      if constexpr (dimension > 2)
        _fill_measures<face>(iset, iset_measures);
    }
    return iset_measures;
  }

  Vertex_field centers_of_mass(const icus::ISet &iset) {
    auto iset_centers = Vertex_field{iset};
    _fill_centers_of_mass<node>(iset, iset_centers);
    _fill_centers_of_mass<edge>(iset, iset_centers);
    // if dim > 1, cells are other objects (neither nodes nor edges)
    if constexpr (dimension > 1) {
      _fill_centers_of_mass<cell>(iset, iset_centers);
      // if dim > 2, faces are other objects (neither nodes, nor edges, nor
      // cells)
      if constexpr (dimension > 2)
        _fill_centers_of_mass<face>(iset, iset_centers);
    }
    return iset_centers;
  }

  Vertex_field isobarycenters(const icus::ISet &iset) {
    auto iset_centers = Vertex_field{iset};
    _fill_isobarycenters<node>(iset, iset_centers);
    _fill_isobarycenters<edge>(iset, iset_centers);
    // if dim > 1, cells are other objects (nor nodes nor edges)
    if constexpr (dimension > 1) {
      _fill_isobarycenters<cell>(iset, iset_centers);
      // if dim > 2, faces are other objects (nor nodes, nor edges, nor cells)
      if constexpr (dimension > 2)
        _fill_isobarycenters<face>(iset, iset_centers);
    }
    return iset_centers;
  }

  template <std::size_t obj_dim>
  void _fill_measures(icus::ISet iset, Scalar_field iset_measures) {
    using std::views::zip;
    // intersect iset and raw_mesh object where new_base is iset
    auto obj_iset = icus::intersect(iset, elements[obj_dim], iset);
    if (obj_iset.size() > 0) {
      auto obj_measures = get_measures<obj_dim>().extract(obj_iset);
      // obj_iset.mapping is the index in iset
      for (auto &&[i, meas] : zip(obj_iset.mapping(), obj_measures))
        iset_measures(i) = meas;
    }
  }

  template <std::size_t obj_dim>
  void _fill_centers_of_mass(icus::ISet iset, Vertex_field iset_centers) {
    using std::views::zip;
    // intersect iset and raw_mesh object where new_base is iset
    auto obj_iset = icus::intersect(iset, elements[obj_dim], iset);
    if (obj_iset.size() > 0) {
      auto obj_centers = get_centers_of_mass<obj_dim>().extract(obj_iset);
      // obj_iset.mapping is the index in iset
      for (auto &&[i, P] : zip(obj_iset.mapping(), obj_centers))
        iset_centers(i) = P;
    }
  }

  template <std::size_t obj_dim> bool _fill_isobarycenters() {
    static_assert(obj_dim >= 0);
    if constexpr (obj_dim == node) {
      _isobarycenters[obj_dim] = vertices;
    } else if constexpr (obj_dim <= dimension) {
      _isobarycenters[obj_dim] = barycenter(
          raw_mesh_t::template get_connectivity<obj_dim, node>(), vertices);
    } else {
      return false;
    }
    return true;
  }

  template <std::size_t obj_dim>
  void _fill_isobarycenters(icus::ISet iset, Vertex_field iset_centers) {
    using std::views::zip;
    // intersect iset and raw_mesh object where new_base is iset
    auto obj_iset = icus::intersect(iset, elements[obj_dim], iset);
    if (obj_iset.size() > 0) {
      auto obj_centers = get_isobarycenters<obj_dim>().extract(obj_iset);
      // obj_iset.mapping is the index in iset
      for (auto &&[i, P] : zip(obj_iset.mapping(), obj_centers))
        iset_centers(i) = P;
    }
  }

  template <std::size_t obj_dim> bool _fill_measures_and_centers_of_mass() {
    static_assert(obj_dim >= 0);
    if constexpr (obj_dim == 0) {
      _centers_of_mass[obj_dim] = vertices;
      _measures[obj_dim] = Scalar_field{elements[obj_dim]};
      // by convention the measure of dimension 0 object is 1
      _measures[obj_dim].fill(1.0);
    } else if constexpr (obj_dim == 1) {
      _centers_of_mass[obj_dim] = barycenter(
          raw_mesh_t::template get_connectivity<obj_dim, node>(), vertices);
      _measures[obj_dim] = length(
          raw_mesh_t::template get_connectivity<obj_dim, node>(), vertices);
    } else if constexpr (obj_dim == 2) {
      auto [meas, points] =
          area_and_CoM(raw_mesh_t::template get_connectivity<obj_dim, edge>(),
                       raw_mesh_t::template get_connectivity<edge, node>(),
                       vertices, get_isobarycenters<obj_dim>());
      _measures[obj_dim] = meas;
      _centers_of_mass[obj_dim] = points;
    } else if constexpr (obj_dim == 3) {
      auto [meas, points] = volume_and_CoM(
          raw_mesh_t::template get_connectivity<cell, face>(),
          raw_mesh_t::template get_connectivity<face, edge>(),
          raw_mesh_t::template get_connectivity<edge, node>(),
          get_cell_isobarycenters(), get_face_isobarycenters(), vertices);
      _measures[obj_dim] = meas;
      _centers_of_mass[obj_dim] = points;
    } else {
      return false;
    }
    return true;
  }

  template <typename Connectivity>
  static Vertex_field barycenter(Connectivity &connectivity,
                                 const Vertex_field &target_coords) {
    assert(connectivity.target() == target_coords.support);
    auto prop_centers = Vertex_field{connectivity.source()};
    for (auto &&[s, targets] : connectivity.targets_by_source())
      prop_centers(s) = one_barycenter(targets, target_coords);
    return prop_centers;
  }

  static inline Vertex one_barycenter(const auto &targets,
                                      const Vertex_field &target_coords) {
    auto bary = Vertex{};
    for (auto &&t : targets)
      bary += target_coords(t);
    bary /= Scalar(targets.size());
    return bary;
  }

  template <typename Connectivity>
  static Scalar_field length(Connectivity &connectivity,
                             const Vertex_field &target_coords) {
    assert(connectivity.target() == target_coords.support);
    auto prop_length = Scalar_field{connectivity.source()};
    for (auto &&[s, targets] : connectivity.targets_by_source()) {
      assert(targets.size() == 2);
      // FIXME todo
      prop_length(s) = icmesh::distance(target_coords(*targets.begin()),
                                        target_coords(*(targets.begin() + 1)));
    }
    return prop_length;
  }

  template <typename Connectivity0, typename Connectivity1>
  static std::tuple<Scalar_field, Vertex_field>
  area_and_CoM(Connectivity0 &surf_edges, Connectivity1 &edges_nodes,
               const Vertex_field &vert_coords,
               const Vertex_field &surf_centers) {
    assert(edges_nodes.target() == vert_coords.support);
    assert(surf_edges.source() == surf_centers.support);

    auto prop_areas = Scalar_field{surf_edges.source()};
    auto prop_centers = Vertex_field{surf_edges.source()};
    for (auto &&[s, edges] : surf_edges.targets_by_source()) {
      assert(edges.size() > 2);
      prop_areas(s) = 0.0;
      prop_centers(s) = Vertex{};
      // cut the 2D object in triangles using the object vertex-center
      for (auto &&the_edge : edges) {
        auto &&[n0, n1] = edges_nodes.targets_by_source(the_edge);
        auto Tarea = triangle_area<dimension>(vert_coords(n0), vert_coords(n1),
                                              surf_centers(s));
        auto Tcenter = vert_coords(n0) + vert_coords(n1) + surf_centers(s);
        Tcenter *= Tarea / 3.0;
        prop_areas(s) += Tarea;
        prop_centers(s) += Tcenter;
      }
      assert(prop_areas(s) != 0);
      prop_centers(s) /= prop_areas(s);
    }
    return std::make_tuple(prop_areas, prop_centers);
  }

  template <typename Connectivity0, typename Connectivity1>
  static Scalar_field
  area(Connectivity0 &surf_edges, Connectivity1 &edges_nodes,
       const Vertex_field &vert_coords, const Vertex_field &surf_centers) {
    auto [areas, centers] =
        area_and_CoM(surf_edges, edges_nodes, vert_coords, surf_centers);
    return areas;
  }

  template <typename Connectivity0, typename Connectivity1,
            typename Connectivity2>
  static std::tuple<Scalar_field, Vertex_field> volume_and_CoM(
      Connectivity0 &cell_face_conn, Connectivity1 &face_edge_conn,
      Connectivity2 &edge_node_conn, const Vertex_field &cell_centers,
      const Vertex_field &face_centers, const Vertex_field &vert_coords) {
    static_assert(dimension == 3);
    assert(cell_face_conn.source() == cell_centers.support);
    assert(cell_face_conn.target() == face_edge_conn.source());
    assert(face_edge_conn.source() == face_centers.support);
    assert(face_edge_conn.target() == edge_node_conn.source());
    assert(edge_node_conn.target() == vert_coords.support);

    auto prop_volumes = Scalar_field{cell_face_conn.source()};
    auto prop_centers = Vertex_field{cell_face_conn.source()};
    for (auto &&[the_cell, faces] : cell_face_conn.targets_by_source()) {
      prop_volumes(the_cell) = 0.0;
      prop_centers(the_cell) = Vertex{};
      // cut the 3D object in tetrahedron using the cell vertex-center,
      // the faces center and the edges
      auto &&yk = cell_centers(the_cell);
      for (auto &&the_face : faces) {
        auto &&xs = face_centers(the_face);

        for (auto &&the_edge : face_edge_conn.targets_by_source(the_face)) {
          auto &&[n0, n1] = edge_node_conn.targets_by_source(the_edge);
          auto &&x0 = vert_coords(n0);
          auto &&x1 = vert_coords(n1);
          auto Tvolume = tetra_volume(x0, x1, xs, yk);
          auto Tcenter = x0 + x1 + xs + yk;
          Tcenter *= Tvolume / 4.0;
          prop_volumes(the_cell) += Tvolume;
          prop_centers(the_cell) += Tcenter;
        }
      }
      assert(prop_volumes(the_cell) != 0);
      prop_centers(the_cell) /= prop_volumes(the_cell);
    }
    return std::make_tuple(prop_volumes, prop_centers);
  }

  template <typename Connectivity0, typename Connectivity1,
            typename Connectivity2>
  static Scalar_field
  volume(Connectivity0 &cell_face_conn, Connectivity1 &face_edge_conn,
         Connectivity2 &edge_node_conn, const Vertex_field &cell_centers,
         const Vertex_field &face_centers, const Vertex_field &vert_coords) {
    auto [volumes, centers] =
        volume_and_CoM(cell_face_conn, face_edge_conn, edge_node_conn,
                       cell_centers, face_centers, vert_coords);
    return volumes;
  }

  template <std::size_t obj_dim>
    requires(obj_dim >= 2 && obj_dim <= dimension)
  Scalar diameter(const index_type el) {
    // check that "el" is one of the elements of dimension "obj_dim"
    assert((el >= 0 && el < elements[obj_dim].size()));
    // By convention, the diameter of an object is the largest
    // distance between two of its nodes
    auto el_nodes = raw_mesh_t::template get_connectivity<obj_dim, node>()
                        .targets_by_source(el);
    Scalar diam = 0.0;
    for (auto &&n0 = el_nodes.begin(); n0 < el_nodes.end(); ++n0) {
      for (auto &&n1 = n0 + 1; n1 < el_nodes.end(); ++n1) {
        diam = std::max(diam, icmesh::distance(vertices(*n0), vertices(*n1)));
      }
    }
    return diam;
  }

  template <size_t obj_dim>
    requires((obj_dim == 1 || obj_dim == 2) && obj_dim < dimension)
  Vertex normal_vect_out(const index_type el, const index_type el_ref) {
    /* Return the normal unit vector to an element el of
       dimension obj_dim, pointing outward with respect
       to another element el_ref of dimension obj_dim+1,
       in a mesh of dimension ≥ 2
       (considering that vectors only make sense in a
       space of dimension ≥ 2 and that the "normal to A
       outward from B" is well defined only if the
       co-dimension of objects A and B is equal to 1).
    */
    // check that "el" is one of the elements of dimension obj_dim
    assert((el >= 0 && el < elements[obj_dim].size()));
    // check that "el_ref" is one of the elements of dimension obj_dim+1
    assert((el_ref >= 0 && el_ref < elements[obj_dim + 1].size()));
    // find the nodes of "el"
    auto el_nodes = raw_mesh_t::template get_connectivity<obj_dim, node>()
                        .targets_by_source(el);
    // find the "outward direction"
    // FIXME : this does not work if the center-of-mass of el_ref is aligned
    // with element el, it would be better to use a "starring point" of el_ref
    auto xref = get_centers_of_mass<obj_dim + 1>()(el_ref);
    auto vout = get_centers_of_mass<obj_dim>()(el) - xref;

    Vertex v;
    if constexpr (obj_dim + 1 == dimension) {
      // "el" is an edge in a 2D space or a face in a 3D space, its
      // normal direction is well defined
      v = icmesh::normal_vect<dimension>(vertices, el_nodes);
    } else {
      // "el" is an edge in a 3D space, its normal direction
      // is not defined : we need to use xref to make a triangle
      auto x0 = vertices(el_nodes[0]);
      auto x1 = vertices(el_nodes[1]);
      // v1 : vector tangent to the edge
      auto v1 = x1 - x0;
      // v2 : vector normal to the triangle (x0, x1, xref)
      auto v2 = icmesh::normal_vect<dimension>(x0, x1, xref);
      // v : unit normal vector to the segment (x0, x1) in the plane (x0, x1, x)
      v = icmesh::vector_product<dimension>(v1, v2);
      v /= norm(v);
    }

    // scalar product between vout and v to get the "outward" sign
    Scalar s = sum(vout * v);
    if (s < 0.0)
      v *= -1.0;
    return v;
  }
};

template <typename... Args> auto mesh(Args &&...args) {
  return Geomtraits{raw_mesh(std::forward<Args>(args)...)};
}

//
template <typename... Args> auto mesh_tetra(Args &&...args) {
  return Geomtraits{raw_mesh_tetra(std::forward<Args>(args)...)};
}

//
template <typename... Args> auto mesh_cuboid(Args &&...args) {
  return Geomtraits{raw_mesh_cuboid(std::forward<Args>(args)...)};
}

} // namespace icmesh
