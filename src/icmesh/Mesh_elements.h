#pragma once

#include <array>
#include <icus/ISet.h>

namespace icmesh {

template <std::size_t dim>
  requires(dim > 0 && dim <= 3)
struct Mesh_elements {
  enum Element_type : std::size_t {
    node = 0,
    edge = 1,
    face = dim - 1,
    cell = dim
  };
  template <Element_type et> struct Element_iset : icus::ISet {
    static constexpr auto element_type = et;
    using icus::ISet::operator=;
  };
  icus::ISet joined_elements;
  std::array<icus::ISet, dim + 1> elements;
  Element_iset<node> nodes;
  Element_iset<edge> edges;
  Element_iset<face> faces;
  Element_iset<cell> cells;
  Mesh_elements() = default;
  Mesh_elements(const std::array<icus::ISet, dim + 1> &elts) {
    // FIXME: ISet API should be changed
    //        arguments could becoume const std::array<icus::ISet, dim+1>&
    //        elements
    // auto joined_elements = icus::join_with_no_base(elements);
    // nodes = std::get<node>(joined_elements);
    // edges = std::get<edge>(joined_elements);
    // faces = std::get<face>(joined_elements);
    // cells = std::get<cell>(joined_elements);
    elements = elts; // useless copy with API change
    // the copy stresses the need for API change
    // we want the arguments to be const&
    // std::cout << "building mesh elements:";
    // for (auto &&e : elts) {
    //   std::cout << " " << e.size();
    // }
    // std::cout << std::endl;

    // join_no_base : joined_elements is the direct parent of each element
    // could be join and rebase but we lose information doing rebase.
    joined_elements = icus::join_no_base(elements);
    nodes = elements[node];
    edges = elements[edge];
    faces = elements[face];
    cells = elements[cell];
  }
};

// template deduction guide
template <std::size_t dimp1>
Mesh_elements(const std::array<icus::ISet, dimp1> &)
    -> Mesh_elements<dimp1 - 1>;

template <std::size_t n>
  requires(n > 1)
auto mesh_elements(const std::array<icus::ISet, n> &elements) {
  return Mesh_elements{elements};
}

template <typename... I>
  requires(sizeof...(I) >= 1 &&
           std::conjunction_v<std::is_same<std::decay_t<I>, icus::ISet>...>)
auto mesh_elements(I &&...isets) {
  return Mesh_elements{
      std::array<icus::ISet, sizeof...(I)>{std::forward<I>(isets)...}};
}

} // namespace icmesh
