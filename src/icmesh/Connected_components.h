#pragma once

#include <compass_cxx_utils/pretty_print.h>
#include <icmesh/Geomtraits.h>

// This utility creates 2 distinct meshes: one with the volumic mesh
// and one with the gamma faces (and 0 cells)
// todo: the mesh type of the volumic mesh should be the same than
// the given mesh.

namespace icmesh {

template <typename Mesh_t>
  requires(Mesh_t::dimension == 3)
struct Connected_components {
  using Default_m =
      Geomtraits<Raw_mesh<Mesh_t::dimension,
                          default_connectivity_matrix<Mesh_t::dimension>>>;
  Mesh_t original_mesh;
  Default_m mesh;
  icus::ISet gamma; // set of gamma faces
  // connectivity{mesh.joined_elements, mesh.joined_elemnts, same_location}
  icus::Connectivity<icus::ISet> same_location;

  Connected_components(Mesh_t &m, icus::ISet g) : original_mesh{m} {
    if (g.empty())
      throw std::runtime_error(
          "Cannot create connected components meshes with empty gamma list");
    assert(g.is_included_in(m.faces));

    // gama_nodes.image gives the nodes belonging to gamma
    auto gnodes = m.template get_connectivity<m.face, m.node>()
                      .extract(g, m.nodes)
                      .image();

    // build the set of new nodes
    auto &&[new_cn, cells_newcn, newcn_gnodes, gcells] =
        build_new_nodes(m, g, gnodes);

    // ----------------------------------------------------
    // build the new mesh which contains the 3D mesh and gamma
    build_connected_mesh(m, g, gcells, gnodes, cells_newcn, newcn_gnodes);

    // test that faces_eges * edges_nodes is coherent with faces_nodes,
    // and that gamma objects are not in cells_objects connectivity
    assert(test_consistency(mesh));
  }

private:
  inline auto new_edge_i(auto n0, auto n1, auto edge_size, auto &newedges,
                         auto &newedges_i, auto &newe_newn) {
    auto nmin = std::min(n0, n1);
    auto nmax = std::max(n0, n1);
    auto it = std::find(newedges[nmin].begin(), newedges[nmin].end(), nmax);
    if (it != newedges[nmin].end()) {
      // edge already exists, get its index
      int it_i = std::distance(newedges[nmin].begin(), it);
      auto new_i = newedges_i[nmin][it_i];
      return std::make_tuple(new_i, edge_size);
    }
    // if not found, create the new edge
    auto new_i = edge_size++;
    // store the new edge and its index
    newedges[nmin].push_back(nmax);
    newedges_i[nmin].push_back(new_i);
    newe_newn[new_i] = {n0, n1};

    return std::make_tuple(new_i, edge_size);
  }

  void build_connected_mesh(Mesh_t &m, icus::ISet g, icus::ISet gcells,
                            icus::ISet gnodes, auto cells_newcn,
                            auto newcn_gnodes) {

    // the object far from gamma keep their indices, the gamma objects keep
    // their indices, the new nodes, edges, faces are numbering starting from
    // m.obj.size()

    // remove print
    compass::utils::Pretty_printer print;
    // Fgamma is true if the face belongs to gamma
    // the faces of the new mesh become mesh.faces +
    // new_faces there are as many new_faces as gamma_cells connections
    auto Fgamma = icus::make_field<bool>(m.faces);
    Fgamma.fill(false);
    Fgamma.view(g).fill(true);

    // ------------------------------------------------------------------------------------
    // build the new nodes and their positions
    // the new nodes contain m.nodes + the new_cn (gnodes indices are
    // kept indentical, so the new node indices start at m.nodes.size())
    assert(gnodes.parent() == m.nodes);
    auto nb_nodes = m.nodes.size(); // will be updated
    auto new_ni = m.nodes.size();
    // create the new indices of the new nodes, nb_nodes contains all the new
    // mesh nodes
    auto new_cn = cells_newcn.target();
    auto newcn_i = icus::make_field<icus::index_type>(new_cn);
    for (auto &&i : new_cn) {
      newcn_i(i) = new_ni++; // index is new_ni, increment for the next new node
      ++nb_nodes;
    }
    // At min same nb of new nodes as nb
    // of gamma node (the min is obtained if gamma is only at the mesh boundary)
    assert(nb_nodes >= m.nodes.size() + gnodes.size());

    // store the new nodes positions
    auto new_nodes = icus::ISet(nb_nodes);
    auto new_pos = typename Mesh_t::Vertex_field{new_nodes};
    // init the position with m.vertices
    // cannot use new_pos.view(m.nodes) because new_nodes is not in the tree
    for (auto &&[i, pos] : m.vertices.enumerate())
      new_pos(i) = pos;
    // loop over the new nodes
    for (auto &&n : new_cn) {
      // to get its position: get its index in gnodes, gnodes.parent is m.nodes
      auto &&[gn] = newcn_gnodes.targets_by_source(n);
      // modify the position of its new index
      new_pos(newcn_i(n)) = m.vertices(gnodes.mapping()[gn]);
    }

    // ------------------------------------------------------------------------------------
    // build the new face object and the new cell_faces, faces_edges and
    // faces_nodes connectivities info the cell iset does not change
    auto cells_faces = m.template get_connectivity<m.cell, m.face>();
    auto cells_edges = m.template get_connectivity<m.cell, m.edge>();
    auto faces_nodes = m.template get_connectivity<m.face, m.node>();
    auto faces_edges = m.template get_connectivity<m.face, m.edge>();
    auto edges_nodes = m.template get_connectivity<m.edge, m.node>();
    std::vector<std::vector<icus::index_type>> c_newf{m.cells.size()};
    // the new faces contain m.faces + the dupplication due to gamma
    // (gamma indices are kept) at most 2 faces are introduced for each
    // gamma face
    std::vector<std::vector<icus::index_type>> newf_newn{m.faces.size() +
                                                         2 * g.size()};
    std::vector<std::vector<icus::index_type>> newf_newe{m.faces.size() +
                                                         2 * g.size()};
    auto nb_faces = m.faces.size(); // will be updated
    auto new_fi = m.faces.size();   // new face indices start at m.faces.size()
    // to store the mapping new face index -> gamma index to fill same_loc
    std::vector<icus::index_type> same_face_loc(2 * g.size());

    // idem for the new edges
    auto gamma_edges =
        m.template get_connectivity<m.face, m.edge>().extract(g, m.edges);
    auto gedges = gamma_edges.image();
    auto nb_edges = m.edges.size(); // will be updated
    auto new_ei = m.edges.size();   // new edge indices start at m.edges.size()
    // the new edges contain m.edges + the dupplication due to gamma
    // (gedges indices are kept) at most max_new_edges edges are introduced
    auto max_new_edges =
        cells_edges.extract(gcells, m.edges).connections().size();
    std::vector<std::array<icus::index_type, 2>> newe_newn{m.edges.size() +
                                                           max_new_edges};
    // to store the mapping new edge index -> gamma edge index to fill same_loc
    std::vector<icus::index_type> same_edge_loc(max_new_edges);
    // does the edge belongs to gamma ?
    auto Egamma = icus::make_field<bool>(m.edges);
    Egamma.fill(false);
    Egamma.view(gedges).fill(true);

    // the two following vectors will store the edge and nodes indices
    // of the new edges only, such that
    // new_edges[n0] contains the vector of all the nodes
    // connecting to n0 such that min(n0, node) = n0,
    // new_edges_i[n0] contains the edge indices in same order
    // nb_nodes already contains the new number of nodes
    std::vector<std::vector<icus::index_type>> new_edges{nb_nodes};
    std::vector<std::vector<icus::index_type>> new_edges_i{nb_nodes};

    // fill the mapping between the indice of m.nodes/m.edges and the new index
    // (the new index depends on the cell, the mapping is updated for each cell)
    // to build the 3D mesh:
    // the non-gamma nodes/edges keep the same index, the gamma nodes/edges are
    // modified
    auto map_nodes_newcn = icus::make_field<icus::index_type>(m.nodes);
    map_nodes_newcn.fill(std::views::iota(0, (int)m.nodes.size()));

    // init the connectivities with the previous values.
    // It initializes the mesh "far from" gamma where nothing is modified,
    // and the "gamma mesh" where the original indices are kept.
    // Creations and modifications will be done later
    for (auto &&[K, faces] : cells_faces.targets_by_source())
      // init new cell_face connectivity with previous indices. If K belongs to
      // gcells, the values will be modified
      c_newf[K] = {faces.begin(), faces.end()};
    // if K is not a neighbouring cell, faces are not modified, neither the

    // idem for faces_nodes, new faces with new nodes will be created later
    for (auto &&[f, nodes] : faces_nodes.targets_by_source())
      newf_newn[f] = {nodes.begin(), nodes.end()};
    // idem for faces_edges, new faces with new edges will be created later
    for (auto &&[f, edges] : faces_edges.targets_by_source())
      newf_newe[f] = {edges.begin(), edges.end()};
    // idem for edges_nodes, new edges with new nodes will be created later
    for (auto &&e : m.edges) {
      auto &&[n0, n1] = edges_nodes.targets_by_source(e);
      newe_newn[e] = {n0, n1};
    }

    // gcells contains the cells touching at least one gamma nodes
    assert(gcells.parent() == m.cells);
    for (auto &&K : gcells.mapping()) {
      // if K touches at least one gamma node, some nodes indices will change,
      // find the node index to init map_nodes_newcn
      auto Knewcn = cells_newcn.targets_by_source(K);
      for (auto &&n : Knewcn) {
        auto &&[gn] = newcn_gnodes.targets_by_source(n);
        // old and new indices of the node (view from cell K)
        auto m_ni = gnodes.mapping()[gn];
        auto new_ni = newcn_i(n);
        map_nodes_newcn(m_ni) = new_ni;
      }

      // init the connectivity newe_newn with the new nodes indices
      // of the edges which do not belong to gamma (they keep the same
      // indices) the edges belonging to gamma are filled using new_edge_i
      for (auto &&e : cells_edges.targets_by_source(K)) {
        if (!Egamma(e)) {
          auto &&[n0, n1] = edges_nodes.targets_by_source(e);
          newe_newn[e] = {map_nodes_newcn(n0), map_nodes_newcn(n1)};
        }
      }

      // build the connectivity K_faces with the new face indices, is init
      // with previous ones
      for (auto &&f : c_newf[K]) {
        // get connectivity with previous indices
        auto f_nodes = faces_nodes.targets_by_source(f);
        auto f_edges = faces_edges.targets_by_source(f);
        // the nodes indices may be modified even if non gamma face
        // initialize with previous indices
        // create a new vector because cannot use newf_newn[f]: f is the old
        // index, the new one is not known yet
        std::vector<icus::index_type> newf_nodes = {f_nodes.begin(),
                                                    f_nodes.end()};

        if (Fgamma(f)) {
          // store the gamma information for the "gamma mesh"
          // gamma indices are kept indentical
          newf_newe[f] = {f_edges.begin(), f_edges.end()};
          newf_newn[f] = newf_nodes;

          // store the mapping new index -> gamma index to fill same_loc.
          // "- m.faces.size()" to reduce the size of the vector
          same_face_loc[new_fi - m.faces.size()] = f;
          // modify the connectivity c_newf[K] with the new face index (new_fi)
          // and increment for the next new face
          f = new_fi++;
          ++nb_faces;
        } // end if Fgamma
        // initialize newf_newe[f] with previous indices, f is now the new
        // index
        newf_newe[f] = {f_edges.begin(), f_edges.end()};
        // loop over face edges (previous index of edge)
        // modification only if gamma edge
        for (auto &&e : newf_newe[f]) {
          // if gamma edge find or create new edge
          if (Egamma(e)) {
            // old indices of the nodes
            auto &&[n0, n1] = edges_nodes.targets_by_source(e);
            // get the new indices of these nodes
            auto newn0 = map_nodes_newcn(n0);
            auto newn1 = map_nodes_newcn(n1);
            // find or create the new edge, fill newe_newn
            auto &&[new_ei, e_size] = new_edge_i(
                newn0, newn1, nb_edges, new_edges, new_edges_i, newe_newn);
            // store the mapping new index -> gamma index to fill same_loc.
            // "- m.edges.size()" to reduce the size of the vector
            same_edge_loc[new_ei - m.edges.size()] = e;
            e = new_ei; // modify newf_newe[f]
            nb_edges = e_size;
          } // end gamma edge
        } // end loop over edges
        // modify the connectivity f_nodes with the new node indices
        for (auto &&n : newf_nodes)
          n = map_nodes_newcn(n);
        newf_newn[f] = newf_nodes;

      } // end loop over K faces
    } // end loop over gamma cells
    // at min same nb of new faces/edges
    // as nb of gamma faces/edges, at max twice
    assert(nb_faces >= m.faces.size() + g.size());
    assert(nb_faces <= m.faces.size() + 2 * g.size());
    assert(nb_edges >= m.edges.size() + gedges.size());
    assert(nb_edges <= m.edges.size() + max_new_edges);
    newf_newn.resize(nb_faces);
    newf_newe.resize(nb_faces);
    newe_newn.resize(nb_edges);

    // ----------------------------------------------------------------
    // build the new mesh
    // Todo: should have the same structure as the given mesh.
    // Maybe difficult with the structured mesh because
    // many info are not stored ???
    // creates new mesh isets
    mesh = icmesh::mesh(nb_nodes, nb_edges, nb_faces, m.cells.size());
    mesh.vertices.fill(new_pos);
    mesh.template set_connectivity<Mesh_t::cell, Mesh_t::face>(c_newf);
    mesh.template set_connectivity<Mesh_t::face, Mesh_t::edge>(newf_newe);
    mesh.template set_connectivity<Mesh_t::face, Mesh_t::node>(newf_newn);
    mesh.template set_connectivity<Mesh_t::edge, Mesh_t::node>(newe_newn);

    // store the gamma set of faces in the new mesh
    gamma = mesh.faces.extract(g.rebase(m.faces).mapping());

    // ----------------------------------------------------------------
    // build same location: connectivity from joined_elements to joined_elements
    // which stores the indexes of the other objects at the same location.
    // Set of nodes or edges or faces. It does not store the identity index.
    std::vector<std::vector<icus::index_type>> same_loc{
        mesh.joined_elements.size()};
    // loop over the new nodes
    for (auto &&n : new_cn) {
      // get its index in gnodes, gnodes.parent is m.nodes
      auto &&[gn] = newcn_gnodes.targets_by_source(n);
      // previous node index is still gamma node index
      auto prev_n = gnodes.mapping()[gn];
      auto new_n = newcn_i(n); // new index in mesh.nodes
      // find new node indices in joined_elements
      auto prev_je = mesh.nodes.mapping()[prev_n];
      auto new_je = mesh.nodes.mapping()[new_n];
      same_loc[prev_je].push_back(new_je);
      same_loc[new_je].push_back(prev_je);
    }
    // loop over the new faces
    for (auto new_f = m.faces.size(); new_f < nb_faces; ++new_f) {
      auto prev_f = same_face_loc[new_f - m.faces.size()];
      // find new face indices in joined_elements
      auto prev_je = mesh.faces.mapping()[prev_f];
      auto new_je = mesh.faces.mapping()[new_f];
      same_loc[prev_je].push_back(new_je);
      same_loc[new_je].push_back(prev_je);
    }
    // loop over the new edges
    for (auto new_e = m.edges.size(); new_e < nb_edges; ++new_e) {
      auto prev_e = same_edge_loc[new_e - m.edges.size()];
      // find new edge indices in joined_elements
      auto prev_je = mesh.edges.mapping()[prev_e];
      auto new_je = mesh.edges.mapping()[new_e];
      same_loc[prev_je].push_back(new_je);
      same_loc[new_je].push_back(prev_je);
    }

    // complete same_loc with transitive operation:
    // elemt i connects j; elemt j connects k; then elemt i connects k
    for (auto &&[i, locs] : std::views::enumerate(same_loc)) {
      for (auto j_it = locs.begin(); j_it < locs.end(); ++j_it) {
        for (auto k_it = j_it + 1; k_it < locs.end(); ++k_it) {
          if (std::find(same_loc[*j_it].begin(), same_loc[*j_it].end(),
                        *k_it) == same_loc[*j_it].end())
            same_loc[*j_it].push_back(*k_it);
          if (std::find(same_loc[*k_it].begin(), same_loc[*k_it].end(),
                        *j_it) == same_loc[*k_it].end())
            same_loc[*k_it].push_back(*j_it);
        }
      }
    }

    same_location = {mesh.joined_elements, mesh.joined_elements, same_loc};
  }

  auto build_new_nodes(Mesh_t &m, icus::ISet g, icus::ISet gnodes) {

    // duplicates the nodes belonging to gamma such as:
    // as many nodes as neighbouring cells
    // ie as many nodes as non-zero in the cells_gammanodes connectivity
    auto cells_gnodes =
        m.template get_connectivity<m.cell, m.node>().restrict_target(gnodes);
    // possible new nodes due to the gamma presence (cn stands for cell-nodes)
    auto tmp_cn = icus::ISet(cells_gnodes.connections().size());
    // build cn_cells connectivity: each cn connects one cell
    // and build cn_gnodes connectivity: each cn connects one gamma node (its
    // position)
    std::vector<icus::index_type> cnc, cnn;
    cnc.reserve(tmp_cn.size());
    cnn.reserve(tmp_cn.size());
    for (auto &&[c, gn] : cells_gnodes.targets_by_source()) {
      for (auto &&n : gn) {
        cnc.push_back(c);
        cnn.push_back(n);
      }
    }
    // todo: why it is mandatory to convert m.cells into iset in the following
    // ???
    auto cn_cells =
        icus::make_connectivity<1>(tmp_cn, icus::ISet(m.cells), cnc);
    auto cn_gnodes = icus::make_connectivity<1>(tmp_cn, gnodes, cnn);

    // build cells_nongamma connectivity
    auto nongamma = m.faces.substract(g);
    auto cells_nongamma =
        m.template get_connectivity<m.cell, m.face>().restrict_target(nongamma);

    // build cn_cn such that 2 nodes are connected if they belong to cells whose
    // connecting face does not belong to gamma cn_cn = cn_cells *
    // cells_nongamma
    // * transpose(cells_nongamma) * transpose(cn_cells): cn_cn = cn_cells *
    // cells_nongamma * transpose(cn_cells * cells_nongamma)
    auto cn_nongamma = cn_cells.compose(cells_nongamma);
    auto cn_cn_nongamma = cn_nongamma.compose(cn_nongamma.transpose());
    // build cn_cn such that 2 nodes are connected if they have the same
    // position
    auto cn_cn_pos = cn_gnodes.compose(cn_gnodes.transpose());
    // compute the intersection between the 2 cn_cn connectivities (term by term
    // multiplication)
    auto cn_cn = icus::intersect(cn_cn_pos, cn_cn_nongamma);

    // creates a vector with cell-nodes size to create the
    // connected components
    // keep the index of the smallest cn connected
    std::vector<icus::index_type> connected_cn(tmp_cn.size());
    // initialize with range(size)
    std::iota(connected_cn.begin(), connected_cn.end(), 0);
    bool new_it = true;
    while (new_it) {
      auto previous_cn = connected_cn;
      for (auto &&[cni, nodes] : cn_cn.targets_by_source()) {
        auto min_cn = tmp_cn.size();
        for (auto &&n : nodes)
          min_cn = std::min(min_cn, connected_cn[n]);

        // change values of all nodes to the min
        for (auto &&n : nodes)
          connected_cn[n] = min_cn;
      }

      // if previous is distinct from connected_cn, do one more iteration
      new_it = false;
      for (auto &&[prev, ccn] : std::views::zip(previous_cn, connected_cn)) {
        // compare integers
        if (std::fabs(prev - ccn) > 1.e-2) {
          new_it = true;
          break;
        }
      }
    }

    // remove dupplicates
    std::set<icus::index_type> new_cn_set(connected_cn.begin(),
                                          connected_cn.end());
    auto new_cn = tmp_cn.extract(new_cn_set);

    // modify connected_cn with the new cn indices, fill the tmp mapping
    // map_tmpcn_newcn
    auto map_tmpcn_newcn = icus::make_field<icus::index_type>(tmp_cn);
    for (auto &&[i, map] : new_cn.enumerate_mapping())
      map_tmpcn_newcn(map) = i;

    for (auto &&val : connected_cn)
      val = map_tmpcn_newcn(val);

    // build the connectivity cells_newcn which connects the mesh cells
    // to the new iset of nodes, same structure as cells_gnodes,
    // but the values are given by connected_cn
    std::vector<std::vector<icus::index_type>> cells_newcn_vect{m.cells.size()};
    auto compt = 0;
    for (auto &&[K, nodes] : cells_gnodes.targets_by_source()) {
      cells_newcn_vect[K] = {connected_cn.begin() + compt,
                             connected_cn.begin() + compt + nodes.size()};
      // cells_newcn_vect[K]);
      compt += nodes.size();
    }
    assert(compt == connected_cn.size());
    assert(compt == cells_gnodes.connections().size());
    auto cells_newcn =
        icus::make_connectivity(m.cells, new_cn, cells_newcn_vect);

    // build the mapping between the new nodes and their indices in m.nodes
    auto newcn_gnodes = cn_gnodes.extract(new_cn, gnodes);
    assert(newcn_gnodes.targets_size_v == 1);

    // cn_cells.image() gives the gamma neighbouring cells (each cells touching
    // at least one gamma node)
    return std::make_tuple(new_cn, cells_newcn, newcn_gnodes, cn_cells.image());
  }

  bool test_consistency(Default_m &m) {
    // check that the gamma faces are not in cells_faces connectivity
    auto &&c_faces = mesh.template get_connectivity<m.cell, m.face>().image();
    assert(intersect(c_faces, gamma, mesh.faces).empty());
    // check that gamma edges are not in cells_edges
    auto c_edges = mesh.template get_connectivity<m.cell, m.edge>().image();
    auto gedges = mesh.template get_connectivity<m.face, m.edge>()
                      .extract(gamma, mesh.edges)
                      .image();
    assert(intersect(c_edges, gedges, mesh.edges).empty());
    // check that gamma nodes are not in cells_nodes
    auto c_nodes = mesh.template get_connectivity<m.cell, m.node>().image();
    auto gnodes = mesh.template get_connectivity<m.face, m.node>()
                      .extract(gamma, mesh.nodes)
                      .image();
    assert(intersect(c_nodes, gnodes, mesh.nodes).empty());

    // test that faces_eges * edges_nodes is coherent with faces_nodes
    auto fn = m.template get_connectivity<m.face, m.node>();
    // compose returns an ordered connectivity
    auto fn_comp = m.template get_connectivity<m.face, m.edge>().compose(
        m.template get_connectivity<m.edge, m.node>());
    assert(fn_comp.has_sorted_targets());
    for (auto &&[ts, tsc] : std::views::zip(fn.targets(), fn_comp.targets())) {
      if (ts.size() != tsc.size()) {
        compass::utils::Pretty_printer print;
        print("Error ! face_node is", ts, "face_edge * edge_node is", tsc);
        return false;
      }
      for (auto &&t : ts) {
        // look for t in tsc (which is sorted)
        if (!std::binary_search(tsc.begin(), tsc.end(), t)) {
          compass::utils::Pretty_printer print;
          print("Error ! face_node is", ts, "face_edge * edge_node is", tsc);
          return false;
        }
      }
    }
    return true;
  }
};

} // namespace icmesh
