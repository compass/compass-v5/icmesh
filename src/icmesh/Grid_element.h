#pragma once

// #include <iostream>
#include <type_traits>

#include <compass_cxx_utils/array_utils.h>

#include "Axis_utils.h"
#include "typedefs.h"

namespace icmesh {

// for a structured grid without alocating memory
template <dimension_type element_dimension, dimension_type space_dimension>
  requires(element_dimension <= space_dimension)
struct Grid_element {
  static constexpr std::uint_fast32_t dimension = element_dimension;
  static constexpr std::uint_fast32_t codimension =
      space_dimension - element_dimension;
  static constexpr auto axis_configurations =
      blocked_axis<codimension, space_dimension>;
  Axis_info<> axis_info;
  std::array<index_type, space_dimension> indices;
  Grid_element() = delete;
  template <typename AxisInfo, typename Indices>
  Grid_element(AxisInfo &&a, Indices &&i)
      : axis_info{std::forward<AxisInfo>(a)},
        indices{std::forward<Indices>(i)} {}
  bool is_valid() const {
    return axis_info.blocked_dimensions() == codimension;
  }
  template <bool break_on_first_axis = false>
    requires(dimension > 0)
  auto limits() const {
    assert(is_valid());
    constexpr auto nb_limits = break_on_first_axis ? 2 : 2 * dimension;
    using Limit = Grid_element<dimension - 1, space_dimension>;
    auto result =
        compass::utils::create_array<nb_limits>(Limit{axis_info, indices});
    std::uint_fast32_t k = 0;
    for (std::uint_fast32_t i = 0; i < space_dimension; ++i) {
      if (axis_info.is_axis_free(i)) {
        result[k].axis_info.block_axis(i);
        assert(result[k].is_valid());
        ++k;
        result[k].axis_info.block_axis(i);
        ++(result[k].indices[i]);
        assert(result[k].is_valid());
        if constexpr (break_on_first_axis)
          break;
        ++k;
      }
    }
    assert(k == nb_limits || (break_on_first_axis && k == 1));
    return result;
  }
  auto first_limits() const { return limits<true>(); }
};

} // namespace icmesh
