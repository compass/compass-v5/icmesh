import numpy as np
import vtkwriters as vtkw
from pathlib import Path
from compass_utils.filenames import proc_filename, create_vtu_directory
from compass_utils import mpi


def are_connected(node1, node2, edges):
    for n, p in edges:
        if (n == node1 and p == node2) or (n == node2 and p == node1):
            return True
    return False


def reorder(nodes, edges):
    # VTK polyhedra writer assumes all vertices of the
    # polyhedron faces are sorted in "connection order"
    # (e.g. nodes[i] and nodes[i+1] are connected by an edge)
    # This is not the case in general so we have to reorder
    # them before dumping the mesh
    if len(nodes) == 2:
        assert are_connected(nodes[0], nodes[1], edges)
        return [nodes[0], nodes[1]]
    else:
        for i in range(1, len(nodes)):
            if are_connected(nodes[0], nodes[i], edges):
                nodes[1], nodes[i] = nodes[i], nodes[1]
                return [nodes[0]] + reorder(nodes[1:], edges)


def edges_as_node_tuple(edges_nodes, edges):
    return [tuple(edges_nodes[edge]) for edge in edges]


def faces_consecutive_nodes(mesh):
    # only implemented in 3D yet
    assert mesh.dimension == 3
    # if faces_nodes is already in connections order
    # (node[n], node[n+1]) is an edge
    if mesh.ordered_faces_nodes:
        # cannot use mesh.faces_nodes.as_array, can be unstructured mesh
        return [nodes.tolist() for nodes in mesh.faces_nodes.targets()]
    else:
        faces_edges = mesh.faces_edges
        # always regular connectivity, ok to use as_array
        edges_nodes = mesh.edges_nodes.as_array()
        # the following does not modify mesh.faces_nodes
        faces_nodes = [
            reorder(
                nodes.tolist(),
                edges_as_node_tuple(edges_nodes, faces_edges.targets_by_source(f)),
            )
            for f, nodes in enumerate(mesh.faces_nodes.targets())
        ]
        return faces_nodes


def build_Kfaces_nodes(mesh):
    # only implemented in 3D yet
    assert mesh.dimension == 3
    # if faces_nodes already in connections order, ie
    # (node[n], node[n+1]) is an edge
    if mesh.ordered_faces_nodes:
        return [
            [mesh.faces_nodes.targets_by_source(face).tolist() for face in faces]
            for faces in mesh.cells_faces.targets()
        ]
    else:
        faces_nodes = faces_consecutive_nodes(mesh)
        return [[faces_nodes[f] for f in faces] for faces in mesh.cells_faces.targets()]


def dump_vtu_3d(
    mesh,
    vtu_filepath,
    pointdata={},
    celldata={},
    ofmt="binary",
    **kwargs,
):
    vertices = mesh.vertices.as_array()
    # build 3D tabular with, for each cell the list of faces nodes in consecutive order
    cells = build_Kfaces_nodes(mesh)
    vtkw.write_vtu(
        vtkw.polyhedra_vtu_doc(
            vertices,
            cells,
            pointdata=pointdata,
            celldata=celldata,
            ofmt=ofmt,
            **kwargs,
        ),
        str(vtu_filepath),
    )


# write a vtu (paraview compatible) with the mesh and pointdata, celldata
# called if necessary by the user, the compass Dumper
# uses dump_mesh (instead of a vtu, save a npz)
def write_mesh(basename, mesh, pointdata={}, celldata={}, ofmt="binary"):
    """
    Write mesh data from a simulation as paraview vtu (single processor) or pvtu (multi processor) file.
    If there are more than one core in the communicator (multi processor case) the vtu files indexed
    in the pvtu files will be written in a vtu directory created alongside the pvtu file.

    :param basename: the output filename (paraview), proc id will be added to the name
    :param mesh: the mesh
    :param pointdata: a dictionnary of point based properties
    :param celldata: a dictionnary of cell based properties
    :param oftm: output format can be 'ascii' or 'binary' (the default)
    """
    # init filename (with proc rank if executed in parallel)
    basename = Path(basename)
    vtu_directory = create_vtu_directory(basename.parent)
    # in sequential, the filename DOES NOT contain the proc rank
    # if executed in parallel, a pvtu header file is added
    filename = vtu_directory / proc_filename(basename.name, mpi.proc_rank)

    # The following function ensures that properties have the right format
    # no copy is made if the underlying array already matches the requirements
    def check_properties(propdict):
        # possibly convert some values into numpy arrays
        propdict = {k: np.ascontiguousarray(v) for k, v in propdict.items()}
        # vtkwriter does not handle arrays of boolean so we convert them to numpy.int8
        propdict = {
            k: np.asarray(v, dtype=np.int8 if v.dtype == bool else v.dtype)
            for k, v in propdict.items()
        }
        return propdict

    # get the type of the items
    def collect_dtypes(data):
        return {
            name: a.dtype if a.ndim == 1 else (a.dtype, a.shape[1])
            for name, a in data.items()
        }

    pointdata = check_properties(pointdata)
    celldata = check_properties(celldata)

    # write vtu mesh file with properties
    vertices = mesh.vertices.as_array()
    # build 3D tabular with, for each cell the list of faces nodes in consecutive order
    cells = build_Kfaces_nodes(mesh)
    vtkw.write_vtu(
        vtkw.polyhedra_vtu_doc(
            vertices,
            cells,
            pointdata=pointdata,
            celldata=celldata,
            ofmt=ofmt,
        ),
        str(filename),
    )

    # in the multi-processor case, the master proc writes a pvtu header file
    nbprocs = mpi.communicator().size
    if nbprocs > 1 and mpi.is_on_master_proc:
        vtkw.write_pvtu(
            vtkw.pvtu_doc(
                vertices.dtype,
                [
                    str(
                        (vtu_directory / proc_filename(basename.name, pk)).relative_to(
                            basename.parent
                        )
                    )
                    for pk in range(nbprocs)
                ],
                pointdata_types=collect_dtypes(pointdata),
                celldata_types=collect_dtypes(celldata),
            ),
            str(basename),
        )


# write a npz with mesh info
# containing the vertices coord and connectivities such as
# cells_faces, faces_nodes; and info given in kwargs
def dump_mesh(
    filename,
    mesh,
    kwargs={},
):
    """
    Write mesh data from a simulation as npz files.
    Face sites info are contained elsewhere, not necessary to store anything more in the mesh info.

    :param filename: the output filename
    :param mesh: the mesh
    :param optional kwargs: a dictionary to dump with the mesh
    """
    # # WARNING:
    # # if executed in parallel, a parts.npy file is generated before partitionning
    # # even in sequentiel, the filename contains the proc rank
    vertices = mesh.vertices.as_array()
    kwargs.update({"vertices": vertices})

    def add_array(name, data):
        kwargs.update(
            {
                # offset of the connectivity (nb of targets for each source)
                f"{name}_offsets": np.cumsum([len(a) for a in data]),
                # all values in 1D storage
                f"{name}_values": np.hstack(data),
            }
        )

    # convert the connectivity into a CRS format to save it in npz
    # cannot save 2D array because the nb of targets are not necessarily homogeneous
    # todo: this structure may already exist (connectivity storage in icus!)
    def add_conn(name, conn):
        data = [np.array(targ) for targ in conn.targets()]
        add_array(name, data)

    add_conn("cells_faces", mesh.cells_faces)
    # store faces_nodes with nodes in consecutive order
    add_array("faces_nodes", faces_consecutive_nodes(mesh))

    # save all values into npz
    np.savez(filename, **kwargs)
