#pragma once

#include <cmath>
#include <compass_cxx_utils/array_utils.h>
#include <icus/Field.h>
#include <stdexcept>
#include <tuple>

namespace icmesh {
// we allow operations between vertices (i.e. array)
using namespace compass::utils::array_operators;
using Scalar = double;

template <typename Vertex> static inline Scalar norm(Vertex const &v) {
  return sqrt(sum(v * v));
}

template <typename Vertex>
static inline Scalar distance(Vertex const &v0, Vertex const &v1) {
  auto v = v1 - v0;
  return norm(v);
}

template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 3, Vertex> vector_product(const Vertex &a,
                                                              const Vertex &b) {
  return {a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2],
          a[0] * b[1] - a[1] * b[0]};
}

// compute of the unit normal vector v of the segment (a, b) in a 2D space
template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 2, Vertex> normal_vect(const Vertex &a,
                                                           const Vertex &b) {
  Vertex v = {b[1] - a[1], a[0] - b[0]};
  v /= norm(v);
  return v;
}

// compute of the unit normal vector v of the triangle (a, b, c) in a 3D space
template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 3, Vertex>
normal_vect(const Vertex &a, const Vertex &b, const Vertex &c) {
  Vertex v = vector_product<3>(b - a, c - a);
  v /= norm(v);
  return v;
}

// compute of the unit normal vector of the object defined by the points
// of coordinates coords(points) in a n-D space
template <size_t n, typename Vertex>
  requires(n >= 2)
static inline Vertex normal_vect(const icus::Field<Vertex> coords,
                                 const auto &points) {
  // check that the coordinates are defined in a n-D space
  assert(coords(0).size() == n);
  // check that enough points are provided to define an object of dim n-1
  assert(points.size() >= n);
  // minimal distance between points
  Scalar eps = 1e-9;
  // find a first point in the object
  Vertex x0 = coords(points[0]);
  // find a second point not too close to the first one (to avoid round-up error
  // in the computation)
  bool found = false;
  int i = 1;
  Vertex x1;
  while (!found && i < points.size()) {
    x1 = coords(points[i]);
    if (icmesh::distance(x0, x1) > eps)
      found = true;
    ++i;
  }
  if (!found)
    throw std::runtime_error("Degenerated object : normal_vect needs at least "
                             "two points not too close to each other.");

  if constexpr (n == 2) {
    // only two points needed
    return normal_vect<n>(x0, x1);
  } else if constexpr (n == 3) {
    // find a third point not too close to the first two and not aligned with
    // them (restart from the last value of i in the previous loop)
    found = false;
    Vertex x2;
    while (!found && i < points.size()) {
      x2 = coords(points[i]);
      if ((icmesh::distance(x0, x2) > eps) &&
          (icmesh::distance(x1, x2) > eps) &&
          (norm(vector_product<n>(x1 - x0, x2 - x0)) > eps))
        found = true;
      ++i;
    }
    if (found)
      return normal_vect<n>(x0, x1, x2);
    else
      throw std::runtime_error(
          "Degenerated object : normal_vect needs at least 3 points not "
          "aligned and not too close to each other.");
  }
}

// compute the area of a triangle (only meaningfull in 2D or 3D)
template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 1, Scalar>
triangle_area(const Vertex &a, const Vertex &b, const Vertex &c) {
  return 0.0;
}

template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 2, Scalar>
triangle_area(const Vertex &a, const Vertex &b, const Vertex &c) {
  // |det(b-a,c-a)| / 2
  Vertex ab = b - a;
  Vertex ac = c - a;
  return fabs((ab[0] * ac[1] - ab[1] * ac[0])) / 2.0;
}

template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 3, Scalar>
triangle_area(const Vertex &a, const Vertex &b, const Vertex &c) {
  // Heron's formula for triangle area
  // norm(vector product) / 2
  Vertex v = vector_product<3>(b - a, c - a);
  return norm(v) / 2.0;
}

// compute the volume of a tetrahedron
template <typename Vertex>
static inline Scalar tetra_volume(const Vertex &a, const Vertex &b,
                                  const Vertex &c, const Vertex &d) {
  constexpr auto dim = std::tuple_size<Vertex>::value;
  static_assert(dim == 3 && "tetrahedra do not exist in dimension < 3");

  // The volume of a tetrahedron of vertex a, b, c, d is
  // |det(a-d, b-d, c-d)| / 6
  auto e1 = a - d;
  auto e2 = b - d;
  auto e3 = c - d;
  auto volT = e1[0] * e2[1] * e3[2] + e2[0] * e3[1] * e1[2] +
              e3[0] * e1[1] * e2[2] - e1[0] * e2[2] * e3[1] -
              e2[0] * e3[2] * e1[1] - e3[0] * e1[2] * e2[1];
  return fabs(volT) / 6.0;
}

// compute the outward unit normal vector v of the triangle (a, b, c)
// outward with respect to point x
// and compute the area of the triangle
template <typename Vertex>
static inline auto triangle_VecNormalT(const Vertex &a, const Vertex &b,
                                       const Vertex &c, const Vertex &x) {
  constexpr auto dim = std::tuple_size<Vertex>::value;
  static_assert(dim >= 2 && "triangles do not exist in dimension < 2");

  double area; // area of the triangle
  Vertex v;    // vector normal to the triangle's plane (meaningfull only in 3D)

  if constexpr (dim == 2) {
    // the normal unit vector has no meaning when the mesh is 2D (it
    // would be out of the mesh) so keep it null
    v = {0.0, 0.0};
    area = triangle_area<dim>(a, b, c);
  } else if constexpr (dim == 3) {
    // Heron's formula for triangle area
    v = vector_product<dim>(b - a, c - a);
    area = norm(v);
    v /= area;   // unit vector
    area /= 2.0; // area = norm of v / 2

    // scalar product between (xt-x) and v to get the 'outward' sign
    Vertex xt = a + b + c;
    xt /= 3.0;
    double s = sum((xt - x) * v);
    if (s < 0.0)
      v *= -1.0;
  }

  return std::make_tuple(v, area);
}

} // namespace icmesh
