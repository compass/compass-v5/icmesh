#pragma once

#include <cstdint>
#include <icus/types.h>

namespace icmesh {

using dimension_type = std::uint_fast32_t;
using index_type = icus::index_type;

} // namespace icmesh
