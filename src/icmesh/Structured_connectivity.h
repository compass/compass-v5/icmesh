#pragma once

#include <array>
#include <cassert>
#include <vector>

#include <icus/Connectivity.h>
#include <icus/ISet.h>

#include "Grid_element.h"
#include "Lattice.h"

namespace icmesh {

template <dimension_type space_dimension> struct Structured_connectivity {
  static constexpr dimension_type dimension = space_dimension;
  static constexpr dimension_type dim = space_dimension;
  template <dimension_type d> using Element = Grid_element<d, space_dimension>;
  std::array<index_type, dim> shape;
  std::array<icus::ISet, dim + 1> elements;
  std::array<index_type, (1 << dim) + 1> offsets;
  template <int codimension>
    requires(codimension >= 0 && codimension <= space_dimension)
  bool _check_offsets() const {
    if constexpr (codimension == 0) {
      return offsets[0] == 0; // no axis blocked
    } else {
      return offsets[*(blocked_axis<codimension, dim>.begin())] == 0 &&
             _check_offsets<codimension - 1>();
    }
    return false;
  }
  template <int codimension>
    requires(codimension >= 0 && codimension <= space_dimension)
  void _generate_offsets() {
    using compass::utils::array_operators::prod;
    if constexpr (codimension == 0) {
      Axis_info<> info;
      elements[dim] = prod(axis_info_shape(info));
      assert(offsets[0] == 0); // no axis blocked
    } else {
      index_type offset = 0;
      for (auto &&info : blocked_axis<codimension, dim>) {
        offsets[info] = offset;
        offset += prod(axis_info_shape(info));
      }
      elements[dim - codimension] = offset;
      _generate_offsets<codimension - 1>();
    }
  }
  template <typename Shape> Structured_connectivity(const Shape &sh) {
    assert(sh.size() == dim);
    for (std::size_t i = 0; i < dim; ++i) {
      shape[i] = sh[i];
    }
    assert(std::ranges::all_of(shape, [](auto &&n) { return n > 0; }));
    offsets.fill(0);
    _generate_offsets<dim>();
    assert(_check_offsets<dim>());
  }
  template <std::size_t d>
  auto axis_info_shape(const Axis_info<d> &info) const {
    auto alt_shape = shape;
    for (std::size_t i = 0; i < dim; ++i) {
      if (info.is_axis_blocked(i)) {
        ++alt_shape[i];
      }
    }
    return alt_shape;
  }
  template <dimension_type d>
  bool valid_element_indices(const Element<d> &e) const {
    const auto eshape = axis_info_shape(e.axis_info);
    const auto &indices = e.indices;
    for (std::size_t i = 0; i < dim; ++i) {
      if (std::cmp_less(indices[i], 0) ||
          std::cmp_greater_equal(indices[i], eshape[i]))
        return false;
    }
    return true;
  }
  template <dimension_type d>
    requires(d <= dimension)
  auto index(const Element<d> &e) const {
    assert(valid_element_indices(e));
    const auto eshape = axis_info_shape(e.axis_info);
    index_type pos = offsets[e.axis_info];
    index_type offset = 0;
    for (dimension_type i = 0; i < dim; ++i) {
      offset += e.indices[i];
      if (i + 1 < dim)
        offset *= eshape[i + 1];
    }
    pos += offset;
    return pos;
  }
  template <dimension_type d>
    requires(d <= dimension)
  Element<d> element(index_type k) const {
    Axis_info<> axis_info;
    for (auto &&info : blocked_axis<Element<d>::codimension, dimension>) {
      assert(
          std::cmp_equal(info.blocked_dimensions(), Element<d>::codimension));
      if (offsets[info] > k)
        break;
      axis_info = info;
    }
    assert(axis_info.blocked_dimensions() == Element<d>::codimension);
    k -= offsets[axis_info];
    const auto eshape = axis_info_shape(axis_info);
    std::array<index_type, dimension> indices;
    for (dimension_type i = 0; i < dim; ++i) {
      index_type offset = 1;
      for (dimension_type j = i + 1; j < dim; ++j)
        offset *= eshape[j];
      indices[i] = k / offset;
      k -= indices[i] * offset;
    }
    assert(k == 0);
    return Element<d>{axis_info, indices};
  }
  // ds = dimension source, dt = dimension target
  template <dimension_type ds, dimension_type dt>
    requires((dt < ds) && (ds <= dimension))
  index_type *element_connectivity(const index_type i, index_type *out) const {
    if constexpr (dt + 1 == ds) {
      for (auto &&e : element<ds>(i).limits()) {
        (*out) = index(e);
        ++out;
      }
    }
    return out;
  }
  template <dimension_type d>
    requires(d <= dimension)
  void _recurse_to_vertices(const Element<d> &e, index_type *out) const {
    if constexpr (d == 0) {
      (*out) = index(e);
    } else {
      auto [first, second] = e.first_limits();
      _recurse_to_vertices(first, out);
      _recurse_to_vertices(second, out + (1 << (d - 1)));
    }
  }
  template <dimension_type ds, dimension_type dt>
    requires((dt < ds) && (ds <= dimension))
  constexpr static dimension_type connectivity_size() {
    if constexpr (dt == 0)
      return 1 << ds;
    if constexpr (dt + 1 == ds)
      return 2 * ds;
    if constexpr (ds > 2 && dt >= 1)
      return ds * (ds + 1);
    return 0;
  }
  template <dimension_type ds, dimension_type dt>
    requires((dt < ds) && (ds <= dimension))
  auto connectivity() const {
    assert(ds < elements.size());
    assert(elements[ds]);
    constexpr dimension_type n = connectivity_size<ds, dt>();
    std::vector<index_type> v;
    v.resize(elements[ds].size() * n);
    assert(v.size() == elements[ds].size() * n);
    auto p = v.data();
    if constexpr (dt == 0) {
      for (auto &&i : elements[ds]) {
        _recurse_to_vertices(element<ds>(i), p);
        p += n;
      }
    }
    if constexpr (dt != 0 && dt + 1 == ds) {
      for (auto &&i : elements[ds]) {
        p = element_connectivity<ds, dt>(i, p);
      }
    }
    if constexpr (dt != 0 && dt + 1 != ds) {
      static_assert(dt >= 1);
      static_assert(ds == 3); // the case dt=1 ds=2 has been handled by dt+1==ds
      // forward declaration for recursive call
      std::function<void(const std::size_t)> add_element;
      for (auto &&i : elements[ds]) {
        auto indices = element<ds>(i).indices;
        static_assert(size(indices) == ds);
        // copy info
        for (auto info : blocked_axis<ds - dt, ds>) {
          add_element = [&](const std::size_t k) {
            if (k >= ds) {
              (*p) = index<dt>({info, indices});
              ++p;
            } else {
              add_element(k + 1);
              if (info.is_axis_blocked(k)) {
                ++indices[k];
                add_element(k + 1);
                --indices[k];
              }
            }
          };
          add_element(0);
        }
      }
    }
    assert(std::cmp_equal(std::distance(v.data(), p), elements[ds].size() * n));
    assert(std::cmp_equal(std::distance(v.data(), p), v.size()));
    return icus::make_connectivity<n>(elements[ds], elements[dt], v);
  }
};

template <typename T> struct is_structured_connectivity {
  static constexpr bool value = false;
};

template <std::uint_fast32_t space_dimension>
struct is_structured_connectivity<Structured_connectivity<space_dimension>> {
  static constexpr bool value = true;
};

template <typename T>
inline constexpr bool is_structured_connectivity_v =
    is_structured_connectivity<T>::value;

template <std::size_t dim>
auto structured_connectivity(const std::array<std::size_t, dim> &shape) {
  return Structured_connectivity<dim>{shape};
}

template <typename... N> auto structured_connectivity(N... n) {
  constexpr std::size_t dim = sizeof...(n);
  std::array<std::size_t, dim> shape{{static_cast<std::size_t>(n)...}};
  return Structured_connectivity<dim>{shape};
}

template <typename L>
  requires is_lattice_v<L>
auto structured_connectivity(L &&lattice) {
  return structured_connectivity(lattice.shape());
}

} // namespace icmesh
