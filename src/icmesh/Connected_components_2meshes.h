#pragma once

#include <compass_cxx_utils/pretty_print.h>
#include <icmesh/Geomtraits.h>

// This utility creates 2 distinct meshes: one with the volumic mesh
// and one with the gamma faces (and 0 cells)
// todo: the mesh type of the volumic mesh should be the same than
// the given mesh.

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
//              WARNING: this function is not tested/used,
//              prefer the file Connected_components.h
//              which store main_mesh and gamma_mesh in the same mesh
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

namespace icmesh {

template <typename Mesh_t>
  requires(Mesh_t::dimension == 3)
struct Connected_components_2meshes {
  using Default_m =
      Geomtraits<Raw_mesh<Mesh_t::dimension,
                          default_connectivity_matrix<Mesh_t::dimension>>>;
  Mesh_t original_mesh;
  Default_m main_mesh;
  Default_m gamma_mesh;
  icus::Connectivity<icus::ISet, icus::ISet> faces_gamma;
  icus::Connectivity<icus::ISet, icus::ISet> nodes_gnodes;

  Connected_components_2meshes(Mesh_t &m, icus::ISet gamma) : original_mesh{m} {
    if (gamma.empty())
      throw std::runtime_error(
          "Cannot create connected components meshes with empty gamma list");
    assert(gamma.is_included_in(m.faces));

    // gama_nodes.image gives the nodes belonging to gamma
    auto gnodes = m.template get_connectivity<m.face, m.node>()
                      .extract(gamma, m.nodes)
                      .image();

    // build the set of new nodes
    auto &&[new_cn, cells_newcn, newcn_gnodes, gcells] =
        build_new_nodes(m, gamma, gnodes);

    // ----------------------------------------------------
    // build the matrix mesh, get the mapping between the new faces and gamma
    auto newf_gamma =
        build_main_mesh(m, gamma, gcells, gnodes, cells_newcn, newcn_gnodes);

    // test that faces_eges * edges_nodes is coherent with faces_nodes
    assert(test_consistency(main_mesh));

    // ----------------------------------------------------
    // build gamma mesh (nn, ne, nf, nk) with 0 cells (3D because coordinates
    // are 3D)
    build_gamma_mesh(m, gamma, gnodes);
    // store the mapping between the new faces in main_mesh and the gamma faces
    faces_gamma = icus::make_connectivity(
        icus::ISet(main_mesh.faces), icus::ISet(gamma_mesh.faces), newf_gamma);
  }

private:
  inline auto get_new_i(auto &recycle_indices, auto set_size) {
    // if there are some indices to recycle, recycle one and remove it from the
    // stock, the set_size does not change if the recycling stock is empty,
    // incremente the set_size
    if (recycle_indices.size() > 0) {
      auto new_i = recycle_indices.back();
      recycle_indices.pop_back();
      return std::make_tuple(new_i, set_size);
    }
    return std::make_tuple(set_size, set_size + 1);
  }

  inline auto new_edge_i(auto n0, auto n1, auto &recycle_indices,
                         auto edge_size, auto &newedges, auto &newedges_i,
                         auto &newe_newn) {
    auto nmin = std::min(n0, n1);
    auto nmax = std::max(n0, n1);
    auto it = std::find(newedges[nmin].begin(), newedges[nmin].end(), nmax);
    if (it != newedges[nmin].end()) {
      // edge already exists, get its index
      int it_i = std::distance(newedges[nmin].begin(), it);
      auto new_i = newedges_i[nmin][it_i];
      return std::make_tuple(new_i, edge_size);
    }
    // if not found, create the new edge
    // recycle a gedge index if possible, otherwise edge_size++
    auto &&[new_i, set_size] = get_new_i(recycle_indices, edge_size);
    // store the new edge and its index
    newedges[nmin].push_back(nmax);
    newedges_i[nmin].push_back(new_i);
    newe_newn[new_i] = {n0, n1};

    return std::make_tuple(new_i, set_size);
  }

  auto build_main_mesh(Mesh_t &m, icus::ISet gamma, icus::ISet gcells,
                       icus::ISet gnodes, auto cells_newcn, auto newcn_gnodes) {
    compass::utils::Pretty_printer print;
    // build booleans: Kgamma is true if the cell touches at least one gamma
    // nodes Fgamma is true if the face belongs to gamma
    auto Kgamma = icus::make_field<bool>(m.cells);
    Kgamma.fill(false);
    // gcells contains the gamma neighbouring cells
    Kgamma.view(gcells).fill(true);
    // the faces of the matrix mesh become mesh.faces.substract(gamma) +
    // new_faces there are as many new_faces as gamma_cells connections
    auto Fgamma = icus::make_field<bool>(m.faces);
    Fgamma.fill(false);
    Fgamma.view(gamma).fill(true);
    // store the mapping between the new face index and the corresponding gamma
    // index the new faces contain m.faces\gamma + the dupplication due to gamma
    // (gamma indices are recycled) at most 2 faces are introduced for each
    // gamma face
    std::vector<std::vector<index_type>> newf_gamma{m.faces.size() +
                                                    gamma.size()};

    // ------------------------------------------------------------------------------------
    // build the new nodes and their positions
    // the new nodes contain m.nodes\gnodes + the new_cn (gnodes indices are
    // recycled so the numbering is contigues)
    assert(gnodes.parent() == m.nodes);
    std::vector<icus::index_type> recycling_node_i = {
        gnodes.mapping().begin(), gnodes.mapping().end()}; // copy
    auto nb_nodes = m.nodes.size();                        // will be updated
    // create the new indices of the new nodes, nb_nodes contains all the new
    // mesh nodes
    auto new_cn = cells_newcn.target();
    auto newcn_i = icus::make_field<icus::index_type>(new_cn);
    for (auto &&i : new_cn) {
      auto &&[new_ni, set_size] = get_new_i(recycling_node_i, nb_nodes);
      newcn_i(i) = new_ni;
      nb_nodes = set_size;
    }
    // all the gamma indices must be recycled, at min same nb of new nodes as nb
    // of gamma node (the min is obtained if gamma is only at the mesh boundary)
    assert(recycling_node_i.size() == 0);
    assert(nb_nodes >= m.nodes.size());

    // store the new nodes positions
    auto new_nodes = icus::ISet(nb_nodes);
    auto new_pos = typename Mesh_t::Vertex_field{new_nodes};
    // fill the position with m.vertices, will modify the position of the new
    // nodes
    // cannot use new_pos.view(m.nodes) because new_nodes is not in the tree
    for (auto &&[i, pos] : m.vertices.enumerate())
      new_pos(i) = pos;
    // loop over the new nodes
    for (auto &&n : new_cn) {
      // to get its position: get its index in gnodes, gnodes.parent is m.nodes
      auto &&[gn] = newcn_gnodes.targets_by_source(n);
      // modify the position of its new index
      new_pos(newcn_i(n)) = m.vertices(gnodes.mapping()[gn]);
    }

    // ------------------------------------------------------------------------------------
    // build the new face object and the new cell_faces and faces_nodes
    // connectivities info
    // the cell iset does not change
    auto cells_faces = m.template get_connectivity<m.cell, m.face>();
    auto cells_edges = m.template get_connectivity<m.cell, m.edge>();
    auto faces_nodes = m.template get_connectivity<m.face, m.node>();
    auto faces_edges = m.template get_connectivity<m.face, m.edge>();
    auto edges_nodes = m.template get_connectivity<m.edge, m.node>();
    std::vector<std::vector<icus::index_type>> c_newf{m.cells.size()};
    // the new faces contain m.faces\gamma + the dupplication due to gamma
    // (gamma indices are recycled) at most 2 faces are introduced for each
    // gamma face
    std::vector<std::vector<icus::index_type>> newf_newn{m.faces.size() +
                                                         gamma.size()};
    std::vector<std::vector<icus::index_type>> newf_newe{m.faces.size() +
                                                         gamma.size()};
    auto gamma_i = gamma.rebase(m.faces).mapping();
    std::vector<icus::index_type> recycling_face_i = {gamma_i.begin(),
                                                      gamma_i.end()}; // copy
    auto nb_faces = m.faces.size(); // will be updated

    // idem for the new edges
    auto gamma_edges =
        m.template get_connectivity<m.face, m.edge>().extract(gamma, m.edges);
    auto gedges = gamma_edges.image();
    std::vector<icus::index_type> recycling_edge_i = {
        gedges.mapping().begin(), gedges.mapping().end()}; // copy
    auto nb_edges = m.edges.size();                        // will be updated
    // the new edges contain m.edges\gedges + the dupplication due to gamma
    // (gedges indices are recycled) at most 2 edges are introduced for each
    // gamma edge
    std::vector<std::array<icus::index_type, 2>> newe_newn{m.edges.size() +
                                                           gedges.size()};
    // does the edge belongs to gamma ?
    auto Egamma = icus::make_field<bool>(m.edges);
    Egamma.fill(false);
    Egamma.view(gedges).fill(true);

    // the two following vectors will store the edge and nodes indices
    // of the new edges only, such that
    // new_edges[n0] contains the vector of all the nodes
    // connecting to n0 such that min(n0, node) = n0,
    // new_edges_i[n0] contains the edge indices in same order
    // nb_nodes already contains the new number of nodes
    std::vector<std::vector<icus::index_type>> new_edges{nb_nodes};
    std::vector<std::vector<icus::index_type>> new_edges_i{nb_nodes};

    // fill the mapping between the indice of m.nodes/m.edges and the new index
    // (the new index depends on the cell, the mapping is updated for each cell)
    // the non-gamma nodes/edges keep the same index, the gamma nodes/edges are
    // modified
    auto map_nodes_newcn = icus::make_field<icus::index_type>(m.nodes);
    map_nodes_newcn.fill(std::views::iota(0, (int)m.nodes.size()));

    for (auto &&[K, faces] : cells_faces.targets_by_source()) {
      // init new cell_face connectivity with previous indices. If K belongs to
      // Kgamma, the values will be modified
      c_newf[K] = {faces.begin(), faces.end()};
      // if K is not a neighbouring cell, faces are not modified, neither the
      // edges nor the nodes
      if (!Kgamma(K)) {
        // not optimal, the objects are done multiple times because loop over
        // the connectivity K_objects
        for (auto &&f : faces) {
          auto f_nodes = faces_nodes.targets_by_source(f);
          newf_newn[f] = {f_nodes.begin(), f_nodes.end()};
          auto f_edges = faces_edges.targets_by_source(f);
          newf_newe[f] = {f_edges.begin(), f_edges.end()};
        }
        for (auto &&e : cells_edges.targets_by_source(K)) {
          // get indices of the nodes, are not modified
          auto &&[n0, n1] = edges_nodes.targets_by_source(e);
          newe_newn[e] = {n0, n1};
        }
      } else {
        // if K touches at least one gamma node, some nodes indices will change,
        // find the node index to init map_nodes_newcn
        auto Knewcn = cells_newcn.targets_by_source(K);
        for (auto &&n : Knewcn) {
          auto &&[gn] = newcn_gnodes.targets_by_source(n);
          // old and new indices of the node (view from cell K)
          auto m_ni = gnodes.mapping()[gn];
          auto new_ni = newcn_i(n);
          map_nodes_newcn(m_ni) = new_ni;
        }

        // init the connectivity newe_newn with the new nodes indices
        // of the edges which do not belong to gamma (they keep the same
        // indices) the edges belonging to gamma are filled in new_edge_i
        for (auto &&e : cells_edges.targets_by_source(K)) {
          if (!Egamma(e)) {
            auto &&[n0, n1] = edges_nodes.targets_by_source(e);
            newe_newn[e] = {map_nodes_newcn(n0), map_nodes_newcn(n1)};
          }
        }

        // build the connectivity K_faces with the new face indices, is init
        // with previous ones
        for (auto &&f : c_newf[K]) {
          // get connectivity with previous indices
          auto f_nodes = faces_nodes.targets_by_source(f);
          auto f_edges = faces_edges.targets_by_source(f);
          // the nodes indices may be modified even if non gamma face
          // initialize with previous indices
          // create a new vector because cannot use newf_newn[f]: f is the old
          // index, the new one is not known yet
          std::vector<icus::index_type> f_newnodes = {f_nodes.begin(),
                                                      f_nodes.end()};

          if (Fgamma(f)) {
            // modify the connectivity c_newf[K] with the new face index:
            // recycle a gamma index if possible, otherwise new_fi = nb_face++
            auto &&[new_fi, f_size] = get_new_i(recycling_face_i, nb_faces);
            // store the corresponding gamma index
            auto it = std::find(gamma_i.begin(), gamma_i.end(), f);
            // f is a gamma face so it must be contained in gamma
            assert(it != gamma_i.end());
            newf_gamma[new_fi] = {std::distance(gamma_i.begin(), it)};
            f = new_fi; // modify c_newf[K]
            nb_faces = f_size;
          } // end if Fgamma
          // initialize newf_newe[f] with previous indices, f is already the new
          // index
          newf_newe[f] = {f_edges.begin(), f_edges.end()};
          // loop over face edges (previous index of edge)
          // modification only if gamma edge
          for (auto &&e : newf_newe[f]) {
            // if gamma edge find or create new edge
            if (Egamma(e)) {
              // old indices of the nodes
              auto &&[n0, n1] = edges_nodes.targets_by_source(e);
              // get the new indices of these nodes
              auto newn0 = map_nodes_newcn(n0);
              auto newn1 = map_nodes_newcn(n1);
              // find or create the new edge, fill newe_newn
              auto &&[new_ei, e_size] =
                  new_edge_i(newn0, newn1, recycling_edge_i, nb_edges,
                             new_edges, new_edges_i, newe_newn);
              e = new_ei; // modify newf_newe[f]
              nb_edges = e_size;
            } // end gamma edge
          } // end loop over edges
          // modify the connectivity f_nodes with the new node indices
          for (auto &&n : f_newnodes)
            n = map_nodes_newcn(n);
          newf_newn[f] = f_newnodes;

        } // end loop over K faces
      }
    }
    // all the gamma indices must be recycled, at min same nb of new faces/edges
    // as nb of gamma faces/edges (the min is obtained if gamma is included in
    // the mesh boundary)
    assert(recycling_face_i.size() == 0);
    assert(nb_faces >= m.faces.size());
    assert(recycling_edge_i.size() == 0);
    assert(nb_edges >= m.edges.size());
    newf_newn.resize(nb_faces);
    newf_newe.resize(nb_faces);
    newf_gamma.resize(nb_faces);
    newe_newn.resize(nb_edges);

    // build the new mesh
    // Todo: should have the same structure as the given mesh.
    // Maybe difficult with the structured mesh because
    // many info are not stored ???
    // creates new mesh isets
    main_mesh = mesh(nb_nodes, nb_edges, nb_faces, m.cells.size());
    main_mesh.vertices.fill(new_pos);
    main_mesh.template set_connectivity<Mesh_t::cell, Mesh_t::face>(c_newf);
    main_mesh.template set_connectivity<Mesh_t::face, Mesh_t::edge>(newf_newe);
    main_mesh.template set_connectivity<Mesh_t::face, Mesh_t::node>(newf_newn);
    main_mesh.template set_connectivity<Mesh_t::edge, Mesh_t::node>(newe_newn);
    return newf_gamma;
  }

  void build_gamma_mesh(Mesh_t &m, icus::ISet gamma, icus::ISet gnodes) {
    // build the new connectivities
    auto gamma_edges =
        m.template get_connectivity<m.face, m.edge>().extract(gamma, m.edges);
    auto gedges = gamma_edges.image();
    auto gamma_gedges = gamma_edges.restrict_target(gedges);
    auto gedges_gnodes = m.template get_connectivity<m.edge, m.node>()
                             .extract(gedges, m.nodes)
                             .template restrict_target<2>(gnodes);

    // build new mesh with new ISets created with their sizes
    // build gamma mesh (nn, ne, nf, nk) with 0 cells (3D because coordinates
    // are 3D)
    gamma_mesh = mesh(gnodes.size(), gedges.size(), gamma.size(), 0);
    gamma_mesh.vertices.fill(m.vertices.extract(gnodes));
    gamma_mesh.template set_connectivity<Mesh_t::face, Mesh_t::edge>(
        gamma_gedges.targets());
    assert(icus::is_regular_connectivity<decltype(gedges_gnodes)>);
    assert(gedges_gnodes.targets_size_v == 2);
    gamma_mesh.template set_connectivity<Mesh_t::edge, Mesh_t::node>(
        gedges_gnodes.targets());
  }

  auto build_new_nodes(Mesh_t &m, icus::ISet gamma, icus::ISet gnodes) {

    // duplicates the nodes belonging to gamma such as:
    // as many nodes as neighbouring cells
    // ie as many nodes as non-zero in the cells_gammanodes connectivity
    auto cells_gnodes =
        m.template get_connectivity<m.cell, m.node>().restrict_target(gnodes);
    // possible new nodes due to the gamma presence (cn stands for cell-nodes)
    auto tmp_cn = icus::ISet(cells_gnodes.connections().size());
    // build cn_cells connectivity: each cn connects one cell
    // and build cn_gnodes connectivity: each cn connects one gamma node (its
    // position)
    std::vector<icus::index_type> cnc, cnn;
    cnc.reserve(tmp_cn.size());
    cnn.reserve(tmp_cn.size());
    for (auto &&[c, gn] : cells_gnodes.targets_by_source()) {
      for (auto &&n : gn) {
        cnc.push_back(c);
        cnn.push_back(n);
      }
    }
    // todo: why it is mandatory to convert m.cells into iset in the following
    // ???
    auto cn_cells =
        icus::make_connectivity<1>(tmp_cn, icus::ISet(m.cells), cnc);
    auto cn_gnodes = icus::make_connectivity<1>(tmp_cn, gnodes, cnn);

    // build cells_nongamma connectivity
    auto nongamma = m.faces.substract(gamma);
    auto cells_nongamma =
        m.template get_connectivity<m.cell, m.face>().restrict_target(nongamma);

    // build cn_cn such that 2 nodes are connected if they belong to cells whose
    // connecting face does not belong to gamma cn_cn = cn_cells *
    // cells_nongamma
    // * transpose(cells_nongamma) * transpose(cn_cells): cn_cn = cn_cells *
    // cells_nongamma * transpose(cn_cells * cells_nongamma)
    auto cn_nongamma = cn_cells.compose(cells_nongamma);
    auto cn_cn_nongamma = cn_nongamma.compose(cn_nongamma.transpose());
    // build cn_cn such that 2 nodes are connected if they have the same
    // position
    auto cn_cn_pos = cn_gnodes.compose(cn_gnodes.transpose());
    // compute the intersection between the 2 cn_cn connectivities (term by term
    // multiplication)
    auto cn_cn = icus::intersect(cn_cn_pos, cn_cn_nongamma);

    // creates a vector with cell-nodes size to create the
    // connected components
    // keep the index of the smallest cn connected
    std::vector<icus::index_type> connected_cn(tmp_cn.size());
    // initialize with range(size)
    std::iota(connected_cn.begin(), connected_cn.end(), 0);
    bool new_it = true;
    while (new_it) {
      auto previous_cn = connected_cn;
      for (auto &&[cni, nodes] : cn_cn.targets_by_source()) {
        auto min_cn = tmp_cn.size();
        for (auto &&n : nodes)
          min_cn = std::min(min_cn, connected_cn[n]);

        // change values of all nodes to the min
        for (auto &&n : nodes)
          connected_cn[n] = min_cn;
      }

      // if previous is distinct from connected_cn, do one more iteration
      new_it = false;
      for (auto &&[prev, ccn] : std::views::zip(previous_cn, connected_cn)) {
        // compare integers
        if (std::fabs(prev - ccn) > 1.e-2) {
          new_it = true;
          break;
        }
      }
    }

    // remove dupplicates
    std::set<icus::index_type> new_cn_set(connected_cn.begin(),
                                          connected_cn.end());
    auto new_cn = tmp_cn.extract(new_cn_set);

    // modify connected_cn with the new cn indices, fill the tmp mapping
    // map_tmpcn_newcn
    auto map_tmpcn_newcn = icus::make_field<icus::index_type>(tmp_cn);
    for (auto &&[i, map] : new_cn.enumerate_mapping())
      map_tmpcn_newcn(map) = i;

    for (auto &&val : connected_cn)
      val = map_tmpcn_newcn(val);

    // build the connectivity cells_newcn which connects the mesh cells
    // to the new iset of nodes, same structure as cells_gnodes,
    // but the values are given by connected_cn
    std::vector<std::vector<icus::index_type>> cells_newcn_vect{m.cells.size()};
    auto compt = 0;
    for (auto &&[K, nodes] : cells_gnodes.targets_by_source()) {
      cells_newcn_vect[K] = {connected_cn.begin() + compt,
                             connected_cn.begin() + compt + nodes.size()};
      // cells_newcn_vect[K]);
      compt += nodes.size();
    }
    assert(compt == connected_cn.size());
    assert(compt == cells_gnodes.connections().size());
    auto cells_newcn =
        icus::make_connectivity(m.cells, new_cn, cells_newcn_vect);

    // build the mapping between the new nodes and their indices in m.nodes
    auto newcn_gnodes = cn_gnodes.extract(new_cn, gnodes);
    assert(newcn_gnodes.targets_size_v == 1);

    // cn_cells.image() gives the gamma neighbouring cells (each cells touching
    // at least one gamma node)
    return std::make_tuple(new_cn, cells_newcn, newcn_gnodes, cn_cells.image());
  }

  bool test_consistency(Default_m &m) {
    auto fn = m.template get_connectivity<m.face, m.node>();
    // returns an ordered connectivity
    auto fn_comp = m.template get_connectivity<m.face, m.edge>().compose(
        m.template get_connectivity<m.edge, m.node>());
    assert(fn_comp.has_sorted_targets());
    for (auto &&[ts, tsc] : std::views::zip(fn.targets(), fn_comp.targets())) {
      if (ts.size() != tsc.size()) {
        compass::utils::Pretty_printer print;
        print("Error ! face_node is", ts, "face_edge * edge_node is", tsc);
        return false;
      }
      for (auto &&t : ts) {
        // look for t in tsc (which is sorted)
        if (!std::binary_search(tsc.begin(), tsc.end(), t)) {
          compass::utils::Pretty_printer print;
          print("Error ! face_node is", ts, "face_edge * edge_node is", tsc);
          return false;
        }
      }
    }
    return true;
  }
};

} // namespace icmesh
