#pragma once

#include <compass_cxx_utils/array_utils.h>
#include <icus/Field.h>

#include "Connectivity_matrix.h"
#include "Mesh_elements.h"

namespace icmesh {

template <std::size_t dim, typename CMat,
          typename Vertex = std::array<double, dim>>
  requires(dim > 0 && dim <= 3 && is_connectivity_matrix_v<CMat> &&
           CMat::dimension == dim)
struct Raw_mesh : Mesh_elements<dim> {
  static constexpr std::size_t dimension = dim;
  using vertex_type = Vertex;
  using Vertex_field = icus::Field<Vertex>;
  using shape_type = std::array<size_t, dim>;
  using Bounding_box = std::pair<vertex_type, vertex_type>;
  std::optional<Bounding_box> bounding_box;
  std::optional<shape_type> shape;
  CMat _connectivity;
  Vertex_field vertices;
  // ordered_faces_nodes is True if the connectivity faces_nodes is in "good"
  // order which means that (node[n], node[n+1]) is an edge
  bool ordered_faces_nodes;
  Raw_mesh() = default;
  template <typename E, typename T>
    requires(is_connectivity_matrix_v<std::decay_t<T>>)
  Raw_mesh(E &&elts, T &&con)
      : Mesh_elements<dim>{std::forward<E>(elts)},
        _connectivity{std::forward<T>(con)},
        vertices{Mesh_elements<dim>::nodes}, ordered_faces_nodes{false} {}
  // template <typename E, typename T, typename Pts>
  //   requires(is_connectivity_matrix_v<std::decay_t<T>> &&
  //   std::ranges::range<Pts>)
  // Raw_mesh(E &&elts, T &&con, Pts&& points)
  //     : Mesh_elements<dim>{std::forward<E>(elts)}, _connectivity{
  //                                                      std::forward<T>(con)},
  //                                                      vertices{Mesh_elements<dim>::nodes,
  //                                                      std::forward<Pts>(points)},
  //                                                      ordered_faces_nodes{false}
  //                                                      {}

  template <typename E, typename T, typename vProp>
    requires(is_connectivity_matrix_v<
                std::decay_t<T>>) // FIXME: require icus::is_property_v<vProp>
  Raw_mesh(E &&elts, T &&con, vProp &&vprop)
      : Mesh_elements<dim>{std::forward<E>(elts)},
        _connectivity{std::forward<T>(con)},
        vertices{std::forward<vProp>(vprop)}, ordered_faces_nodes{false} {}

  template <typename... X>
    requires(sizeof...(X) == 0 || sizeof...(X) == dimension)
  static vertex_type build_vertex(X &&...x) {
    if constexpr (sizeof...(X) == 0) {
      vertex_type v;
      for (auto &&x : v)
        x = 0;
      return v;
    } else {
      using scalar_type = typename vertex_type::value_type;
      return vertex_type{static_cast<scalar_type>(x)...};
    }
  }

  template <std::size_t source_i, std::size_t target_i>
  bool has_connectivity() const {
    return (bool)_connectivity.template connectivity<source_i, target_i>();
  }

  template <std::size_t source_i, std::size_t target_i>
  auto &get_connectivity() {
    static_assert(source_i != target_i);
    if (!has_connectivity<source_i, target_i>()) {
      auto ok = build_connectivity<source_i, target_i>();
      assert(ok);
    }
    return _connectivity.template connectivity<source_i, target_i>();
  }

  /** Should we do a copy of the connectivity ??? */
  template <std::size_t source_i, std::size_t target_i, typename Con>
    requires(icus::is_connectivity<Con>)
  void set_connectivity(Con con) {
    static_assert(source_i != target_i);
    assert(con.source() == this->elements[source_i]);
    assert(con.target() == this->elements[target_i]);
    auto &C = _connectivity.template connectivity<source_i, target_i>();
    C = con;
  }

  /** this will preserve connectivity targets order which is
   *  usually the expected behavior for meshes */
  template <std::size_t source_i, std::size_t target_i, typename... Ts>
  void set_connectivity(Ts &&...ts) {
    static_assert(source_i != target_i);
    auto &C = _connectivity.template connectivity<source_i, target_i>();
    C = std::decay_t<decltype(C)>{this->elements[source_i],
                                  this->elements[target_i],
                                  std::forward<Ts>(ts)..., true};
  }

  template <std::size_t source_i, std::size_t target_i>
  bool build_connectivity() {
    static_assert(source_i != target_i);
    auto &con = _connectivity.template connectivity<source_i, target_i>();
    if (!con) {
      // if the transposed of the required connectivity exists,
      // build using transpose
      auto &transposed =
          _connectivity.template connectivity<target_i, source_i>();
      if (transposed) {
        con = transposed.template transpose<con.targets_size_v>();
        assert(con);
        assert(con.source() == this->elements[source_i]);
        assert(con.target() == this->elements[target_i]);
        return true;
        // else build with compose (and transpose if necessary)
      } else {
        constexpr std::size_t min_i = std::min(source_i, target_i);
        constexpr std::size_t max_i = std::max(source_i, target_i);
        // max - min is 2 or 3 because 0<= i, j <= DIM <= 3
        // max - min = 0 has no default value
        // and max - min = 1 should be already initialized
        assert(max_i - min_i == 2 || max_i - min_i == 3);
        // assert(!_connectivity.template connectivity<max_i, min_i>());
        if constexpr (max_i - min_i == 2) {
          // Conn(i+1, i) is initialized
          auto &con1i = _connectivity.template connectivity<min_i + 1, min_i>();
          auto &con21 =
              _connectivity.template connectivity<min_i + 2, min_i + 1>();
          assert(con1i && con21);
          auto &con2i = _connectivity.template connectivity<min_i + 2, min_i>();
          con2i = con21.template compose<icus::ISet, con1i.targets_size_v,
                                         con2i.targets_size_v>(con1i);
          // assert(get_connectivity<max, min>());
          // will compute tranpose if necessary
          return build_connectivity<source_i, target_i>();
        } else if constexpr (max_i - min_i == 3) {
          auto &con3i = _connectivity.template connectivity<min_i + 3, min_i>();
          if (_connectivity.template connectivity<min_i + 2, min_i>()) {
            auto &con2i =
                _connectivity.template connectivity<min_i + 2, min_i>();
            auto &con32 =
                _connectivity.template connectivity<min_i + 3, min_i + 2>();
            // Conn(i+1, i) are known
            assert(con32);
            con3i = con32.template compose<icus::ISet, con2i.targets_size_v,
                                           con3i.targets_size_v>(con2i);
          } else if (_connectivity
                         .template connectivity<min_i + 3, min_i + 1>()) {
            auto &con1i =
                _connectivity.template connectivity<min_i + 1, min_i>();
            // Conn(i+1, i) are known
            assert(con1i);
            auto &con31 =
                _connectivity.template connectivity<min_i + 3, min_i + 1>();
            con3i = con31.template compose<icus::ISet, con1i.targets_size_v,
                                           con3i.targets_size_v>(con1i);
          } else {
            auto ok = build_connectivity<min_i + 2, min_i>();
            assert(ok);
            ok = build_connectivity<max_i, min_i>();
            assert(ok);
          }
          // will compute tranpose if necessary
          // assert(get_connectivity<max, min>());
          return build_connectivity<source_i, target_i>();
        }
      } // end if (transposed)
      return false;
    } // end if (!con)
    assert(con.source() == this->elements[source_i]);
    assert(con.target() == this->elements[target_i]);
    return true;
  }
};

// template deduction guide
template <typename E, typename CMat>
  requires(is_connectivity_matrix_v<std::decay_t<CMat>>)
Raw_mesh(E &&, CMat &&) -> Raw_mesh<CMat::dimension, std::decay_t<CMat>>;

template <typename E, typename CMat, typename vProp>
  requires(is_connectivity_matrix_v<
           std::decay_t<CMat>>) // FIXME: require icus::is_property_v<vProp>
Raw_mesh(E &&, CMat &&, vProp &&)
    -> Raw_mesh<CMat::dimension, std::decay_t<CMat>>;

template <typename StCon>
  requires(is_structured_connectivity_v<std::decay_t<StCon>>)
auto raw_mesh(StCon &&sc) {
  return Raw_mesh{sc.elements, connectivity_matrix(sc)};
}

template <typename L>
  requires is_lattice_v<L>
auto raw_mesh(L &&lattice) {
  using compass::utils::array_operators::prod;
  auto sc = structured_connectivity(lattice.elements_shape());
  auto nodes = sc.elements[0];
  auto vertices = icus::make_field<typename L::Vertex>(nodes);
  for (auto &&i : sc.elements[0]) {
    vertices(i) = lattice(sc.template element<0>(i).indices);
  }
  auto raw_mesh = Raw_mesh{sc.elements, connectivity_matrix(sc), vertices};
  raw_mesh.bounding_box =
      std::make_pair(lattice.origin(), lattice.origin_opposite());
  typename decltype(raw_mesh)::shape_type shape;
  for (std::size_t i = 0; i < sc.shape.size(); ++i) {
    shape[i] = sc.shape[i];
  }
  raw_mesh.shape = shape;
  return raw_mesh;
}

template <std::integral... I>
  requires(sizeof...(I) > 1)
auto raw_mesh(const I... i) {
  auto r_mesh = Raw_mesh{mesh_elements(icus::ISet{i}...),
                         default_connectivity_matrix<sizeof...(I) - 1>{}};
  // init vertices as a Field of Vertex whose support is r_mesh.nodes
  r_mesh.vertices = typename decltype(r_mesh)::Vertex_field(r_mesh.nodes);
  return r_mesh;
}

// triangle (2D) or tetra (3D) mesh
template <std::integral... I>
  requires(sizeof...(I) > 1)
auto raw_mesh_tetra(const I... i) {
  auto r_mesh = Raw_mesh{mesh_elements(icus::ISet{i}...),
                         connectivity_matrix_tetra<sizeof...(I) - 1>{}};
  // init vertices as a Field of Vertex whose support is r_mesh.nodes
  r_mesh.vertices = typename decltype(r_mesh)::Vertex_field(r_mesh.nodes);
  return r_mesh;
}

// quadrangle (2D) or cuboid (3D) mesh
template <std::integral... I>
  requires(sizeof...(I) > 1)
auto raw_mesh_cuboid(const I... i) {
  auto r_mesh = Raw_mesh{mesh_elements(icus::ISet{i}...),
                         structured_connectivity_matrix<sizeof...(I) - 1>{}};
  // init vertices as a Field of Vertex whose support is r_mesh.nodes
  r_mesh.vertices = typename decltype(r_mesh)::Vertex_field(r_mesh.nodes);
  return r_mesh;
}

} // namespace icmesh
