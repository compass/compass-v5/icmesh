import numpy as np
import meshio
from compass_utils import messages
from icmesh import mesh, mesh_tetra, mesh_cuboid


def default_mesh(filename):
    # Node ordering in cells can be found here https://github.com/nschloe/meshio/wiki/Node-ordering-in-cells
    given_mesh = meshio.read(filename)
    # get mesh connectivity
    faces_nodes = []
    all_triangles_edges = []
    all_triangles_nodes = []
    all_quadrangles_edges = []
    all_quadrangles_nodes = []
    all_edges_nodes = []
    cells_faces = []
    cells_triangles = []
    cells_quadrangles = []
    cells_nodes = []
    cells_types = []
    tfi = 0
    qfi = 0
    fi = 0
    ei = 0
    mesh_type = None
    for vtktype, c_nodes in given_mesh.cells_dict.items():
        nb_cells = len(c_nodes)
        # tetrahedral cells
        if vtktype == "tet" or vtktype == "tetra":
            if mesh_type is not None:
                mesh_type = "default"
            else:
                mesh_type = vtktype
            n_Kfaces = 4
            c_types = np.full((nb_cells,), 10, dtype=int)
            cells_types.extend(c_types)
            cells_nodes.extend(c_nodes.tolist())
            for nodes in c_nodes:
                tri_faces = []
                qua_faces = []
                # fill edges
                # Each tetrahedron has 6 edges, defined by pairs of nodes
                all_edges_nodes.append(np.sort([nodes[0], nodes[1]]))  # ei
                all_edges_nodes.append(np.sort([nodes[0], nodes[2]]))  # ei + 1
                all_edges_nodes.append(np.sort([nodes[0], nodes[3]]))  # ei + 2
                all_edges_nodes.append(np.sort([nodes[1], nodes[2]]))  # ei + 3
                all_edges_nodes.append(np.sort([nodes[1], nodes[3]]))  # ei + 4
                all_edges_nodes.append(np.sort([nodes[2], nodes[3]]))  # ei + 5

                # fill faces
                all_triangles_nodes.append(np.asarray([nodes[0], nodes[1], nodes[2]]))
                all_triangles_edges.append(np.asarray([ei, ei + 1, ei + 3]))

                all_triangles_nodes.append(np.asarray([nodes[0], nodes[1], nodes[3]]))
                all_triangles_edges.append(np.asarray([ei, ei + 2, ei + 4]))

                all_triangles_nodes.append(np.asarray([nodes[1], nodes[2], nodes[3]]))
                all_triangles_edges.append(np.asarray([ei + 3, ei + 4, ei + 5]))

                all_triangles_nodes.append(np.asarray([nodes[2], nodes[0], nodes[3]]))
                all_triangles_edges.append(np.asarray([ei + 1, ei + 2, ei + 5]))

                ei += 6

                # append both triangles and quadrangles to have a same length (cell size)
                cells_triangles.append(np.arange(tfi, tfi + n_Kfaces))
                tfi += n_Kfaces
                cells_quadrangles.append(np.asarray(qua_faces))

        # hexahedron with the list of face, for each face the list of nodes
        elif vtktype == "polyhedron8":
            if mesh_type is not None:
                mesh_type = "default"
            else:
                mesh_type = "hexahedron"  # to check if it is same as "polyhedron8" or structured
            c_types = np.full((nb_cells,), 100, dtype=int)  # todo: which type ???
            cells_types.extend(c_types)
            # loop over each cell
            for K_faces_nodes in c_nodes:
                n_Kfaces = len(K_faces_nodes)
                cells_nodes.append(np.unique(K_faces_nodes).tolist())
                # loop over each face
                tri_faces = []
                qua_faces = []
                for f_nodes in K_faces_nodes:
                    # fill edges
                    all_edges_nodes.append(np.sort([f_nodes[0], f_nodes[-1]]))
                    for i in range(0, len(f_nodes) - 1):
                        all_edges_nodes.append(np.sort([f_nodes[i], f_nodes[i + 1]]))
                    # fill faces
                    if len(f_nodes) == 3:
                        all_triangles_edges.append(np.arange(ei, ei + 3))
                        all_triangles_nodes.append(np.asarray(f_nodes))
                        ei += 3
                        tri_faces.append(tfi)
                        tfi += 1
                    elif len(f_nodes) == 4:
                        all_quadrangles_edges.append(np.arange(ei, ei + 4))
                        all_quadrangles_nodes.append(np.asarray(f_nodes))
                        ei += 4
                        qua_faces.append(qfi)
                        qfi += 1
                    else:
                        messages.error(
                            f"face type ({len(f_nodes)} nodes) not recognized in polyhedron8 in meshio2compass"
                        )
                # cells_faces, distinguish triangles from quadrangles for the new numbering (done later)
                cells_triangles.append(np.asarray(tri_faces))
                cells_quadrangles.append(np.asarray(qua_faces))

        # voxel is a regular hexahedron (not the same nodes numbering)
        elif vtktype == "voxel":
            # messages.error(f"cell type {vtktype} not recognized in meshio2compass")
            if mesh_type is not None:
                mesh_type = "default"
            else:
                mesh_type = vtktype
            n_Kfaces = 6
            c_types = np.full((nb_cells,), 12, dtype=int)
            cells_types.extend(c_types)
            cells_nodes.extend(c_nodes.tolist())
            for nodes in c_nodes:
                # face 0
                # faces_nodes.append([nodes[0], nodes[1], nodes[3], nodes[2]])
                all_edges_nodes.append(np.sort([nodes[0], nodes[1]]))  # ei
                all_edges_nodes.append(np.sort([nodes[1], nodes[3]]))  # ei + 1
                all_edges_nodes.append(np.sort([nodes[2], nodes[3]]))  # ei + 2
                all_edges_nodes.append(np.sort([nodes[0], nodes[2]]))  # ei + 3
                # same index of the face in quadrangles_edges and in quadrangles_nodes
                all_quadrangles_edges.append(np.arange(ei, ei + 4))
                all_quadrangles_nodes.append(np.asarray(nodes[:4]))

                # face 1
                # faces_nodes.append([nodes[4], nodes[5], nodes[7], nodes[6]])
                all_edges_nodes.append(np.sort([nodes[4], nodes[5]]))  # ei + 4
                all_edges_nodes.append(np.sort([nodes[5], nodes[7]]))  # ei + 5
                all_edges_nodes.append(np.sort([nodes[6], nodes[7]]))  # ei + 6
                all_edges_nodes.append(np.sort([nodes[4], nodes[6]]))  # ei + 7

                all_quadrangles_edges.append(np.arange(ei + 4, ei + 8))
                all_quadrangles_nodes.append(np.asarray(nodes[4:]))

                # face 2
                # faces_nodes.append([nodes[0], nodes[1], nodes[5], nodes[4]])
                all_edges_nodes.append(np.sort([nodes[1], nodes[5]]))  # ei + 8
                all_edges_nodes.append(np.sort([nodes[0], nodes[4]]))  # ei + 9

                all_quadrangles_edges.append(np.asarray([ei, ei + 8, ei + 4, ei + 9]))
                all_quadrangles_nodes.append(
                    np.asarray([nodes[0], nodes[1], nodes[5], nodes[4]])
                )

                # face 3
                # faces_nodes.append([nodes[1], nodes[3], nodes[7], nodes[5]])
                all_edges_nodes.append(np.sort([nodes[3], nodes[7]]))  # ei + 10

                all_quadrangles_edges.append(
                    np.asarray([ei + 1, ei + 10, ei + 5, ei + 8])
                )
                all_quadrangles_nodes.append(
                    np.asarray([nodes[1], nodes[3], nodes[7], nodes[5]])
                )

                # face 4
                # faces_nodes.append([nodes[3], nodes[2], nodes[6], nodes[7]])
                all_edges_nodes.append(np.sort([nodes[2], nodes[6]]))  # ei + 11

                all_quadrangles_edges.append(
                    np.asarray([ei + 2, ei + 11, ei + 6, ei + 10])
                )
                all_quadrangles_nodes.append(
                    np.asarray([nodes[3], nodes[2], nodes[6], nodes[7]])
                )

                # face 5
                # faces_nodes.append([nodes[2], nodes[0], nodes[4], nodes[6]])
                # all edges_nodes are already defined
                all_quadrangles_edges.append(
                    np.asarray([ei + 3, ei + 9, ei + 7, ei + 11])
                )
                all_quadrangles_nodes.append(
                    np.asarray([nodes[2], nodes[0], nodes[4], nodes[6]])
                )

                # cells_faces, distinguish triangles from quadrangles for the new numbering (done later)
                cells_triangles.append(np.array([], dtype=int))
                cells_quadrangles.append(np.arange(qfi, qfi + n_Kfaces))
                qfi += n_Kfaces
                ei += 12

        # hexahedron cells
        elif vtktype == "hexahedron":
            if mesh_type is not None:
                mesh_type = "default"
            else:
                mesh_type = vtktype
            n_Kfaces = 6
            c_types = np.full((nb_cells,), 12, dtype=int)
            cells_types.extend(c_types)
            cells_nodes.extend(c_nodes.tolist())
            # edges and faces numbering without knowing the index in neighbouring cells
            # duplicates are removed later
            for nodes in c_nodes:
                # face 0
                all_edges_nodes.append(np.sort([nodes[0], nodes[1]]))  # ei
                all_edges_nodes.append(np.sort([nodes[1], nodes[2]]))  # ei + 1
                all_edges_nodes.append(np.sort([nodes[2], nodes[3]]))  # ei + 2
                all_edges_nodes.append(np.sort([nodes[0], nodes[3]]))  # ei + 3
                # same index of the face in triangles_edges and in triangles_nodes
                # same index of the face in quadrangles_edges and in quadrangles_nodes
                all_quadrangles_edges.append(np.arange(ei, ei + 4))
                all_quadrangles_nodes.append(np.asarray(nodes[:4]))
                # face 1
                all_edges_nodes.append(np.sort([nodes[4], nodes[5]]))  # ei + 4
                all_edges_nodes.append(np.sort([nodes[5], nodes[6]]))  # ei + 5
                all_edges_nodes.append(np.sort([nodes[6], nodes[7]]))  # ei + 6
                all_edges_nodes.append(np.sort([nodes[4], nodes[7]]))  # ei + 7
                # all_quadrangles_edges.append(np.arange(ei + 4, ei + 7))
                # ei + 8 not ei + 7
                all_quadrangles_edges.append(np.arange(ei + 4, ei + 8))
                all_quadrangles_nodes.append(np.asarray(nodes[4:]))
                # face 2
                all_edges_nodes.append(np.sort([nodes[1], nodes[5]]))  # ei + 8
                all_edges_nodes.append(np.sort([nodes[0], nodes[4]]))  # ei + 9
                all_quadrangles_edges.append(np.asarray([ei, ei + 8, ei + 4, ei + 9]))
                all_quadrangles_nodes.append(
                    np.asarray([nodes[0], nodes[1], nodes[5], nodes[4]])
                )
                # face 3
                all_edges_nodes.append(np.sort([nodes[2], nodes[6]]))  # ei + 10
                all_edges_nodes.append(np.sort([nodes[3], nodes[7]]))  # ei + 11
                all_quadrangles_edges.append(
                    np.asarray([ei + 2, ei + 10, ei + 6, ei + 11])
                )
                all_quadrangles_nodes.append(
                    np.asarray([nodes[3], nodes[2], nodes[6], nodes[7]])
                )
                # face 4
                all_quadrangles_edges.append(
                    np.asarray([ei + 3, ei + 11, ei + 7, ei + 9])
                )
                all_quadrangles_nodes.append(
                    np.asarray([nodes[0], nodes[3], nodes[7], nodes[4]])
                )
                # face 5
                all_quadrangles_edges.append(
                    np.asarray([ei + 1, ei + 10, ei + 5, ei + 8])
                )
                all_quadrangles_nodes.append(
                    np.asarray([nodes[1], nodes[2], nodes[6], nodes[5]])
                )
                # cells_faces, distinguish triangles from quadrangles for the new numbering (done later)
                cells_triangles.append(np.array([], dtype=int))
                cells_quadrangles.append(np.arange(qfi, qfi + n_Kfaces))
                qfi += n_Kfaces
                ei += 12

        # wedge cells
        elif vtktype == "wedge":
            if mesh_type is not None:
                mesh_type = "default"
            else:
                mesh_type = vtktype
            c_types = np.full((nb_cells,), 13, dtype=int)
            cells_types.extend(c_types)
            cells_nodes.extend(c_nodes.tolist())
            # store all edges, faces of the cell as new object, np.unique is done later
            for nodes in c_nodes:
                # face 0
                all_edges_nodes.append(np.sort([nodes[0], nodes[1]]))  # ei
                all_edges_nodes.append(np.sort([nodes[1], nodes[2]]))  # ei + 1
                all_edges_nodes.append(np.sort([nodes[0], nodes[2]]))  # ei + 2
                # same index of the face in triangles_edges and in triangles_nodes
                all_triangles_edges.append(np.arange(ei, ei + 3))
                all_triangles_nodes.append(np.asarray(nodes[:3]))
                # face 1
                all_edges_nodes.append(np.sort([nodes[3], nodes[4]]))  # ei + 3
                all_edges_nodes.append(np.sort([nodes[4], nodes[5]]))  # ei + 4
                all_edges_nodes.append(np.sort([nodes[3], nodes[5]]))  # ei + 5
                # same index of the face in triangles_edges and in triangles_nodes
                all_triangles_edges.append(np.arange(ei + 3, ei + 6))
                all_triangles_nodes.append(np.asarray(nodes[3:]))
                # face 2
                all_edges_nodes.append(np.sort([nodes[1], nodes[4]]))  # ei + 6
                all_edges_nodes.append(np.sort([nodes[0], nodes[3]]))  # ei + 7
                # same index of the face in quadrangles_edges and in quadrangles_nodes
                all_quadrangles_edges.append(np.asarray([ei, ei + 3, ei + 6, ei + 7]))
                all_quadrangles_nodes.append(
                    np.asarray([nodes[0], nodes[1], nodes[4], nodes[3]])
                )
                # face 3
                all_edges_nodes.append(np.sort([nodes[2], nodes[5]]))  # ei + 8
                # same index of the face in quadrangles_edges and in quadrangles_nodes
                all_quadrangles_edges.append(
                    np.asarray([ei + 2, ei + 5, ei + 7, ei + 8])
                )
                all_quadrangles_nodes.append(
                    np.asarray([nodes[0], nodes[3], nodes[5], nodes[2]])
                )
                # face 4
                # same index of the face in quadrangles_edges and in quadrangles_nodes
                all_quadrangles_edges.append(
                    np.asarray([ei + 1, ei + 4, ei + 6, ei + 8])
                )
                all_quadrangles_nodes.append(
                    np.asarray([nodes[1], nodes[4], nodes[5], nodes[2]])
                )
                # cells_faces, distinguish triangles from quadrangles for the new numbering (done later)
                cells_triangles.append(np.arange(tfi, tfi + 2))
                cells_quadrangles.append(np.arange(qfi, qfi + 3))
                tfi += 2
                qfi += 3
                ei += 9

        # check the pyramid keyword !
        elif vtktype == "pyramid":
            if mesh_type is not None:
                mesh_type = "default"
            else:
                mesh_type = vtktype
            n_Kfaces = 5
            c_types = np.full((nb_cells,), 14, dtype=int)
            cells_types.extend(c_types)
            cells_nodes.extend(c_nodes.tolist())
            for nodes in c_nodes:
                tri_faces = []
                qua_faces = []
                # fill edges
                # Each pyramid has 8 edges, defined by pairs of nodes
                all_edges_nodes.append(np.sort([nodes[0], nodes[3]]))  # ei
                for i in range(0, len(nodes) - 2):
                    # ei + 1, ei + 2, ei + 3
                    all_edges_nodes.append(np.sort([nodes[i], nodes[i + 1]]))

                for i in range(0, len(nodes) - 1):
                    # ei + 4, ei + 5, ei + 6, ei + 7
                    all_edges_nodes.append(np.sort([nodes[i], nodes[len(nodes) - 1]]))

                # fill face quadrangle
                all_quadrangles_nodes.append(np.asarray(nodes[:4]))  # four first nodes
                qua_faces.append(qfi)
                qfi += 1
                all_quadrangles_edges.append(np.arange(ei, ei + 4))

                # fill face
                all_triangles_nodes.append([nodes[0], nodes[1], nodes[4]])
                all_triangles_edges.append(np.asarray([ei + 1, ei + 4, ei + 5]))

                all_triangles_nodes.append([nodes[1], nodes[2], nodes[4]])
                all_triangles_edges.append(np.asarray([ei + 2, ei + 5, ei + 6]))

                all_triangles_nodes.append([nodes[2], nodes[3], nodes[4]])
                all_triangles_edges.append(np.asarray([ei + 3, ei + 6, ei + 7]))

                all_triangles_nodes.append([nodes[3], nodes[0], nodes[4]])
                all_triangles_edges.append(np.asarray([ei, ei + 4, ei + 7]))

                # increment of 8 edges
                ei += 8

                # append both triangles and quadrangles to have a same size
                cells_triangles.append(np.arange(tfi, tfi + n_Kfaces - 1))
                tfi += n_Kfaces - 1
                cells_quadrangles.append(np.asarray(qua_faces))
        # 1D and 2D cells not read
        elif (
            (vtktype == "vertex")
            | (vtktype == "line")
            | (vtktype == "triangle")
            | (vtktype == "quad")
        ):
            messages.warning(
                f"cell type {vtktype} not read in meshio2icmesh, only 3D cells are read"
            )
        else:
            messages.error(f"cell type {vtktype} not recognized in meshio2icmesh")

    # remove all duplicated objects (up to now, all cells have been initialized
    # as if it has no neighbours, so no commun objects)
    # remove duplicated edges, inverse is such that edges_nodes[inverse] = all_edges_nodes
    edges_nodes, inverse = np.unique(all_edges_nodes, axis=0, return_inverse=True)
    # modify the faces_edges connectivity with new edges numbering (use inverse)
    tmp_tri_edges = []  # new numbering of edges
    tmp_quadra_edges = []  # new numbering of edges
    for edges in all_triangles_edges:
        # sort them to compare them later
        tmp_tri_edges.append(np.sort(inverse[edges]))
        # test consistency
        for e, ae in zip(inverse[edges], edges):
            assert np.all(edges_nodes[e] == all_edges_nodes[ae])
    for edges in all_quadrangles_edges:
        # sort them to compare them later
        tmp_quadra_edges.append(np.sort(inverse[edges]))
        for e, ae in zip(inverse[edges], edges):
            assert np.all(edges_nodes[e] == all_edges_nodes[ae])
    # remove duplicated faces, new face numbering
    tri_edges, t_indices, t_inverse = np.unique(
        tmp_tri_edges, axis=0, return_index=True, return_inverse=True
    )
    quadra_edges, q_indices, q_inverse = np.unique(
        tmp_quadra_edges, axis=0, return_index=True, return_inverse=True
    )
    # remove the duplicated faces in all_quadrangles_nodes and all_triangles_nodes
    # do not change nodes indices, nor order of the nodes in each face
    tri_nodes = np.asarray(all_triangles_nodes)[t_indices]
    quadra_nodes = np.asarray(all_quadrangles_nodes)[q_indices]
    tmp_cells_quad = []
    tmp_cells_tri = []

    # attention: q_inverse starts from 0, means new quadra face numbering from 0.
    # increment of face numbering from max(t_inverse) for q_inverse when there are both t_inverse and q_inverse
    q_start = 0
    if len(t_inverse) > 0:
        q_start = max(t_inverse) + 1
    q_inverse += q_start

    # use the new faces numbering in cells_faces
    for faces in cells_triangles:
        if len(faces) == 0:
            tmp_cells_tri.append(np.array([], dtype=int))
        else:
            tmp_cells_tri.append(t_inverse[faces])
    for faces in cells_quadrangles:
        if len(faces) == 0:
            tmp_cells_quad.append(np.array([], dtype=int))
        else:
            tmp_cells_quad.append(q_inverse[faces])

    # concatenate: in the same cell, join all face type
    # if tmp_cells_tri empty or tmp_cells_quad empty
    # in case of mixed cells type, cell_face is not homogeneous in dimension 1 (axis = 1)
    # cells_faces = np.concatenate((tmp_cells_tri, tmp_cells_quad), axis=1)

    # create cells faces by appending all cell types
    cells_faces = []
    if not tmp_cells_tri:
        cells_faces = np.asarray(tmp_cells_quad)  # tmp_cells_tri empty
    elif not tmp_cells_quad:
        cells_faces = np.asarray(tmp_cells_tri)  # tmp_cells_quad empty
    else:
        if len(tmp_cells_tri) != len(tmp_cells_quad):
            messages.error(
                "mesh with two types cells, but different length of tmp_cells_tri and tmp_cells_quad"
            )
        cells_faces = [
            np.concatenate((cell_tri, cell_quad)).tolist()
            for cell_tri, cell_quad in zip(tmp_cells_tri, tmp_cells_quad)
        ]

    # not the same number of edges in each faces, idem in faces_nodes
    faces_edges = []
    faces_edges.extend(tri_edges.tolist())
    faces_edges.extend(quadra_edges.tolist())
    # bonus : we easily have faces_nodes, also give it to the connectivity
    faces_nodes = []
    faces_nodes.extend(tri_nodes.tolist())
    faces_nodes.extend(quadra_nodes.tolist())

    # get mesh data
    # cell data
    cells_data_dict = {}
    for data_name, data_dict in given_mesh.cell_data_dict.items():
        cells_data = []
        for data_celltype, data in data_dict.items():
            # "if" are necessary and in same order as previously to keep the cells order
            if data_celltype == "tet" or data_celltype == "tetra":
                cells_data.extend(data)
            elif data_celltype == "polyhedron8":
                cells_data.extend(data)
            elif data_celltype == "voxel":
                cells_data.extend(data)
            elif data_celltype == "hexahedron":
                cells_data.extend(data)
            elif data_celltype == "wedge":
                cells_data.extend(data)
            elif data_celltype == "pyramid":
                cells_data.extend(data)

        cells_data_dict[data_name] = np.array(cells_data)

    # create and fill the compass mesh
    n_vertices = len(given_mesh.points)
    n_edges = len(edges_nodes)
    n_faces = len(faces_edges)
    assert len(faces_nodes) == n_faces
    n_cells = len(cells_faces)

    # call different mesh type generators
    if (mesh_type == "tetra") | (mesh_type == "tet"):
        # tetra mesh connectivity implemented
        compass_mesh = mesh_tetra(n_vertices, n_edges, n_faces, n_cells)
    elif mesh_type == "hexahedron":
        # call hex connectivity
        compass_mesh = mesh_cuboid(n_vertices, n_edges, n_faces, n_cells)
    else:
        # call default connectivity
        compass_mesh = mesh(n_vertices, n_edges, n_faces, n_cells)

    # initialize the vertices coordinates
    for i, c in enumerate(given_mesh.points):
        compass_mesh.vertices[i] = np.asarray(c)
    # initialize the connectivities
    compass_mesh.set_edges_nodes(edges_nodes)
    en = compass_mesh.edges_nodes
    assert len(en.source()) == n_edges
    assert len(en.target()) == n_vertices
    compass_mesh.set_faces_edges(faces_edges)
    compass_mesh.set_faces_nodes(faces_nodes, True)  # preserve_targets_order
    # precise that the nodes in each face are sorted to respect the edges nodes
    compass_mesh.ordered_faces_nodes = True
    compass_mesh.set_cells_faces(cells_faces)
    compass_mesh.set_cells_nodes(cells_nodes, True)  # preserve_targets_order
    return compass_mesh, cells_data_dict


def meshio2icmesh(mesh_file, mesh_type="default"):
    if mesh_type == "default":
        return default_mesh(mesh_file)
    else:
        messages.error(
            f"Only default mesh implemented in meshio2compass, no {mesh_type}"
        )
