#pragma once

#include <compass_cxx_utils/combinations.h>
#include <concepts>
#include <cstdint>
#include <utility>

#include "Axis_info.h"

namespace icmesh {

template <int n, int dim> struct Blocked_axis {
  using Combinations = compass::utils::combinations<n, dim>;
  struct iterator {
    struct Sentinel {};
    constexpr static auto sentinel = Sentinel{};
    using Combination_iterator = Combinations::iterator;
    Axis_info<> info;
    Combination_iterator combination;
    iterator() { reset_info(); }
    iterator(const Combination_iterator &ci) : combination{ci} { reset_info(); }
    void reset_info() {
      static_assert(std::is_same_v<std::decay_t<decltype(*combination)>,
                                   std::array<int, n>>);
      if constexpr (n == 0) {
        assert(!combination.ok());
      } else {
        info.reset();
        for (auto &&axis : *combination) {
          info.block_axis(axis);
        }
      }
    }
    iterator &operator++() {
      ++combination;
      reset_info();
      return *this;
    }
    const auto &operator*() const { return info; }
    bool operator!=(const Sentinel &) const { return combination.ok(); }
  };
  static auto begin() { return iterator{Combinations::begin()}; }
  // will not be used
  static auto end() { return iterator::sentinel; }
};

template <int n, int dim> inline constexpr Blocked_axis<n, dim> blocked_axis;

} // namespace icmesh
