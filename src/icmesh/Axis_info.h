#pragma once

#include <bitset>
#include <utility>

namespace icmesh {

template <std::size_t ndim = 8> struct Axis_info {
  std::bitset<ndim> blocked_axis;
  operator std::size_t() const { return blocked_axis.to_ulong(); }
  Axis_info &reset() {
    blocked_axis.reset();
    return *this;
  }
  bool valid_axis_index(const std::integral auto i) const {
    return std::cmp_greater_equal(i, 0) && std::cmp_less(i, ndim);
  }
  bool is_axis_blocked(const std::integral auto i) const {
    assert(valid_axis_index(i));
    return blocked_axis.test(i);
  }
  bool is_axis_free(const std::integral auto i) const {
    return !is_axis_blocked(i);
  }
  Axis_info &block_axis(const std::integral auto i) {
    assert(valid_axis_index(i));
    blocked_axis.set(i);
    return *this;
  }
  Axis_info &free_axis(const std::integral auto i) {
    assert(valid_axis_index(i));
    blocked_axis.reset(i);
    return *this;
  }
  int blocked_dimensions() const { return blocked_axis.count(); }
};

} // namespace icmesh
