#pragma once

#include <tuple>
#include <utility>

#include <icus/Connectivity.h>

#include "Structured_connectivity.h"

namespace icmesh {

template <int... I> using Connectivity_size = std::integer_sequence<int, I...>;

template <int ts = -1>
using Mesh_connectivity = icus::Connectivity<icus::ISet, icus::ISet, ts>;

struct Undefined {};

template <typename T> struct is_undefined {
  static constexpr bool value = false;
};

template <> struct is_undefined<Undefined> {
  static constexpr bool value = true;
};

template <typename T>
inline constexpr bool is_undefined_v = is_undefined<T>::value;

template <std::size_t i, int t0, int... ts>
  requires(i <= sizeof...(ts))
constexpr int Get(std::integer_sequence<int, t0, ts...>) {
  if constexpr (i == 0)
    return t0;
  else
    return Get<i - 1>(std::integer_sequence<int, ts...>{});
}

// define the type of one row of the connectivity matrix
// row of Connectivity which can be structured or not
// so row of elements of different types
template <int... ts> struct CM_row {
  // no Connectivity on the diagonal (no connectivity node - node for example)
  // Mesh Connectivity outside of the diagonal
  using type = std::tuple<
      std::conditional_t<ts == 0, Undefined, Mesh_connectivity<ts>>...>;
  static constexpr std::size_t size = sizeof...(ts);
  template <std::size_t i>
    requires(i < sizeof...(ts))
  static constexpr int get() {
    return Get<i>(std::integer_sequence<int, ts...>{});
  }
};

// TODO: Connectivity edges_nodes always of size 2 ???
// default type of one row of the connectivity matrix
// row of Connectivity which can be structured or not
// so row of elements of different types
template <std::size_t i, std::size_t dim>
  requires(i <= dim)
struct default_CM_row {
  // "helper" transform a list of integers to something available at the
  // compilation
  template <int... ts> struct helper;
  template <int... ts>
    requires(sizeof...(ts) <= dim)
  struct helper<ts...> {
    // void Connectivity on the diagonal (no connectivity node - node for
    // example) unstructured Connectivity outside of the diagonal complex code
    // to have the dimension available at the compilation
    using type = std::conditional_t<
        sizeof...(ts) == i, typename helper<ts..., 0>::type,
        std::conditional_t<i == 1 && sizeof...(ts) == 0,
                           typename helper<ts..., 2>::type,
                           typename helper<ts..., -1>::type>>;
  };
  template <int... ts>
    requires(sizeof...(ts) == dim + 1)
  struct helper<ts...> {
    using type = CM_row<ts...>;
  };
  using type = helper<>::type;
};

// Conncectivity matrix for tetrahedral mesh
// 0 -1 -1 -1
// 2  0 -1 -1
// 3  3  0 -1
// 4  6  4  0
template <std::size_t i, std::size_t dim>
  requires(i <= dim)
struct CM_row_tetra {
  template <int... ts> struct helper;
  // diagonal
  template <int... ts>
    requires(sizeof...(ts) == i)
  struct helper<ts...> {
    using type = typename helper<ts..., 0>::type;
  };
  // upper matrix
  template <int... ts>
    requires(sizeof...(ts) > i && sizeof...(ts) <= dim)
  struct helper<ts...> {
    using type = typename helper<ts..., -1>::type;
  };
  // mat(2,1)
  template <int... ts>
    requires(i == 1 && sizeof...(ts) == 0)
  struct helper<ts...> {
    // edges_nodes always of size 2, wtih row 1 + 1 and col 0 + 1
    using type = typename helper<ts..., 2>::type;
  };
  // mat(3,1)
  template <int... ts>
    requires(i == 2 && sizeof...(ts) == 0)
  struct helper<ts...> {
    // faces_nodes of size 3 for tetrah with row i + 1 and col ts + 1
    using type = typename helper<ts..., 3>::type;
  };
  // mat(3,2)
  template <int... ts>
    requires(i == 2 && sizeof...(ts) == 1)
  struct helper<ts...> {
    // faces_edges of size 3 for tetrah with row i + 1 and col ts + 1
    using type = typename helper<ts..., 3>::type;
  };
  // mat(4,1)
  template <int... ts>
    requires(i == 3 && sizeof...(ts) == 0)
  struct helper<ts...> {
    // cells_nodes of size 4 for tetrah with row i + 1 and col ts + 1
    using type = typename helper<ts..., 4>::type;
  };
  // mat(4,2)
  template <int... ts>
    requires(i == 3 && sizeof...(ts) == 1)
  struct helper<ts...> {
    // cells_edges of size 6 for tetrah with row i + 1 and col ts + 1
    using type = typename helper<ts..., 6>::type;
  };
  // mat(4,3)
  template <int... ts>
    requires(i == 3 && sizeof...(ts) == 2)
  struct helper<ts...> {
    // cells_faces of size 4 for tetrah with row i + 1 and col ts + 1
    using type = typename helper<ts..., 4>::type;
  };
  template <int... ts>
    requires(sizeof...(ts) == dim + 1)
  struct helper<ts...> {
    using type = CM_row<ts...>;
  };
  using type = helper<>::type;
};

// One row of the Structured connectivity matrix (cartesian grid)
// row of Connectivity which can be structured or not
// so row of elements of different types
template <std::size_t i, std::size_t dim>
  requires(i <= dim)
struct Structured_CM_row {
  template <int... ts> struct helper;
  template <int... ts>
    requires(sizeof...(ts) < i)
  struct helper<ts...> {
    static constexpr auto n =
        Structured_connectivity<dim>::template connectivity_size<i, sizeof...(
                                                                        ts)>();
    using type = typename helper<ts..., n>::type;
  };
  template <int... ts>
    requires(sizeof...(ts) == i)
  struct helper<ts...> {
    using type = typename helper<ts..., 0>::type;
  };
  template <int... ts>
    requires(sizeof...(ts) > i && sizeof...(ts) <= dim)
  struct helper<ts...> {
    using type = typename helper<ts..., -1>::type;
  };
  template <int... ts>
    requires(sizeof...(ts) == dim + 1)
  struct helper<ts...> {
    using type = CM_row<ts...>;
  };
  using type = helper<>::type;
};

template <std::size_t i, std::size_t dim>
using structured_CM_row = typename Structured_CM_row<i, dim>::type;

template <int... ts> using connectivity_matrix_row = CM_row<ts...>;

template <int... ts>
using connectivity_matrix_row_t = typename connectivity_matrix_row<ts...>::type;

template <typename T> struct is_CM_row {
  static constexpr bool value = false;
};

template <int... ts> struct is_CM_row<CM_row<ts...>> {
  static constexpr bool value = true;
};

template <typename T> inline constexpr bool is_CM_row_v = is_CM_row<T>::value;

// Matrix with (possibly) different types at each element
// because Connectivity can be structured or not
template <typename... CMR>
  requires(
      sizeof...(CMR) > 1 &&
      std::conjunction_v<std::bool_constant<is_CM_row_v<CMR>>...> &&
      std::conjunction_v<std::bool_constant<sizeof...(CMR) == CMR::size>...>)
struct Connectivity_matrix {
  static constexpr std::size_t dimension = sizeof...(CMR) - 1;
  using tuple_type = std::tuple<typename CMR::type...>;
  tuple_type data;
  // get the connectivity with the indices of row and column as template
  template <std::size_t i, std::size_t j>
    requires((i != j) && (i < dimension + 1) && (j < dimension + 1))
  const auto &connectivity() const {
    return std::get<j>(std::get<i>(data));
  }
  // get the connectivity with the indices of row and column as template
  template <std::size_t i, std::size_t j>
    requires((i != j) && (i < dimension + 1) && (j < dimension + 1))
  auto &connectivity() {
    return std::get<j>(std::get<i>(data));
  }
  template <std::size_t i, std::size_t j = dimension, typename T = Undefined>
    requires(i <= dimension)
  bool is_consistent_row(const T &source = Undefined{}) const {
    if constexpr (j == 0) {
      return true;
    } else {
      if constexpr (i == j) {
        return is_consistent_row<i, j - 1>(source);
      } else {
        const auto &cij = connectivity<i, j>();
        if constexpr (is_undefined_v<std::decay_t<decltype(cij)>>) {
          return is_consistent_row<i, j - 1>(source);
        } else {
          if (!cij)
            return is_consistent_row<i, j - 1>(source);
          if constexpr (!is_undefined_v<std::decay_t<T>>) {
            if (cij.source() != source)
              return false;
          }
          return is_consistent_row<i, j - 1>(cij.source());
        }
      }
    }
  }
  template <std::size_t j, std::size_t i = dimension, typename T = Undefined>
    requires(j <= dimension)
  bool is_consistent_column(const T &target = Undefined{}) const {
    if constexpr (i == 0) {
      return true;
    } else {
      if constexpr (i == j) {
        return is_consistent_column<j, i - 1>(target);
      } else {
        const auto &cij = connectivity<i, j>();
        if constexpr (is_undefined_v<std::decay_t<decltype(cij)>>) {
          return is_consistent_column<j, i - 1>(target);
        } else {
          if (!cij)
            return is_consistent_column<j, i - 1>(target);
          if constexpr (!is_undefined_v<std::decay_t<T>>) {
            if (cij.target() != target)
              return false;
          }
          return is_consistent_column<j, i - 1>(cij.target());
        }
      }
    }
  }
  template <std::size_t i = dimension>
    requires(i <= dimension)
  bool is_consistent() const {
    bool ok = is_consistent_row<i>() && is_consistent_column<i>();
    if (not ok)
      return false;
    if constexpr (i == 0) {
      return true;
    } else {
      return is_consistent<i - 1>();
    }
  }
  // FIXME: add a method to check that source/targets are consistent with a
  // given set of elements
};

// Connectivity matrix for any mesh
template <std::size_t dim> struct Default_connectivity_matrix {
  template <typename Index_sequence> struct helper;
  template <std::size_t... I> struct helper<std::index_sequence<I...>> {
    using type = Connectivity_matrix<typename default_CM_row<I, dim>::type...>;
  };
  using type = helper<std::make_index_sequence<dim + 1>>::type;
};

template <std::size_t dim>
using default_connectivity_matrix = Default_connectivity_matrix<dim>::type;

// Connectivity matrix for tetrahadral mesh
template <std::size_t dim> struct Connectivity_matrix_tetra {
  template <typename Index_sequence> struct helper;
  template <std::size_t... I> struct helper<std::index_sequence<I...>> {
    using type = Connectivity_matrix<typename CM_row_tetra<I, dim>::type...>;
  };
  using type = helper<std::make_index_sequence<dim + 1>>::type;
};

template <std::size_t dim>
using connectivity_matrix_tetra = Connectivity_matrix_tetra<dim>::type;

// Connectivity matrix for cartesian grid
template <std::size_t dim> struct Structured_connectivity_matrix {
  template <typename Index_sequence> struct helper;
  template <std::size_t... I> struct helper<std::index_sequence<I...>> {
    using type = Connectivity_matrix<structured_CM_row<I, dim>...>;
  };
  using type = helper<std::make_index_sequence<dim + 1>>::type;
};

template <std::size_t dim>
using structured_connectivity_matrix =
    Structured_connectivity_matrix<dim>::type;

template <std::size_t i, typename T> constexpr auto get(const T &) {
  return T::template get<i>();
}

template <typename T> struct is_connectivity_matrix {
  static constexpr bool value = false;
};

template <typename... CMR>
struct is_connectivity_matrix<Connectivity_matrix<CMR...>> {
  static constexpr bool value = true;
};

template <typename T>
inline constexpr bool is_connectivity_matrix_v =
    is_connectivity_matrix<std::decay_t<T>>::value;

template <std::size_t i, std::size_t j, typename StCon, typename CMat>
  requires(is_structured_connectivity_v<StCon> &&
           is_connectivity_matrix_v<CMat> &&
           StCon::dimension == CMat::dimension && i <= StCon::dimension &&
           j <= StCon::dimension)
auto _fill(const StCon &sc, CMat &mat) {
  if constexpr (i == 0) {
    return;
  } else {
    // i>0
    if constexpr (j >= i) {
      _fill<i, i - 1>(sc, mat);
    } else {
      mat.template connectivity<i, j>() = sc.template connectivity<i, j>();
      if constexpr (j == 0) {
        _fill<i - 1, i - 1>(sc, mat);
      } else {
        _fill<i, j - 1>(sc, mat);
      }
    }
  }
}
template <typename StCon, typename CMat>
  requires(is_structured_connectivity_v<StCon> &&
           is_connectivity_matrix_v<CMat> &&
           StCon::dimension == CMat::dimension)
auto fill(const StCon &sc, CMat &mat) {
  constexpr auto n = StCon::dimension;
  _fill<n, n>(sc, mat);
}

template <typename StCon>
  requires(is_structured_connectivity_v<StCon>)
auto connectivity_matrix(const StCon &con) {
  structured_connectivity_matrix<StCon::dimension> mat;
  assert(mat.is_consistent());
  fill(con, mat);
  return mat;
}

} // namespace icmesh
