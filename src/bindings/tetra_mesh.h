#pragma once

#include <icmesh/Geomtraits.h>

#include "icmesh-bindings-typedefs.h"

namespace icmesh {
namespace bindings {

void bind_tetra_mesh_utils(nb::module_ &module) {

  // creates a triangele or tetrahedral mesh by giving
  // the number of elements in each mesh object (number of nodes, edges, faces,
  // cells) no 1D default mesh, use structured mesh
  module.def("mesh_tetra",
             // 2D mesh has 3 arguments
             // elements sizes, will create the coresponding ISets
             [](int nn, int ne, int nk) -> nb::object {
               return nb::cast(mesh_tetra(nn, ne, nk));
             });
  module.def("mesh_tetra",
             // 3D tetra mesh has 4 arguments
             // elements sizes, will create the coresponding ISets
             [](int nn, int ne, int nf, int nk) -> nb::object {
               return nb::cast(mesh_tetra(nn, ne, nf, nk));
             });
}
} // namespace bindings
} // namespace icmesh
