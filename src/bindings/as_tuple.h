#pragma once

#include <array>
#include <utility>

#include <nanobind/nanobind.h>

namespace icmesh {

namespace bindings {

template <typename> struct Tuple_factory;

template <std::size_t... i> struct Tuple_factory<std::index_sequence<i...>> {
  template <typename T>
  static constexpr auto apply(const std::array<T, sizeof...(i)> &a) {
    return nanobind::make_tuple(T{a[i]}...);
  }
};

template <typename T, std::size_t n>
constexpr auto as_tuple(const std::array<T, n> &a) {
  return Tuple_factory<std::make_index_sequence<n>>::apply(a);
}

} // namespace bindings

} // namespace icmesh
