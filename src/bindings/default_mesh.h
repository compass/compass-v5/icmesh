#pragma once

#include <icmesh/Geomtraits.h>

#include "icmesh-bindings-typedefs.h"

namespace icmesh {
namespace bindings {

void bind_default_utils(nb::module_ &module) {

  // creates default mesh by giving the number of elements in each
  // mesh object (number of nodes, edges, faces, cells)
  // no 1D default mesh, use structured mesh
  module.def("mesh",
             // 3D mesh has 4 elements
             // elements sizes, will create the coresponding ISets
             [](int nn, int ne, int nf, int nk) -> nb::object {
               return nb::cast(mesh(nn, ne, nf, nk));
             });
  module.def("mesh",
             // 2D mesh has 3 elements
             // elements sizes, will create the coresponding ISets
             [](int nn, int ne, int nk) -> nb::object {
               return nb::cast(mesh(nn, ne, nk));
             });
}
} // namespace bindings
} // namespace icmesh
