#include "cuboid_mesh.h"
#include "default_mesh.h"
#include "lattice.h"
#include "raw_mesh.h"
#include "tetra_mesh.h"
#include <icmesh/Connected_components.h>
#include <icmesh/Geomtraits.h>
#include <icmesh/Read_fvca6.h>
#include <nanobind/nanobind.h>

// Geomtraits is binded without the heritage of Raw_mesh
// Raw_mesh is not binded, all necessary methods are directly binded to
// Geomtraits

namespace {

namespace nb = nanobind;

template <typename Geom_t> void bind_geom(auto &cls) {
  cls.def(nb::init<const Geom_t &>());

  // bind Raw_mesh methods as Geomtraits methods (no heritage)
  icmesh::bindings::bind_commun_rmesh<Geom_t>(cls);

  // build the isobarycenters of an iset
  cls.def("isobarycenters", &Geom_t::isobarycenters);
  cls.def_prop_ro("cell_isobarycenters", &Geom_t::get_cell_isobarycenters);
  cls.def_prop_ro("face_isobarycenters", &Geom_t::get_face_isobarycenters);
  cls.def_prop_ro("edge_isobarycenters", &Geom_t::get_edge_isobarycenters);
  // build the centers-of-mass of an iset
  cls.def("centers_of_mass", &Geom_t::centers_of_mass);
  cls.def_prop_ro("cell_centers_of_mass", &Geom_t::get_cell_centers_of_mass);
  cls.def_prop_ro("face_centers_of_mass", &Geom_t::get_face_centers_of_mass);
  cls.def_prop_ro("edge_centers_of_mass", &Geom_t::get_edge_centers_of_mass);
  // build the measures of an iset
  cls.def("measures", &Geom_t::measures);
  cls.def_prop_ro("cell_measures", &Geom_t::get_cell_measures);
  cls.def_prop_ro("face_measures", &Geom_t::get_face_measures);
  cls.def_prop_ro("edge_measures", &Geom_t::get_edge_measures);
}

template <std::size_t dim> void bind_default_mesh(nb::module_ &module) {
  using Geom = icmesh::Geomtraits<
      icmesh::Raw_mesh<dim, icmesh::default_connectivity_matrix<dim>>>;
  auto cls_name_str =
      std::string("Mesh") + std::to_string(dim) + std::string("D");
  auto cls_name = cls_name_str.c_str();
  auto cls = nb::class_<Geom>(module, cls_name,
                              "Mesh with units functions over geometry");

  // bind commun Raw_mesh methods and Geomtraits methods
  bind_geom<Geom>(cls);
  // bind set_connectivities as set_edges_nodes...
  // done only for default mesh, not for cartesian mesh
  icmesh::bindings::bind_default_set_conn<Geom>(cls);
}

// bind mesh tetra, connectivity matrix is known
template <std::size_t dim>
  requires(dim > 1 && dim < 4)
void bind_tetra_mesh(nb::module_ &module) {
  using Geom = icmesh::Geomtraits<
      icmesh::Raw_mesh<dim, icmesh::connectivity_matrix_tetra<dim>>>;

  // rename different meshes
  std::string cls_name_str;
  if constexpr (dim == 2)
    cls_name_str = std::string("TriangleMesh");
  else
    cls_name_str = std::string("TetraMesh");

  const char *cls_name = cls_name_str.c_str();
  // auto cls_name_str = std::string("Mesh_tetra_") + std::to_string(dim);
  // auto cls_name = cls_name_str.c_str();
  auto cls = nb::class_<Geom>(module, cls_name,
                              "Tetra mesh with units functions over geometry");

  // bind commun Raw_mesh methods and Geomtraits methods
  bind_geom<Geom>(cls);
  // bind set_connectivities as set_edges_nodes...
  icmesh::bindings::bind_tetra_mesh_set_conn<Geom>(cls);
}

template <std::size_t dim>
  requires(dim > 0 && dim < 4)
void bind_struct_mesh(nb::module_ &module) {
  using Geom = icmesh::Geomtraits<
      icmesh::Raw_mesh<dim, icmesh::structured_connectivity_matrix<dim>>>;

  // rename different meshes
  std::string cls_name_str;
  if constexpr (dim == 1)
    cls_name_str = std::string("Mesh1D");
  else if constexpr (dim == 2)
    cls_name_str = std::string("QuadMesh");
  else
    cls_name_str = std::string("CuboidMesh");
  const char *cls_name = cls_name_str.c_str();
  auto cls = nb::class_<Geom>(module, cls_name,
                              "Cuboid mesh with units functions over geometry");

  // creates mesh elements python classes
  icmesh::bindings::elements_cls<Geom>(module, cls_name);
  // bind commun Raw_mesh methods and Geomtraits methods
  bind_geom<Geom>(cls);
  // bind methods of Raw_mesh specific to structured meshes
  icmesh::bindings::bind_struct_rmesh<Geom>(cls);
  // bind set_connectivities as set_edges_nodes...
  icmesh::bindings::bind_struct_mesh_set_conn<Geom>(cls);
}

struct Mesh_factory {
  nb::object operator()(const std::size_t dim, auto &&offset) const {
    switch (dim) {
    case 1:
      return nb::cast(icmesh::mesh(icmesh::lattice(offset(0))));
    case 2:
      return nb::cast(icmesh::mesh(icmesh::lattice(offset(0), offset(1))));
    case 3:
      return nb::cast(
          icmesh::mesh(icmesh::lattice(offset(0), offset(1), offset(2))));
    default:
      throw std::runtime_error("unhandled dimension");
    }
  }
};

// bind mesh tetra, connectivity matrix is known
template <typename Mesh_t>
void bind_connected_components(nb::module_ &module, auto &cls_name) {
  using Connected_comp = icmesh::Connected_components<Mesh_t>;
  auto cls = nb::class_<Connected_comp>(
      module, cls_name,
      "Decompose into connected components wrt a set of faces");
  cls.def(nb::init<Mesh_t &, icus::ISet>());
  cls.def_ro("original_mesh", &Connected_comp::original_mesh);
  cls.def_ro("mesh", &Connected_comp::mesh);
  cls.def_ro("gamma", &Connected_comp::gamma);
  cls.def_ro("same_location", &Connected_comp::same_location);
}
} // namespace

void bind_mesh(nb::module_ &module) {
  // dependency on icus to handle index sets and connectivities from python
  nb::module_::import_("icus");

  icmesh::bindings::bind_vertex<1>(module);
  icmesh::bindings::bind_vertex<2>(module);
  icmesh::bindings::bind_vertex<3>(module);

  // bind structured (cartesian, scottish...) and cuboid meshes
  bind_struct_mesh<1>(module);
  bind_struct_mesh<2>(module);
  bind_struct_mesh<3>(module);

  // utilities to construct struct meshes
  icmesh::bindings::bind_regular(module, "mesh", Mesh_factory{});
  icmesh::bindings::bind_scottish(module, "mesh", Mesh_factory{});

  // bind default meshes
  // no 1D default mesh, same type as structured mesh
  // always 2 nodes per edge
  bind_default_mesh<2>(module);
  bind_default_mesh<3>(module);

  // bind tetra mesh
  bind_tetra_mesh<2>(module);
  bind_tetra_mesh<3>(module);

  // utilities to construct default, tetra and cuboid meshes
  icmesh::bindings::bind_default_utils(module);
  icmesh::bindings::bind_tetra_mesh_utils(module);
  icmesh::bindings::bind_cuboid_mesh_utils(module);

  // fvca6 is a format of the FVCA6 conference containing many connectivities
  module.def("read_fvca6", &icmesh::read_fvca6);
  module.def("read_fvca6", [](const char *name) {
    return icmesh::read_fvca6(std::string{name});
  });

  // bind the structure which decomposes the mesh into connected components
  // with respect to a set of faces gamma
  using Geom = icmesh::Geomtraits<
      icmesh::Raw_mesh<3, icmesh::structured_connectivity_matrix<3>>>;
  bind_connected_components<Geom>(module, "Connected_components");
}
