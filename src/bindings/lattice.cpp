#include "lattice.h"
#include "as_tuple.h"

namespace {

template <typename... Axis>
void bind_lattice_class(nb::module_ &module, const char *name) {

  using Lattice = icmesh::Lattice<Axis...>;

  auto cls = nb::class_<Lattice>(module, name, "A Lattice class");
  cls.def_prop_ro("elements_shape", [](const Lattice &self) {
    return icmesh::bindings::as_tuple(self.elements_shape());
  });
  cls.def_prop_ro("points_shape", [](const Lattice &self) {
    return icmesh::bindings::as_tuple(self.points_shape());
  });
  cls.def_prop_ro("origin", [](const Lattice &self) {
    return icmesh::bindings::as_tuple(self.origin());
  });
  cls.def_prop_ro("extent", [](const Lattice &self) {
    return icmesh::bindings::as_tuple(self.extent());
  });
}

struct Lattice_factory {
  nb::object operator()(const std::size_t dim, auto &&offset) const {
    switch (dim) {
    case 1:
      return nb::cast(lattice(offset(0)));
    case 2:
      return nb::cast(lattice(offset(0), offset(1)));
    case 3:
      return nb::cast(lattice(offset(0), offset(1), offset(2)));
    default:
      throw std::runtime_error("unhandled dimension");
    }
  }
};

} // namespace

void bind_lattice(nb::module_ &module) {

  using co = icmesh::Constant_axis_offset<double>;
  using so = icmesh::Specific_axis_offset<double>;

  bind_lattice_class<co>(module, "Regular_lattice_1");
  bind_lattice_class<co, co>(module, "Regular_lattice_2");
  bind_lattice_class<co, co, co>(module, "Regular_lattice_3");
  bind_lattice_class<so>(module, "Structured_lattice_1");
  bind_lattice_class<so, so>(module, "Structured_lattice_2");
  bind_lattice_class<so, so, so>(module, "Structured_lattice_3");
  bind_lattice_class<co, so>(module, "Hybrid_lattice_coxso");
  bind_lattice_class<so, co>(module, "Hybrid_lattice_soxco");
  bind_lattice_class<co, co, so>(module, "Hybrid_lattice_coxcoxso");
  bind_lattice_class<so, co, so>(module, "Hybrid_lattice_soxcoxso");
  bind_lattice_class<co, so, co>(module, "Hybrid_lattice_coxsoxco");
  bind_lattice_class<so, so, co>(module, "Hybrid_lattice_soxsoxco");

  icmesh::bindings::bind_regular(module, "lattice", Lattice_factory{});
  icmesh::bindings::bind_scottish(module, "lattice", Lattice_factory{});
}
