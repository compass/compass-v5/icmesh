#pragma once

#include <icmesh/Geomtraits.h>

#include "icmesh-bindings-typedefs.h"

namespace icmesh {
namespace bindings {

void bind_cuboid_mesh_utils(nb::module_ &module) {

  // creates a quadrangle or cuboid mesh by giving
  // the number of elements in each mesh object (number of nodes, edges, faces,
  // cells)
  module.def("mesh_cuboid",
             // 2D quad mesh has 3 arguments
             // elements sizes, will create the coresponding ISets
             [](int nn, int ne, int nk) -> nb::object {
               return nb::cast(mesh_cuboid(nn, ne, nk));
             });
  module.def("mesh_cuboid",
             // 3D cuboid mesh has 4 arguments
             // elements sizes, will create the coresponding ISets
             [](int nn, int ne, int nf, int nk) -> nb::object {
               return nb::cast(mesh_cuboid(nn, ne, nf, nk));
             });
}
} // namespace bindings
} // namespace icmesh
