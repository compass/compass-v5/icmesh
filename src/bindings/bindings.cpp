#include <nanobind/nanobind.h>

namespace nb = nanobind;

void bind_lattice(nb::module_ &);
void bind_mesh(nb::module_ &);

NB_MODULE(bindings, module) {
  bind_lattice(module);
  bind_mesh(module);
}
