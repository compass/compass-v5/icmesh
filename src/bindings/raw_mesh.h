#include "as_tuple.h"
#include "icmesh-bindings-typedefs.h"
#include "lattice.h"
#include <icmesh/Raw_mesh.h>
#include <icus/binding/Connectivity.h>
#include <icus/field-binding.h>

namespace icmesh {
namespace bindings {

namespace ib = icus::binding;

template <std::size_t dim, typename Vertex = std::array<scalar, dim>>
void bind_vertex(nb::module_ &module) {

  auto classname = std::string("Vertex_") + std::to_string(dim);
  auto pyVertex = nb::class_<Vertex>(module, classname.c_str());
  pyVertex.def(nb::init<>());
  pyVertex.def(
      "__init__",
      [](Vertex *self,
         nb::ndarray<typename Vertex::value_type, nb::shape<dim>, nb::c_contig>
             v) {
        new (self) Vertex{};
        std::copy(v.data(), v.data() + dim, self->data());
      });
  pyVertex.def(
      "as_array", // could be any name that you find explicit
                  // this binding is NOT mandatory for field exposition
                  // as packs to work (cf. below)
      [](nb::handle self) {
        return nb::ndarray<scalar, nb::numpy>{
            nb::cast<Vertex &>(self).data(), {dim}, self};
      },
      nb::keep_alive<0, 1>());
  auto fieldname = std::string("Vertices_") + std::to_string(dim);
  icus::bind_field(module, pyVertex, fieldname.c_str(),
                   icus::dl::Pack<double>{{dim}});
}

template <typename mesh_t>
void elements_cls(nb::module_ &module, auto &class_name) {
  static constexpr std::size_t dim = mesh_t::dimension;
  // bind types, they are the same for structured and default meshes
  auto cls_name = std::string(class_name);
  nb::class_<typename mesh_t::template Element_iset<mesh_t::node>, icus::ISet>(
      module, (cls_name + "_nodes").c_str());
  nb::class_<typename mesh_t::template Element_iset<mesh_t::edge>, icus::ISet>(
      module, (cls_name + "_edges").c_str());
  if constexpr (dim > 1) {
    nb::class_<typename mesh_t::template Element_iset<mesh_t::cell>,
               icus::ISet>(module, (cls_name + "_cells").c_str());
    if constexpr (dim > 2) {
      nb::class_<typename mesh_t::template Element_iset<mesh_t::face>,
                 icus::ISet>(module, (cls_name + "_faces").c_str());
    }
  }
}

// bind regular connectivity: set_edges_nodes for example, where targets can be
// a list or ndarray
template <typename mesh_t, std::size_t source_i, std::size_t target_i, int ts>
  requires(ts > 0)
void bind_set_connectivity(auto &cls, auto &prop_name) {
  // from list
  cls.def(
      prop_name,
      [](mesh_t &self, nb::list &targets, const bool preserve_targets_order,
         const bool sorted_input_targets) {
        auto conn = ib::_reg_connectivity_from_list<ts>(
            self.elements[source_i], self.elements[target_i], targets,
            preserve_targets_order, sorted_input_targets);
        self.template set_connectivity<source_i, target_i>(conn);
      },
      nb::arg("targets"), nb::arg("preserve_targets_order") = false,
      nb::arg("sorted_input_targets") = false);
  // from ndarray
  cls.def(
      prop_name,
      [](mesh_t &self, nb::ndarray<icus::index_type, nb::c_contig> &targets,
         const bool preserve_targets_order, const bool sorted_input_targets) {
        auto source = self.elements[source_i];
        if (targets.ndim() != 2) {
          throw std::runtime_error("Array must have two dimensions!");
        }
        if (targets.shape(0) != source.size()) {
          throw std::runtime_error(
              "The number of targets must match source size!");
        }
        assert(targets.shape(1) == ts);
        auto conn = ib::Regular_connectivity<ts>{
            source, self.elements[target_i],
            std::span{targets.data(), targets.size()}, preserve_targets_order,
            sorted_input_targets};
        self.template set_connectivity<source_i, target_i>(conn);
      },
      nb::arg("targets"), nb::arg("preserve_targets_order") = false,
      nb::arg("sorted_input_targets") = false);
}

// bind non regular connectivity: set_nodes_edges for example,
// where targets can be a list or a ndarray
template <typename mesh_t, std::size_t source_i, std::size_t target_i>
void bind_set_connectivity(auto &cls, auto &prop_name) {
  // from list
  cls.def(
      prop_name,
      [](mesh_t &self, nb::list &targets, const bool preserve_targets_order,
         const bool sorted_input_targets) {
        auto conn = icus::binding::_rand_connectivity_from_list(
            self.elements[source_i], self.elements[target_i], targets,
            preserve_targets_order, sorted_input_targets);
        self.template set_connectivity<source_i, target_i>(conn);
      },
      nb::arg("targets"), nb::arg("preserve_targets_order") = false,
      nb::arg("sorted_input_targets") = false);
  // from ndarray (even if the connectivity is not stored as regular)
  cls.def(
      prop_name,
      [](mesh_t &self, nb::ndarray<icus::index_type, nb::c_contig> &targets,
         const bool preserve_targets_order, const bool sorted_input_targets) {
        auto source = self.elements[source_i];
        if (targets.ndim() != 2) {
          throw std::runtime_error("Array must have two dimensions!");
        }
        if (targets.shape(0) != source.size()) {
          throw std::runtime_error(
              "The number of targets must match source size!");
        }
        auto conn = ib::Random_connectivity{
            source, self.elements[target_i],
            std::span{targets.data(), targets.size()} |
                std::views::chunk(targets.shape(1)),
            preserve_targets_order, sorted_input_targets};
        self.template set_connectivity<source_i, target_i>(conn);
      },
      nb::arg("targets"), nb::arg("preserve_targets_order") = false,
      nb::arg("sorted_input_targets") = false);
}

template <typename mesh_t> void bind_default_set_conn(auto &cls) {
  bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::node, 2>(
      cls, "set_edges_nodes");
  bind_set_connectivity<mesh_t, mesh_t::node, mesh_t::edge>(cls,
                                                            "set_nodes_edges");
  bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::node>(cls,
                                                            "set_cells_nodes");
  bind_set_connectivity<mesh_t, mesh_t::node, mesh_t::cell>(cls,
                                                            "set_nodes_cells");

  if constexpr (mesh_t::edge != mesh_t::face) {
    bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::edge>(
        cls, "set_faces_edges");
    bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::face>(
        cls, "set_edges_faces");
  }
  if constexpr (mesh_t::edge != mesh_t::cell) {
    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::edge>(
        cls, "set_cells_edges");
    bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::cell>(
        cls, "set_edges_cells");
  }
  if constexpr (mesh_t::face != mesh_t::cell) {
    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::face>(
        cls, "set_cells_faces");
    bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::cell>(
        cls, "set_faces_cells");
  }
  if constexpr (mesh_t::node != mesh_t::face) {
    if constexpr (mesh_t::face == mesh_t::edge) {
      // in 2D, face=edge and 2 nodes by edge
      bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::node, 2>(
          cls, "set_faces_nodes");
    } else {
      bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::node>(
          cls, "set_faces_nodes");
    }
    bind_set_connectivity<mesh_t, mesh_t::node, mesh_t::face>(
        cls, "set_nodes_faces");
  }
}

// set connectivity for tetra
template <typename mesh_t> void bind_tetra_mesh_set_conn(auto &cls) {

  // no 1D tetra mesh bind, 1D mesh is done in cuboid (structured) bind

  // lower triangular matrix
  // commun connectivity for 2D and 3D meshes
  bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::node, 2>(
      cls, "set_edges_nodes");

  if constexpr (mesh_t::edge != mesh_t::face) {
    // connectivity for 3D mesh
    bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::node, 3>(
        cls, "set_faces_nodes");

    bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::edge, 3>(
        cls, "set_faces_edges");
    bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::face>(
        cls, "set_edges_faces");

    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::node, 4>(
        cls, "set_cells_nodes");
    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::edge, 6>(
        cls, "set_cells_edges");
    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::face, 4>(
        cls, "set_cells_faces");
  } else {
    // connectivity for 2D mesh
    bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::node, 2>(
        cls, "set_faces_nodes"); // same as edges_nodes

    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::node, 3>(
        cls, "set_cells_nodes");
    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::edge, 3>(
        cls, "set_cells_edges");
    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::face, 3>(
        cls, "set_cells_faces");
  }

  // upper triangular matrix as default
  bind_set_connectivity<mesh_t, mesh_t::node, mesh_t::edge>(cls,
                                                            "set_nodes_edges");
  bind_set_connectivity<mesh_t, mesh_t::node, mesh_t::face>(cls,
                                                            "set_nodes_faces");
  bind_set_connectivity<mesh_t, mesh_t::node, mesh_t::cell>(cls,
                                                            "set_nodes_cells");

  bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::cell>(cls,
                                                            "set_edges_cells");

  bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::cell>(cls,
                                                            "set_faces_cells");
}

// set connectivity for struct mesh
template <typename mesh_t> void bind_struct_mesh_set_conn(auto &cls) {

  // lower triangular matrix
  // commun connectivity for all meshes (1D, 2D, or 3D)
  bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::node, 2>(
      cls, "set_edges_nodes");
  bind_set_connectivity<mesh_t, mesh_t::node, mesh_t::edge>(cls,
                                                            "set_nodes_edges");

  // connectivity for 1D mesh, face == node and cell == edge
  if constexpr (mesh_t::node != mesh_t::face) {
    // 2D or 3D meshes
    if constexpr (mesh_t::edge != mesh_t::face) {
      // 3D mesh
      bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::node, 4>(
          cls, "set_faces_nodes");

      bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::edge, 4>(
          cls, "set_faces_edges");
      bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::face>(
          cls, "set_edges_faces");

      bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::node, 8>(
          cls, "set_cells_nodes");
      bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::edge, 12>(
          cls, "set_cells_edges");

      bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::face, 6>(
          cls, "set_cells_faces");
    } else {
      // 2D mesh
      bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::node, 2>(
          cls, "set_faces_nodes");

      bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::node, 4>(
          cls, "set_cells_nodes");

      bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::edge, 4>(
          cls, "set_cells_edges");

      bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::face, 4>(
          cls, "set_cells_faces");
    }
    // commun connectivity for 2D and 3D meshes, same as default
    bind_set_connectivity<mesh_t, mesh_t::node, mesh_t::face>(
        cls, "set_nodes_faces");
    bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::cell>(
        cls, "set_edges_cells");
    bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::cell>(
        cls, "set_faces_cells");
  } else {
    // 1D mesh
    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::node, 2>(
        cls, "set_cells_nodes");
    bind_set_connectivity<mesh_t, mesh_t::cell, mesh_t::face, 2>(
        cls, "set_cells_faces");
    bind_set_connectivity<mesh_t, mesh_t::edge, mesh_t::face, 2>(
        cls, "set_edges_faces");
    bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::edge>(
        cls, "set_faces_edges");
  }
  // default connectivity for all meshes
  bind_set_connectivity<mesh_t, mesh_t::node, mesh_t::cell>(cls,
                                                            "set_nodes_cells");
  bind_set_connectivity<mesh_t, mesh_t::face, mesh_t::cell>(cls,
                                                            "set_faces_cells");
}

template <typename mesh_t> void bind_commun_rmesh(auto &cls) {
  static constexpr std::size_t dim = mesh_t::dimension;

  cls.def_ro_static("dimension", &mesh_t::dimension);

  cls.def_static("to_vertex",
                 [](nb::ndarray<typename mesh_t::vertex_type::value_type,
                                nb::shape<dim>, nb::c_contig>
                        v) {
                   auto vertex = typename mesh_t::vertex_type{};
                   std::copy(v.data(), v.data() + dim, vertex.data());
                   return vertex;
                 });

  cls.def_ro("joined_elements", &mesh_t::joined_elements);
  cls.def_ro("nodes", &mesh_t::nodes);
  cls.def_ro("edges", &mesh_t::edges);
  cls.def_ro("faces", &mesh_t::faces);
  cls.def_ro("cells", &mesh_t::cells);

  cls.def_prop_ro("elements", [](const mesh_t &self) {
    nb::dict elements;
    elements["nodes"] = nb::cast(self.nodes);
    elements["edges"] = nb::cast(self.edges);
    elements["faces"] = nb::cast(self.faces);
    elements["cells"] = nb::cast(self.cells);
    return elements;
  });

  cls.def_ro("vertices", &mesh_t::vertices);

  // connectivities info
  cls.def_rw("ordered_faces_nodes", &mesh_t::ordered_faces_nodes);
  cls.def_prop_ro("edges_nodes", [](mesh_t &self) -> ib::Connectivity {
    return self.template get_connectivity<mesh_t::edge, mesh_t::node>();
  });
  cls.def_prop_ro("nodes_edges", [](mesh_t &self) -> ib::Connectivity {
    return self.template get_connectivity<mesh_t::node, mesh_t::edge>();
  });
  cls.def_prop_ro("cells_nodes", [](mesh_t &self) -> ib::Connectivity {
    return self.template get_connectivity<mesh_t::cell, mesh_t::node>();
  });
  cls.def_prop_ro("nodes_cells", [](mesh_t &self) -> ib::Connectivity {
    return self.template get_connectivity<mesh_t::node, mesh_t::cell>();
  });
  if constexpr (mesh_t::edge != mesh_t::face) {
    cls.def_prop_ro("faces_edges", [](mesh_t &self) -> ib::Connectivity {
      return self.template get_connectivity<mesh_t::face, mesh_t::edge>();
    });
    cls.def_prop_ro("edges_faces", [](mesh_t &self) -> ib::Connectivity {
      return self.template get_connectivity<mesh_t::edge, mesh_t::face>();
    });
  }
  if constexpr (mesh_t::edge != mesh_t::cell) {
    cls.def_prop_ro("cells_edges", [](mesh_t &self) -> ib::Connectivity {
      return self.template get_connectivity<mesh_t::cell, mesh_t::edge>();
    });
    cls.def_prop_ro("edges_cells", [](mesh_t &self) -> ib::Connectivity {
      return self.template get_connectivity<mesh_t::edge, mesh_t::cell>();
    });
  }
  if constexpr (mesh_t::face != mesh_t::cell) {
    cls.def_prop_ro("faces_cells", [](mesh_t &self) -> ib::Connectivity {
      return self.template get_connectivity<mesh_t::face, mesh_t::cell>();
    });
    cls.def_prop_ro("cells_faces", [](mesh_t &self) -> ib::Connectivity {
      return self.template get_connectivity<mesh_t::cell, mesh_t::face>();
    });
  }
  if constexpr (mesh_t::node != mesh_t::face) {
    cls.def_prop_ro("faces_nodes", [](mesh_t &self) -> ib::Connectivity {
      return self.template get_connectivity<mesh_t::face, mesh_t::node>();
    });
    cls.def_prop_ro("nodes_faces", [](mesh_t &self) -> ib::Connectivity {
      return self.template get_connectivity<mesh_t::node, mesh_t::face>();
    });
  }
}

// methods of Raw_mesh specific to structured meshes
template <typename mesh_t> void bind_struct_rmesh(auto &cls) {
  // bind structured elements
  cls.def_prop_ro("elements_shape", [](const mesh_t &self) {
    assert(self.shape);
    return as_tuple(*self.shape);
  });
  cls.def_prop_ro("points_shape", [](const mesh_t &self) {
    assert(self.shape);
    auto shape = *self.shape;
    for (auto &&n : shape)
      ++n;
    return as_tuple(shape);
  });
  cls.def_prop_ro("extent", [](const mesh_t &self) {
    assert(self.bounding_box);
    using namespace compass::utils::array_operators;
    return as_tuple(self.bounding_box->second - self.bounding_box->first);
  });
  cls.def_prop_ro("origin", [](const mesh_t &self) {
    assert(self.bounding_box);
    return as_tuple(self.bounding_box->first);
  });
  cls.def_prop_ro("bbox", [](const mesh_t &self) {
    assert(self.bounding_box);
    return nb::make_tuple(as_tuple(self.bounding_box->first),
                          as_tuple(self.bounding_box->second));
  });
}

} // namespace bindings
} // namespace icmesh
