#pragma once

#include <span>
#include <string>

#include <nanobind/ndarray.h>

#include <icmesh/Lattice.h>

#include "icmesh-bindings-typedefs.h"

namespace icmesh {
namespace bindings {

template <typename Factory>
void bind_regular(nb::module_ &module, std::string name, Factory &&make) {

  name = std::string("regular_") + name;

  module.def(
      name.c_str(),
      [make](nb::object shape, nb::object extent, nb::object steps,
             nb::object origin) -> nb::object {
        const std::size_t dim = nb::len(shape);
        std::vector<std::size_t> nb_elements(dim, 0);
        for (std::size_t i = 0; i < dim; ++i) {
          if (nb::cast<int>(shape[i]) <= 0)
            throw std::runtime_error(
                std::string("inconsitent shape along axis ") +
                std::to_string(i));
          nb_elements[i] = nb::cast<std::size_t>(shape[i]);
        }
        std::vector<scalar> O(dim, 0.);
        if (!origin.is_none()) {
          if (nb::len(origin) != dim)
            throw std::runtime_error(
                "shape and origin have unconsistent dimensions");
          for (std::size_t i = 0; i < dim; ++i) {
            O[i] = nb::cast<scalar>(origin[i]);
          }
        }
        std::vector<scalar> dx(dim, 1.);
        if (!extent.is_none()) {
          if (!steps.is_none())
            throw std::runtime_error(
                "extent and steps cannot be defined simultaneously");
          if (nb::len(extent) != dim)
            throw std::runtime_error(
                "shape and extent have unconsistent dimensions");
          for (std::size_t i = 0; i < dim; ++i) {
            dx[i] = nb::cast<scalar>(extent[i]) / nb_elements[i];
          }
        } else {
          if (!steps.is_none()) {
            if (nb::len(steps) != dim)
              throw std::runtime_error(
                  "shape and steps have unconsistent dimensions");
            for (std::size_t i = 0; i < dim; ++i) {
              dx[i] = nb::cast<scalar>(steps[i]);
              if (dx[i] <= 0)
                throw std::runtime_error(
                    std::string("inconsitent step along axis ") +
                    std::to_string(i));
            }
          }
        }
        auto offset = [&](int i) {
          return icmesh::Constant_axis_offset<double>{O[i], nb_elements[i],
                                                      dx[i]};
        };
        return make(dim, offset);
      },
      nb::arg("shape"), nb::arg("extent") = nb::none(),
      nb::arg("steps") = nb::none(), nb::arg("origin") = nb::none());
}

template <typename Factory>
void bind_scottish(nb::module_ &module, std::string name, Factory &&make) {

  name = std::string("scottish_") + name;

  module.def(
      name.c_str(), [make](nb::args args, nb::kwargs kwargs) -> nb::object {
        using scalar_array = nb::ndarray<scalar, nb::c_contig>;
        std::vector<scalar_array> axis;
        for (auto &&arg : args) {
          try {
            axis.emplace_back(nb::cast<scalar_array>(arg));
          } catch (nb::cast_error &) {
            throw std::runtime_error(
                "could not convert an argument to array of scalars");
          }
        }
        const std::size_t dim = axis.size();
        std::vector<scalar> O(dim, 0.);
        for (auto &&[key, value] : kwargs) {
          auto name = nb::cast<nb::str>(key).c_str();
          if (strcmp(name, "origin") == 0) {
            if (nb::len(value) != dim)
              throw std::runtime_error("origin have unconsistent dimensions");
            for (std::size_t i = 0; i < dim; ++i) {
              O[i] = nb::cast<scalar>(value[i]);
            }
          } else {
            nb::print((std::string("WARNING: Unhandled ") + name +
                       std::string("argument."))
                          .c_str());
          }
        }
        auto offset = [&](int i) {
          return icmesh::Specific_axis_offset<double>{
              O[i], std::span<scalar>(axis[i].data(), axis[i].shape(0))};
        };
        return make(dim, offset);
      });
}

} // namespace bindings
} // namespace icmesh
