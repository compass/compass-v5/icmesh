#include <icmesh/Raw_mesh.h>

#include <icus/binding/Connectivity.h>
#include <icus/field-binding.h>

#include "as_tuple.h"
#include "icmesh-bindings-typedefs.h"
#include "lattice.h"

namespace {

template <std::size_t dim, typename Vertex = std::array<scalar, dim>>
void bind_structured_mesh(nb::module_ &module) {

  auto classname = std::string("Vertex_") + std::to_string(dim);
  auto pyVertex = nb::class_<Vertex>(module, classname.c_str());
  pyVertex.def(nb::init<>());
  pyVertex.def(
      "__init__",
      [](Vertex *self,
         nb::ndarray<typename Vertex::value_type, nb::shape<dim>, nb::c_contig>
             v) {
        new (self) Vertex{};
        std::copy(v.data(), v.data() + dim, self->data());
      });
  pyVertex.def(
      "as_array", // could be any name that you find explicit
                  // this binding is NOT mandatory for field exposition
                  // as packs to work (cf. below)
      [](nb::handle self) {
        return nb::ndarray<scalar, nb::numpy>{
            nb::cast<Vertex &>(self).data(), {dim}, self};
      },
      nb::keep_alive<0, 1>());
  auto fieldname = std::string("Vertices_") + std::to_string(dim);
  icus::bind_field(module, pyVertex, fieldname.c_str(),
                   icus::dl::Pack<double>{{dim}});

  auto name = std::string("Structured_raw_mesh_") + std::to_string(dim);
  using Mesh =
      icmesh::Raw_mesh<dim, icmesh::structured_connectivity_matrix<dim>>;

  auto cls = nb::class_<Mesh>(module, name.c_str());

  static_assert(dim >= 1 and dim <= 3);
  cls.def_ro_static("dimension", &Mesh::dimension);

  cls.def_static("to_vertex",
                 [](nb::ndarray<typename Mesh::vertex_type::value_type,
                                nb::shape<dim>, nb::c_contig>
                        v) {
                   auto vertex = typename Mesh::vertex_type{};
                   std::copy(v.data(), v.data() + dim, vertex.data());
                   return vertex;
                 });

  nb::class_<typename Mesh::template Element_iset<Mesh::node>, icus::ISet>(
      module, (name + "_nodes").c_str());
  nb::class_<typename Mesh::template Element_iset<Mesh::edge>, icus::ISet>(
      module, (name + "_edges").c_str());
  if constexpr (dim > 1) {
    nb::class_<typename Mesh::template Element_iset<Mesh::cell>, icus::ISet>(
        module, (name + "_cells").c_str());
    if constexpr (dim > 2) {
      nb::class_<typename Mesh::template Element_iset<Mesh::face>, icus::ISet>(
          module, (name + "_faces").c_str());
    }
  }

  cls.def_ro("joined_elements", &Mesh::joined_elements);
  cls.def_ro("nodes", &Mesh::nodes);
  cls.def_ro("edges", &Mesh::edges);
  cls.def_ro("faces", &Mesh::faces);
  cls.def_ro("cells", &Mesh::cells);

  cls.def_prop_ro("edges_nodes", [](Mesh &self) -> icus::binding::Connectivity {
    return self.template get_connectivity<Mesh::edge, Mesh::node>();
  });
  cls.def_prop_ro("nodes_edges", [](Mesh &self) -> icus::binding::Connectivity {
    return self.template get_connectivity<Mesh::node, Mesh::edge>();
  });
  cls.def_prop_ro("cells_nodes", [](Mesh &self) -> icus::binding::Connectivity {
    return self.template get_connectivity<Mesh::cell, Mesh::node>();
  });
  cls.def_prop_ro("nodes_cells", [](Mesh &self) -> icus::binding::Connectivity {
    return self.template get_connectivity<Mesh::node, Mesh::cell>();
  });
  if constexpr (Mesh::edge != Mesh::face) {
    cls.def_prop_ro(
        "faces_edges", [](Mesh &self) -> icus::binding::Connectivity {
          return self.template get_connectivity<Mesh::face, Mesh::edge>();
        });
    cls.def_prop_ro(
        "edges_faces", [](Mesh &self) -> icus::binding::Connectivity {
          return self.template get_connectivity<Mesh::edge, Mesh::face>();
        });
  }
  if constexpr (Mesh::edge != Mesh::cell) {
    cls.def_prop_ro(
        "cells_edges", [](Mesh &self) -> icus::binding::Connectivity {
          return self.template get_connectivity<Mesh::cell, Mesh::edge>();
        });
    cls.def_prop_ro(
        "edges_cells", [](Mesh &self) -> icus::binding::Connectivity {
          return self.template get_connectivity<Mesh::edge, Mesh::cell>();
        });
  }
  if constexpr (Mesh::face != Mesh::cell) {
    cls.def_prop_ro(
        "faces_cells", [](Mesh &self) -> icus::binding::Connectivity {
          return self.template get_connectivity<Mesh::face, Mesh::cell>();
        });
    cls.def_prop_ro(
        "cells_faces", [](Mesh &self) -> icus::binding::Connectivity {
          return self.template get_connectivity<Mesh::cell, Mesh::face>();
        });
  }
  if constexpr (Mesh::node != Mesh::face) {
    cls.def_prop_ro(
        "faces_nodes", [](Mesh &self) -> icus::binding::Connectivity {
          return self.template get_connectivity<Mesh::face, Mesh::node>();
        });
    cls.def_prop_ro(
        "nodes_faces", [](Mesh &self) -> icus::binding::Connectivity {
          return self.template get_connectivity<Mesh::node, Mesh::face>();
        });
  }

  cls.def_prop_ro("elements", [](const Mesh &self) {
    nb::dict elements;
    elements["nodes"] = nb::cast(self.nodes);
    elements["edges"] = nb::cast(self.edges);
    elements["faces"] = nb::cast(self.faces);
    elements["cells"] = nb::cast(self.cells);
    return elements;
  });

  cls.def_ro("vertices", &Mesh::vertices);
  cls.def_prop_ro("elements_shape", [](const Mesh &self) {
    assert(self.shape);
    return icmesh::bindings::as_tuple(*self.shape);
  });
  cls.def_prop_ro("points_shape", [](const Mesh &self) {
    assert(self.shape);
    auto shape = *self.shape;
    for (auto &&n : shape)
      ++n;
    return icmesh::bindings::as_tuple(shape);
  });
  cls.def_prop_ro("extent", [](const Mesh &self) {
    assert(self.bounding_box);
    using namespace compass::utils::array_operators;
    return icmesh::bindings::as_tuple(self.bounding_box->second -
                                      self.bounding_box->first);
  });
  cls.def_prop_ro("origin", [](const Mesh &self) {
    assert(self.bounding_box);
    return icmesh::bindings::as_tuple(self.bounding_box->first);
  });
  cls.def_prop_ro("bbox", [](const Mesh &self) {
    assert(self.bounding_box);
    using icmesh::bindings::as_tuple;
    return nb::make_tuple(as_tuple(self.bounding_box->first),
                          as_tuple(self.bounding_box->second));
  });
}

struct Raw_mesh_factory {
  nb::object operator()(const std::size_t dim, auto &&offset) const {
    switch (dim) {
    case 1:
      return nb::cast(icmesh::raw_mesh(icmesh::lattice(offset(0))));
    case 2:
      return nb::cast(icmesh::raw_mesh(icmesh::lattice(offset(0), offset(1))));
    case 3:
      return nb::cast(
          icmesh::raw_mesh(icmesh::lattice(offset(0), offset(1), offset(2))));
    default:
      throw std::runtime_error("unhandled dimension");
    }
  }
};

} // namespace

void bind_raw_mesh(nb::module_ &module) {

  // dependency on icus to handle index sets and connectivities from python
  nb::module_::import_("icus");

  bind_structured_mesh<1>(module);
  bind_structured_mesh<2>(module);
  bind_structured_mesh<3>(module);

  icmesh::bindings::bind_regular(module, "raw_mesh", Raw_mesh_factory{});
  icmesh::bindings::bind_scottish(module, "raw_mesh", Raw_mesh_factory{});
}
